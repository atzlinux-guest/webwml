#use wml::debian::template title="Debian &ldquo;lenny&rdquo;-utgivelsesinformasjon"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/lenny/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="552f9232b5d467c677963540a5a4236c824e87b9" maintainer="Hans F. Nordhaug"

<p>
  Debian <current_release_lenny> ble utgitt 
  <a href="$(HOME)/News/<current_release_newsurl_lenny/>"><current_release_date_lenny></a>. 
  Debian 5.0.0 ble først utgitt <:=spokendate('2009-02-14'):>.
  Utgivelsen inkluderte mange store endringer beskrevet i vår
  <a href="$(HOME)/News/2009/20090214">pressemelding</a> og i
  <a href="releasenotes">utgivelsesmerknadene</a>.
</p>

<p><strong>
Debian GNU/Linux 5.0 er erstattet av <a href="../squeeze/">Debian 6.0 (<q>squeeze</q>)</a>.
Sikkerhetsoppdateringer opphørte <:=spokendate('2012-02-06'):>.
</strong></p>

<p>
  For å få tak og installere Debian, se siden med
  <a href="debian-installer/">installasjonsinformasjon</a> og 
  <a href="installmanual">installasjonshåndboken</a>. 
  For å oppgradere fra eldre Debian utgaver, se instruksjonene i 
  <a href="releasenotes">utgivelsesmerknadene</a>.
</p>

<p>Følgende datamaskinarkitekturer er støttet i denne utgaven:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>
  I motsetning til våre ønsker kan det være problemer i denne utgave selvom
  den er erklært <em>stabil</em>. Vi har lagd <a href="errata">en list med de
  viktigste kjente problemene</a>, og du kan alltid 
  <a href="reportingbugs">rapportere andre problem</a> til oss.
</p>

<p>
  Sist, men ikke minst, har vi en liste med <a href="credits">folk som har
  sørget for at denne utgaven ble utgitt</a>.
</p>
