#use wml::debian::translation-check translation="e7ce9859ec2794a8c0d0978182d2d513ed96cfa0" maintainer="galaxico"
<define-tag pagetitle>Debian Edu / Skolelinux Bullseye — μια ολοκληρωμένη λύση Linux για το σχολείο σας</define-tag>
<define-tag release_date>2021-08-15</define-tag>
#use wml::debian::news

<p>
Είστε το διαχειριστής συστημάτων για ένα εργαστήριο υπολογιστών ή ενός
ολόκληρου σχολικού δικτύου; Θα θέλατε να εγκαθιστάτε εξυπηρετητές, σταθμούς
εργασίας και φορητούς υπολογιστές να δουλεύουν μαζί;
Θέλετε την σταθερότητα του Debian με δικτυακές υπηρεσίες ήδη προρυθμισμένες;
Θέλετε να έχετε ένα βασισμένο στο web εργαλείο για τη διαχείριση 
τω συστημάτων και αρκετών εκατοντάδων ή και περισσότερων λογαριασμών χρηστών;
Έχετε ποτέ ρωτήσει τον εαυτό σας αν και πώς μπορούν να χρησιμοποιηθούν
παλιότεροι υπολογιστές;
</p>

<p>
Τότε το Debian Edu είναι για σας! The teachers themselves or their technical support
can roll out a complete multi-user multi-machine study environment within
a few days. Debian Edu comes with hundreds of applications pre-installed,
and you can always add more packages from Debian.
</p>

<p>
The Debian Edu developer team is happy to announce Debian Edu 11
<q>Bullseye</q>, the Debian Edu / Skolelinux release based
on the Debian 11 <q>Bullseye</q> release.
Please consider testing it and reporting back (&lt;debian-edu@lists.debian.org&gt;)
to help us to improve it further.
</p>

<h2>Σχετικέ με το Debian Edu και το Skolelinux</h2>

<p>
<a href="https://blends.debian.org/edu"> Debian Edu, also known as
Skolelinux</a>, is a Linux distribution based on Debian providing an
out-of-the box environment of a completely configured school network.
Immediately after installation, a school server running all services
needed for a school network is set up just waiting for users and
machines to be added via GOsa², a comfortable web interface.
A netbooting environment is prepared, so after initial
installation of the main server from CD / DVD / BD or USB stick all other
machines can be installed via the network.
Older computers (even up to ten or so years old) can be used as LTSP
thin clients or diskless workstations, booting from the network without
any installation and configuration at all.
The Debian Edu school server provides an LDAP database and Kerberos
authentication service, centralized home directories, a DHCP server, a web
proxy and many other services. The desktop environment contains more than 70
educational software packages and more are available from the Debian archive.
Schools can choose between the desktop environments Xfce, GNOME, LXDE,
MATE, KDE Plasma, Cinnamon and LXQt.
</p>

<h2>Καινούρια γνωρίσματα του Debian Edu 11 <q>Bullseye</q></h2>

<p>These are some items from the release notes for Debian Edu 11 <q>Bullseye</q>,
based on the Debian 11 <q>Bullseye</q> release.
The full list including more detailed information is part of the related
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Features#New_features_in_Debian_Edu_Bullseye">Debian Edu manual chapter</a>.
</p>

<ul>
<li>
New <a href="https://ltsp.org">LTSP</a> to support diskless workstations. Thin
clients are still supported, now using <a href="https://wiki.x2go.org">X2Go</a>
technology.
</li>
<li>
Booting over the network is provided using iPXE instead of PXELINUX to be
compliant with LTSP.
</li>
<li>
The Debian Installer's graphical mode is used for iPXE installations.
</li>
<li>
Samba is now configured as <q>standalone server</q> with support for SMB2/SMB3.
</li>
<li>
DuckDuckGo is used as default search provider for both Firefox ESR and Chromium.
</li>
<li>
New tool added to set up freeRADIUS with support for both EAP-TTLS/PAP and
PEAP-MSCHAPV2 methods.
</li>
<li>
Improved tool available to configure a new system with <q>Minimal</q> profile as
dedicated gateway.
</li>
</ul>

<h2>Download options, installation steps and manual</h2>

<p>
Official Debian Network-Installer CD images for both 64-bit and 32-bit PCs
are available. The 32-bit image will only be needed in rare cases (for
PCs older than around 15 years). The images can be downloaded at the following
locations:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Alternatively official Debian BD images (more than 5 GB in size) are also
available. It is possible to set up a whole Debian Edu network without an 
Internet connection (including all desktop environments and localization for all
languages supported by Debian).
These images can be downloaded at the following locations: 
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
The images can be verified using the signed checksums provided in the download
directory.
<br />
Once you've downloaded an image, you can check that
<ul>
<li>
its checksum matches that expected from the checksum file; and that
</li>
<li>
the checksum file has not been tampered with.
</li>
</ul>
For more information about how to do these steps, read the
<a href="https://www.debian.org/CD/verify">verification guide</a>.
</p>

<p>
Debian Edu 11 <q>Bullseye</q> is entirely based on Debian 11 <q>Bullseye</q>;
so the sources for all packages are available from the Debian archive.
</p>

<p>
Please note the
<a href="https://wiki.debian.org/DebianEdu/Status/Bullseye">Debian Edu Bullseye status page</a>.
for always up-to-date information about Debian Edu 11 <q>Bullseye</q> including
instructions how to use <code>rsync</code> for downloading the ISO images.
</p>

<p>
When upgrading from Debian Edu 10 <q>Buster</q> please read the related
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Upgrades">Debian Edu manual chapter</a>.
</p>

<p>
For installation notes please read the related
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Installation#Installing_Debian_Edu">Debian Edu manual chapter</a>.
</p>

<p>
After installation you need to take these
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/GettingStarted">first steps</a>.
</p>

<p>
Please see the <a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/">Debian Edu wiki pages</a>
for the latest English version of the Debian Edu <q>Bullseye</q> manual.
The manual has been fully translated to German, French, Italian, Danish, Dutch,
Norwegian Bokmål, Japanese, Simplified Chinese and Portuguese (Portugal).
Partly translated versions exist for Spanish, Romanian and Polish.
An overview of <a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
the latest published versions of the manual</a> is available.
</p>

<p>
More information about Debian 11 <q>Bullseye</q> itself is provided in the
release notes and the installation manual; see <a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Σχετικά με το Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely free
operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.</p>
