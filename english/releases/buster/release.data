<perl>

# list of architectures, ordered by Popularity Contest on 2014-04-26.
@arches = (
#	alpha,
	amd64,
#	arm,
	arm64,
	armel,
	armhf,
#	hppa,
#	'hurd-i386',
	i386,
#	ia64,
#	'kfreebsd-amd64',
#	'kfreebsd-i386',
#	m68k,
	mips,
	mipsel,
	mips64el,
#	powerpc,
#	ppc64,
	ppc64el,
#	s390,
	s390x,
#	sh4,
#	sparc,
);

# list of languages install manual is translated to
%langsinstall = (
	english => "en",
	catalan => "ca",
	czech => "cs",
	danish => "da",
	german => "de",
	greek => "el",
	spanish => "es",
#	basque => "eu,
#	finnish => "fi",
	french => "fr",
#	hungarian => "hu",
	italian => "it",
	japanese => "ja",
	korean => "ko",
	dutch => "nl",
#	norwegian_nynorsk => "nn",
	portuguese => "pt",
#	portuguese_br => "pt-br",
#	romanian => "ro",
	russian => "ru",
	swedish => "sv",
#	tagalog => "tl",
	vietnamese => "vi",
	chinese_cn => "zh-cn",
#	chinese => "zh-tw",
);

# list of languages release notes are translated to
%langsrelnotes = (
	english => "en",
	belarusian => "be",
#	catalan => "ca",
	czech => "cs",
	danish => "da",
	german => "de",
	spanish => "es",
#	finnish => "fi",
	french => "fr",
	italian => "it",
	japanese => "ja",
	lithuanian => "lt",
	malayalam => "ml",
	norwegian_bokmal => "nb",
	dutch => "nl",
	polish => "pl",
	portuguese => "pt",
	portuguese_br => "pt-br",
	romanian => "ro",
	russian => "ru",
	slovak => "sk",
	swedish => "sv",
	vietnamese => "vi",
	chinese_cn => "zh-cn",
	chinese => "zh-tw",
);

</perl>


### While buster is stable, we can reuse the tags defined in
#### templates/debian/release_images.wml.
#### When the website gets prepared for the next stable release,
#### the tags should be defined here instead (see previous releases
#### for examples).
#### Note that images for the new oldstable release will be moved to
#### cdimage/archive at the time of the release!
#
### Next line should be changed to 'wml::debian::installer' when
### preparing for next stable release; don't forget the Makefile!
#

### Hint: the first line of three here need to be used during 'full security support' time,
### while line2+3 are needed, when during LTS period there are different point-release
### version numbers for lts archs and non-lts archs (for example, that's the case for Jessie). 
### Don't forget to define the archs in template/debian/installer.wml for that.
#use wml::debian::installer
<define-tag buster-images-url>https://cdimage.debian.org/cdimage/archive/10.11.0</define-tag>
#<define-tag buster-lts-images-url>https://cdimage.debian.org/cdimage/archive/10.11.0</define-tag>
#<define-tag buster-nonlts-images-url>https://cdimage.debian.org/cdimage/archive/10.11.0</define-tag>
<define-tag buster-cd-release-filename>10.11.0</define-tag>
#<define-tag buster-lts-cd-release-filename>10.11.0</define-tag>
#<define-tag buster-nonlts-cd-release-filename>10.11.0</define-tag>

# Use the first line during full security support time, and line 2+3 during LTS period; see above.
<define-tag netinst-images>
<images-list url="<buster-images-url/>/@ARCH@/iso-cd/debian-<buster-cd-release-filename/>-@ARCH@-netinst.iso" arch="<strip-arches "<buster-images-arches />" "source" />" />
#<images-list url="<buster-lts-images-url/>/@ARCH@/iso-cd/debian-<buster-lts-cd-release-filename/>-@ARCH@-netinst.iso" arch="<strip-arches "<buster-lts-images-arches />" "source" />" />
#<images-list url="<buster-nonlts-images-url/>/@ARCH@/iso-cd/debian-<buster-nonlts-cd-release-filename/>-@ARCH@-netinst.iso" arch="<strip-arches "<buster-nonlts-images-arches />" "source" />" />
</define-tag>

# Use the first line during full security support time, and line 2+3 during LTS period; see above.
<define-tag full-cd-images>
<images-list url="<buster-images-url/>/@ARCH@/iso-cd/" arch="<strip-arches "<buster-images-arches />" "source" /> multi-arch" />
#<images-list url="<buster-lts-images-url/>/@ARCH@/iso-cd/" arch="<strip-arches "<buster-lts-images-arches />" "source" /> multi-arch" />
#<images-list url="<buster-nonlts-images-url/>/@ARCH@/iso-cd/" arch="<strip-arches "<buster-nonlts-images-arches />" "source" />" />
</define-tag>

# Use the first line during full security support time, and line 2+3 during LTS period; see above.
<define-tag full-cd-torrent>
<images-list url="<buster-images-url/>/@ARCH@/bt-cd/" arch="<strip-arches "<buster-images-arches />" "source" /> multi-arch" />
#<images-list url="<buster-lts-images-url/>/@ARCH@/bt-cd/" arch="<strip-arches "<buster-lts-images-arches />" "source" /> multi-arch" />
#<images-list url="<buster-nonlts-images-url/>/@ARCH@/bt-cd/" arch="<strip-arches "<buster-nonlts-images-arches />" "source" />" />
</define-tag>

# Use the first line during full security support time, and line 2+3 during LTS period; see above.
<define-tag full-cd-jigdo>
<images-list url="<buster-images-url/>/@ARCH@/jigdo-cd/" arch="<strip-arches "<buster-images-arches />" "source" /> multi-arch" />
#<images-list url="<buster-lts-images-url/>/@ARCH@/jigdo-cd/" arch="<strip-arches "<buster-lts-images-arches />" "source" /> multi-arch" />
#<images-list url="<buster-nonlts-images-url/>/@ARCH@/jigdo-cd/" arch="<strip-arches "<buster-nonlts-images-arches />" "source" />" />
</define-tag>

# Use the first line during full security support time, and line 2+3 during LTS period; see above.
<define-tag full-dvd-images>
<images-list url="<buster-images-url/>/@ARCH@/iso-dvd/" arch="<buster-images-arches />" />
#<images-list url="<buster-lts-images-url/>/@ARCH@/iso-dvd/" arch="<buster-lts-images-arches /> multi-arch" />
#<images-list url="<buster-nonlts-images-url/>/@ARCH@/iso-dvd/" arch="<buster-nonlts-images-arches />" />
</define-tag>

# Use the first line during full security support time, and line 2+3 during LTS period; see above.
<define-tag full-dvd-torrent>
<images-list url="<buster-images-url/>/@ARCH@/bt-dvd/" arch="<buster-images-arches />" />
#<images-list url="<buster-lts-images-url/>/@ARCH@/bt-dvd/" arch="<buster-lts-images-arches /> multi-arch" />
#<images-list url="<buster-nonlts-images-url/>/@ARCH@/bt-dvd/" arch="<buster-nonlts-images-arches />" />
</define-tag>

# Use the first line during full security support time, and line 2+3 during LTS period; see above.
<define-tag full-dvd-jigdo>
<images-list url="<buster-images-url/>/@ARCH@/jigdo-dvd/" arch="<buster-images-arches />" />
#<images-list url="<buster-lts-images-url/>/@ARCH@/jigdo-dvd/" arch="<buster-lts-images-arches /> multi-arch" />
#<images-list url="<buster-nonlts-images-url/>/@ARCH@/jigdo-dvd/" arch="<buster-nonlts-images-arches />" />
</define-tag>

# Use the first line during full security support time, and line 2 during LTS period; see above.
<define-tag full-bluray-jigdo>
<images-list url="<buster-images-url/>/@ARCH@/jigdo-bd/" arch="i386 amd64 source" />
#<images-list url="<buster-lts-images-url/>/@ARCH@/jigdo-bd/" arch="i386 amd64 source" />
</define-tag>

<define-tag otherimages-url>http://ftp.debian.org/debian/dists/buster/main/installer-@ARCH@/current/images/</define-tag>

# Use the first line during full security support time, and line 2 during LTS period; see above.
<define-tag other-images>
<images-list url="<otherimages-url/>" arch="<strip-arches "<buster-images-arches />" "source" />" />
#<images-list url="<otherimages-url/>" arch="<strip-arches "<buster-lts-images-arches />" "source" />" />
</define-tag>

# Use the first line during full security support time, and line 2 during LTS period; see above.
<define-tag small-non-free-cd-images>
<images-list url="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/archive/<buster-cd-release-filename/>+nonfree/@ARCH@/iso-cd/firmware-<buster-cd-release-filename/>-@ARCH@-netinst.iso" arch="amd64 i386" />
#<images-list url="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/archive/<buster-lts-cd-release-filename/>+nonfree/@ARCH@/iso-cd/firmware-<buster-lts-cd-release-filename/>-@ARCH@-netinst.iso" arch="amd64 i386" />
</define-tag>
