<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Ben Pfaff discovered that the convert_to_decimal function in the GNU
Portability Library contains a heap-based buffer overflow because memory
is not allocated for a trailing '\0' character during %f processing.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
20140202+stable-2+deb8u1.</p>

<p>We recommend that you upgrade your gnulib packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1543.data"
# $Id: $
