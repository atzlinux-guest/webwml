<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Parker Moore from Github Inc, discovered a vulnerability in include:
setting in the config file of jekyll which allow arbitrary file reads.
By simply including a symlink in the include array allowed the
symlinked file to be read into the build when they shouldn’t actually
be read in any circumstance.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.2.0+dfsg-2+deb8u1.</p>

<p>We recommend that you upgrade your jekyll packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1541.data"
# $Id: $
