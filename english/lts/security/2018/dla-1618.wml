<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in libsndfile, the library for
reading and writing files containing sampled sound.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8361">CVE-2017-8361</a>

    <p>The flac_buffer_copy function (flac.c) is affected by a buffer
    overflow. This vulnerability might be leveraged by remote attackers to
    cause a denial of service, or possibly have unspecified other impact
    via a crafted audio file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8362">CVE-2017-8362</a>

    <p>The flac_buffer_copy function (flac.c) is affected by an out-of-bounds
    read vulnerability. This flaw might be leveraged by remote attackers to
    cause a denial of service via a crafted audio file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8363">CVE-2017-8363</a>

    <p>The flac_buffer_copy function (flac.c) is affected by a heap based OOB
    read vulnerability. This flaw might be leveraged by remote attackers to
    cause a denial of service via a crafted audio file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8365">CVE-2017-8365</a>

    <p>The i2les_array function (pcm.c) is affected by a global buffer
    overflow. This vulnerability might be leveraged by remote attackers to
    cause a denial of service, or possibly have unspecified other impact
    via a crafted audio file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14245">CVE-2017-14245</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-14246">CVE-2017-14246</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-17456">CVE-2017-17456</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-17457">CVE-2017-17457</a>

    <p>The d2alaw_array() and d2ulaw_array() functions (src/ulaw.c and
    src/alaw.c) are affected by an out-of-bounds read vulnerability. This
    flaw might be leveraged by remote attackers to cause denial of service
    or information disclosure via a crafted audio file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14634">CVE-2017-14634</a>

    <p>The double64_init() function (double64.c) is affected by a
    divide-by-zero error. This vulnerability might be leveraged by remote
    attackers to cause denial of service via a crafted audio file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13139">CVE-2018-13139</a>

    <p>The psf_memset function (common.c) is affected by a stack-based buffer
    overflow. This vulnerability might be leveraged by remote attackers to
    cause a denial of service, or possibly have unspecified other impact
    via a crafted audio file. The vulnerability can be triggered by the
    executable sndfile-deinterleave.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19432">CVE-2018-19432</a>

    <p>The sf_write_int function (src/sndfile.c) is affected by an
    out-of-bounds read vulnerability. This flaw might be leveraged by
    remote attackers to cause a denial of service via a crafted audio file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19661">CVE-2018-19661</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-19662">CVE-2018-19662</a>

    <p>The i2alaw_array() and i2ulaw_array() functions (src/ulaw.c and
    src/alaw.c) are affected by an out-of-bounds read vulnerability. This
    flaw might be leveraged by remote attackers to cause denial of service
    or information disclosure via a crafted audio file.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.0.25-9.1+deb8u2.</p>

<p>We recommend that you upgrade your libsndfile packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1618.data"
# $Id: $
