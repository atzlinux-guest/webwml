<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Zack Flack found several issues in monit, a utility for monitoring and
managing daemons or similar programs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11454">CVE-2019-11454</a>

      <p>An XSS vulnerabilitty has been reported that could be prevented by
      HTML escaping the log file content when viewed via Monit GUI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11455">CVE-2019-11455</a>

      <p>A buffer overrun vulnerability has been reported in URL decoding.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:5.9-1+deb8u2.</p>

<p>We recommend that you upgrade your monit packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1767.data"
# $Id: $
