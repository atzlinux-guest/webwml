<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in QEMU, a fast processor emulator:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11806">CVE-2018-11806</a>

    <p>It was found that the SLiRP networking implementation could use a wrong
    size when reallocating its buffers, which can be exploited by a
    priviledged user on a guest to cause denial of service or possibly
    arbitrary code execution on the host system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18849">CVE-2018-18849</a>

    <p>It was found that the LSI53C895A SCSI Host Bus Adapter emulation was
    susceptible to an out of bounds memory access, which could be leveraged
    by a malicious guest user to crash the QEMU process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20815">CVE-2018-20815</a>

    <p>A heap buffer overflow was found in the load_device_tree function,
    which could be used by a malicious user to potentially execute
    arbitrary code with the priviledges of the QEMU process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9824">CVE-2019-9824</a>

    <p>William Bowling discovered that the SLiRP networking implementation did
    not handle some messages properly, which could be triggered to leak
    memory via crafted messages.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.1+dfsg-12+deb8u11.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1781.data"
# $Id: $
