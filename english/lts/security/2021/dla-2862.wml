<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A couple of vulnerabilites were found in python-gnupg, a Python
wrapper for the GNU Privacy Guard.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12020">CVE-2018-12020</a>

    <p>Marcus Brinkmann discovered that GnuPG before 2.2.8 improperly
    handled certain command line parameters. A remote attacker
    could use this to spoof the output of GnuPG and cause unsigned
    e-mail to appear signed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6690">CVE-2019-6690</a>

    <p>It was discovered that python-gnupg incorrectly handled the GPG
    passphrase. A remote attacker could send a specially crafted
    passphrase that would allow them to control the output of
    encryption and decryption operations.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.3.9-1+deb9u1.</p>

<p>We recommend that you upgrade your python-gnupg packages.</p>

<p>For the detailed security status of python-gnupg please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-gnupg">https://security-tracker.debian.org/tracker/python-gnupg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2862.data"
# $Id: $
