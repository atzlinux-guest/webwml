<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that SPIP, a website engine for publishing, would
allow a malicious user to perform cross-site scripting attacks, access
sensitive information, or execute arbitrary code.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.1.4-4~deb9u4+deb9u1.</p>

<p>We recommend that you upgrade your spip packages.</p>

<p>For the detailed security status of spip please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/spip">https://security-tracker.debian.org/tracker/spip</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2579.data"
# $Id: $
