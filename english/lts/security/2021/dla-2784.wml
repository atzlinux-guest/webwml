<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential use-after-free vulnerability in
icu, a library which provides Unicode and locale functionality.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21913">CVE-2020-21913</a>

    <p>International Components for Unicode (ICU-20850) v66.1 was discovered to
    contain a use after free bug in the pkg_createWithAssemblyCode function in
    the file tools/pkgdata/pkgdata.cpp.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
57.1-6+deb9u5.</p>

<p>We recommend that you upgrade your icu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2784.data"
# $Id: $
