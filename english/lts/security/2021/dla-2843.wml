<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
                 <p><a href="https://security-tracker.debian.org/tracker/CVE-2021-3653">CVE-2021-3653</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-3655">CVE-2021-3655</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-3679">CVE-2021-3679</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-3732">CVE-2021-3732</a> 
                 <a href="https://security-tracker.debian.org/tracker/CVE-2021-3753">CVE-2021-3753</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-3760">CVE-2021-3760</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-20317">CVE-2021-20317</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-20321">CVE-2021-20321</a> 
                 <a href="https://security-tracker.debian.org/tracker/CVE-2021-20322">CVE-2021-20322</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-22543">CVE-2021-22543</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-37159">CVE-2021-37159</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-38160">CVE-2021-38160</a> 
                 <a href="https://security-tracker.debian.org/tracker/CVE-2021-38198">CVE-2021-38198</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-38199">CVE-2021-38199</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-38204">CVE-2021-38204</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-38205">CVE-2021-38205</a> 
                 <a href="https://security-tracker.debian.org/tracker/CVE-2021-40490">CVE-2021-40490</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-41864">CVE-2021-41864</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-42008">CVE-2021-42008</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-42739">CVE-2021-42739</a> 
                 <a href="https://security-tracker.debian.org/tracker/CVE-2021-43389">CVE-2021-43389</a></p>

<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service, or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3702">CVE-2020-3702</a>

    <p>A flaw was found in the driver for Atheros IEEE 802.11n family of
    chipsets (ath9k) allowing information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16119">CVE-2020-16119</a>

    <p>Hadar Manor reported a use-after-free in the DCCP protocol
    implementation in the Linux kernel. A local attacker can take
    advantage of this flaw to cause a denial of service or potentially
    to execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-0920">CVE-2021-0920</a>

    <p>A race condition was discovered in the local sockets (AF_UNIX)
    subsystem, which could lead to a use-after-free.  A local user
    could exploit this for denial of service (memory corruption or
    crash), or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3612">CVE-2021-3612</a>

    <p>Murray McAllister reported a flaw in the joystick input subsystem.
    A local user permitted to access a joystick device could exploit
    this to read and write out-of-bounds in the kernel, which could
    be used for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3653">CVE-2021-3653</a>

   <p>Maxim Levitsky discovered a vulnerability in the KVM hypervisor
   implementation for AMD processors in the Linux kernel: Missing
   validation of the `int_ctl` VMCB field could allow a malicious L1
   guest to enable AVIC support (Advanced Virtual Interrupt
   Controller) for the L2 guest. The L2 guest can take advantage of
   this flaw to write to a limited but still relatively large subset
   of the host physical memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3655">CVE-2021-3655</a>

    <p>Ilja Van Sprundel and Marcelo Ricardo Leitner found multiple flaws
    in the SCTP implementation, where missing validation could lead to
    an out-of-bounds read.  On a system using SCTP, a networked
    attacker could exploit these to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3679">CVE-2021-3679</a>

    <p>A flaw in the Linux kernel tracing module functionality could
    allow a privileged local user (with CAP_SYS_ADMIN capability) to
    cause a denial of service (resource starvation).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3732">CVE-2021-3732</a>

    <p>Alois Wohlschlager reported a flaw in the implementation of the
    overlayfs subsystem, allowing a local attacker with privileges to
    mount a filesystem to reveal files hidden in the original mount.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3753">CVE-2021-3753</a>

    <p>Minh Yuan reported a race condition in the vt_k_ioctl in
    drivers/tty/vt/vt_ioctl.c, which may cause an out of bounds read
    in vt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3760">CVE-2021-3760</a>

    <p>Lin Horse reported a flaw in the NCI (NFC Controller Interface)
    driver, which could lead to a use-after-free.</p>

    <p>However, this driver is not included in the binary packages
    provided by Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20317">CVE-2021-20317</a>

    <p>It was discovered that the timer queue structure could become
    corrupt, leading to waiting tasks never being woken up.  A local
    user with certain privileges could exploit this to cause a denial
    of service (system hang).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20321">CVE-2021-20321</a>

    <p>A race condition was discovered in the overlayfs filesystem
    driver.  A local user with access to an overlayfs mount and to its
    underlying upper directory could exploit this for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20322">CVE-2021-20322</a>

    <p>An information leak was discovered in the IPv4 implementation.  A
    remote attacker could exploit this to quickly discover which UDP
    ports a system is using, making it easier for them to carry out a
    DNS poisoning attack against that system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22543">CVE-2021-22543</a>

    <p>David Stevens discovered a flaw in how the KVM hypervisor maps
    host memory into a guest.  A local user permitted to access
    /dev/kvm could use this to cause certain pages to be freed when
    they should not, leading to a use-after-free.  This could be used
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37159">CVE-2021-37159</a>

    <p>A flaw was discovered in the hso driver for Option mobile
    broadband modems.  An error during initialisation could lead to a
    double-free or use-after-free.  An attacker able to plug in USB
    devices could use this to cause a denial of service (crash or
    memory corruption) or possibly to run arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38160">CVE-2021-38160</a>

    <p>A flaw in the virtio_console was discovered allowing data
    corruption or data loss by an untrusted device.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38198">CVE-2021-38198</a>

    <p>A flaw was discovered in the KVM implementation for x86
    processors, that could result in virtual memory protection within
    a guest not being applied correctly.  When shadow page tables are
    used - i.e. for nested virtualisation, or on CPUs lacking the EPT
    or NPT feature - a user of the guest OS might be able to exploit
    this for denial of service or privilege escalation within the
    guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38199">CVE-2021-38199</a>

    <p>Michael Wakabayashi reported a flaw in the NFSv4 client
    implementation, where incorrect connection setup ordering allows
    operations of a remote NFSv4 server to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38204">CVE-2021-38204</a>

    <p>A flaw was discovered in the max4321-hcd USB host controller
    driver, which could lead to a use-after-free.</p>

    <p>However, this driver is not included in the binary packages
    provided by Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38205">CVE-2021-38205</a>

    <p>An information leak was discovered in the xilinx_emaclite network
    driver.  On a custom kernel where this driver is enabled and used,
    this might make it easier to exploit other kernel bugs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40490">CVE-2021-40490</a>

    <p>A race condition was discovered in the ext4 subsystem when writing
    to an inline_data file while its xattrs are changing. This could
    result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41864">CVE-2021-41864</a>

    <p>An integer overflow was discovered in the Extended BPF (eBPF)
    subsystem.  A local user could exploit this for denial of service
    (memory corruption or crash), or possibly for privilege
    escalation.</p>

    <p>This can be mitigated by setting sysctl
    kernel.unprivileged_bpf_disabled=1, which disables eBPF use by
    unprivileged users.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42008">CVE-2021-42008</a>

    <p>A heap buffer overflow was discovered in the 6pack serial port
    network driver.  A local user with CAP_NET_ADMIN capability could
    exploit this for denial of service (memory corruption or crash), or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42739">CVE-2021-42739</a>

    <p>A heap buffer overflow was discovered in the firedtv driver for
    FireWire-connected DVB receivers.  A local user with access to a
    firedtv device could exploit this for denial of service (memory
    corruption or crash), or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43389">CVE-2021-43389</a>

    <p>The Active Defense Lab of Venustech discovered a flaw in the CMTP
    subsystem as used by Bluetooth, which could lead to an
    out-of-bounds read and object type confusion.  A local user with
    CAP_NET_ADMIN capability in the initial user namespace could
    exploit this for denial of service (memory corruption or crash),
    or possibly for privilege escalation.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.9.290-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2843.data"
# $Id: $
