<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was discovered in php-pear, which provides core
packages from the PHP Extension and Application Repository. Tar.php in
Archive_Tar allows write operations with Directory Traversal due to
inadequate checking of symbolic links, a related issue to
<a href="https://security-tracker.debian.org/tracker/CVE-2020-28948">CVE-2020-28948</a>. An attacker could escalate privileges by overwriting
files outside the extraction directory through a crafted .tar archive.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:1.10.1+submodules+notgz-9+deb9u3.</p>

<p>We recommend that you upgrade your php-pear packages.</p>

<p>For the detailed security status of php-pear please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php-pear">https://security-tracker.debian.org/tracker/php-pear</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2621.data"
# $Id: $
