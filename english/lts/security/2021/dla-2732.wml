<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in OpenEXR, a library and
tools for the OpenEXR high dynamic-range (HDR) image format. An
attacker could cause a denial of service (DoS) through application
crash, and possibly execute code.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.2.0-11+deb9u4.</p>

<p>We recommend that you upgrade your openexr packages.</p>

<p>For the detailed security status of openexr please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openexr">https://security-tracker.debian.org/tracker/openexr</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2732.data"
# $Id: $
