<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that systems with microprocessors utilizing
speculative execution and indirect branch prediction may allow
unauthorized disclosure of information to an attacker with local
user access via a side-channel analysis (Spectre v2).
Multiple fixes were done already in Linux kernel, intel-microcode,
et al. This fix adds amd-microcode-based IBPB support.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.20181128.1~deb9u1.</p>

<p>We recommend that you upgrade your amd64-microcode packages.</p>

<p>For the detailed security status of amd64-microcode please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/amd64-microcode">https://security-tracker.debian.org/tracker/amd64-microcode</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2743.data"
# $Id: $
