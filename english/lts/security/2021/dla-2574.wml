<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability in the Certificate List Exact Assertion validation
was discovered in OpenLDAP, a free implementation of the Lightweight
Directory Access Protocol. An unauthenticated remote attacker can
take advantage of this flaw to cause a denial of service (slapd
daemon crash) via specially crafted packets.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.4.44+dfsg-5+deb9u8.</p>

<p>We recommend that you upgrade your openldap packages.</p>

<p>For the detailed security status of openldap please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openldap">https://security-tracker.debian.org/tracker/openldap</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2574.data"
# $Id: $
