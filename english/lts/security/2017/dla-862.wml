<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2016-8743">CVE-2016-8743</a> 
in apache2 2.2.22-13+deb7u8 (DLA-841-1) caused
#852623 in sitesummary, breaking the sitesummary-upload functionality.
To address this sitesummary-upload needs to be changed to send CRLF (\r\n)
line endings to be compliant with the apache security fixes for HTTP requests.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.1.8+deb7u2.</p>

<p>We recommend that you upgrade your sitesummary packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-862.data"
# $Id: $
