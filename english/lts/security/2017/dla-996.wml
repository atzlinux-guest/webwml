<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The error page mechanism of the Java Servlet Specification requires
that, when an error occurs and an error page is configured for the
error that occurred, the original request and response are forwarded
to the error page. This means that the request is presented to the
error page with the original HTTP method. If the error page is a
static file, expected behaviour is to serve content of the file as if
processing a GET request, regardless of the actual HTTP method. The
Default Servlet in Apache Tomcat did not do this. Depending on the
original request this could lead to unexpected and undesirable results
for static error pages including, if the DefaultServlet is configured
to permit writes, the replacement or removal of the custom error page.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.0.28-4+deb7u14.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-996.data"
# $Id: $
