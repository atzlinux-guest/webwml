<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a header-splitting vulnerability in ceph, a
distributed storage and file system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1760">CVE-2020-1760</a>

    <p>header-splitting in RGW GetObject has a possible XSS</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.80.7-2+deb8u4.</p>

<p>We recommend that you upgrade your ceph packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2171.data"
# $Id: $
