<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A double free vulnerability was discovered in libvirt, a toolkit to
interact with the virtualization capabilities of recent versions of
Linux (and other OSes).</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.0.0-4+deb9u5.</p>

<p>We recommend that you upgrade your libvirt packages.</p>

<p>For the detailed security status of libvirt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libvirt">https://security-tracker.debian.org/tracker/libvirt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2395.data"
# $Id: $
