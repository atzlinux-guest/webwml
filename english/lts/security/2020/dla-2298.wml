<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in libapache2-mod-auth-openidc, the OpenID
Connect authentication module for the Apache HTTP server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14857">CVE-2019-14857</a>

     <p>Insufficient validation of URLs leads to an Open Redirect
     vulnerability. An attacker may trick a victim into providing
     credentials for an OpenID provider by forwarding the request to an
     illegitimate website.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20479">CVE-2019-20479</a>

     <p>Due to insufficient validatation of URLs an Open Redirect
     vulnerability for URLs beginning with a slash and backslash could be
     abused.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010247">CVE-2019-1010247</a>

     <p>The OIDCRedirectURI page contains generated JavaScript code that uses
     a poll parameter as a string variable, thus might contain additional
     JavaScript code. This might result in Criss-Site Scripting (XSS).</p>


<p>For Debian 9 stretch, these problems have been fixed in version
2.1.6-1+deb9u1.</p>

<p>We recommend that you upgrade your libapache2-mod-auth-openidc packages.</p>

<p>For the detailed security status of libapache2-mod-auth-openidc please
refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc">https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2298.data"
# $Id: $
