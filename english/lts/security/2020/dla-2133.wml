<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17569">CVE-2019-17569</a>

    <p>The refactoring in 7.0.98 introduced a regression. The result of
    the regression was that invalid Transfer-Encoding headers were
    incorrectly processed leading to a possibility of HTTP Request
    Smuggling if Tomcat was located behind a reverse proxy that
    incorrectly handled the invalid Transfer-Encoding header in a
    particular manner. Such a reverse proxy is considered unlikely.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1935">CVE-2020-1935</a>

    <p>The HTTP header parsing code used an approach to end-of-line (EOL)
    parsing that allowed some invalid HTTP headers to be parsed as
    valid. This led to a possibility of HTTP Request Smuggling if Tomcat
    was located behind a reverse proxy that incorrectly handled the
    invalid Transfer-Encoding header in a particular manner. Such a
    reverse proxy is considered unlikely.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1938">CVE-2020-1938</a>

    <p>When using the Apache JServ Protocol (AJP), care must be taken when
    trusting incoming connections to Apache Tomcat. Tomcat treats AJP
    connections as having higher trust than, for example, a similar HTTP
    connection. If such connections are available to an attacker, they
    can be exploited in ways that may be surprising. Prior to Tomcat
    7.0.100, Tomcat shipped with an AJP Connector enabled by default
    that listened on all configured IP addresses. It was expected (and
    recommended in the security guide) that this Connector would be
    disabled if not required.</p>

    <p>Note that Debian already disabled the AJP connector by default.
    Mitigation is only required if the AJP port was made accessible to
    untrusted users.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
7.0.56-3+really7.0.100-1.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2133.data"
# $Id: $
