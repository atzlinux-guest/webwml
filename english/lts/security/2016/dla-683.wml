<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in the graphicsmagick package
that may lead to denial of service through failed assertions, CPU or
memory usage. Some vulnerabilities may also lead to code execution but
no exploit is currently known.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7448">CVE-2016-7448</a>

    <p>Utah RLE: Reject truncated/absurd files which caused huge memory
    allocations and/or consumed huge CPU </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7996">CVE-2016-7996</a>

    <p>missing check that the provided colormap is not larger than 256
    entries resulting in potential heap overflow</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7997">CVE-2016-7997</a>

    <p>denial of service via a crash due to an assertion</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8682">CVE-2016-8682</a>

    <p>stack-based buffer overflow in ReadSCTImage (sct.c)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8683">CVE-2016-8683</a>

    <p>memory allocation failure in ReadPCXImage (pcx.c)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8684">CVE-2016-8684</a>

    <p>memory allocation failure in MagickMalloc (memory.c)</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u5.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-683.data"
# $Id: $
