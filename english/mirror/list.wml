#use wml::debian::template title="Debian worldwide mirror sites" BARETITLE=true

<p>Debian is distributed (<em>mirrored</em>) on hundreds of
servers on the Internet. Using a nearby server will probably speed up
your download, and also reduce the load on our central servers and
on the Internet as a whole.</p>

<p class="centerblock">
  Debian mirrors exist in many countries, and for some we have
  added a <code>ftp.&lt;country&gt;.debian.org</code> alias.  This
  alias usually points to a mirror that syncs regularly and quickly
  and carries all of Debian's architectures.  The debian archive
  is always available via <code>HTTP</code> at the <code>/debian</code>
  location on the server.
</p>

<p class="centerblock">
  Other <strong>mirror sites</strong> may have restrictions on what
  they mirror (due to space restrictions).  Just because a site is not the
  country's <code>ftp.&lt;country&gt;.debian.org</code> does not necessarily
  mean that it is any slower or less up to date than the
  <code>ftp.&lt;country&gt;.debian.org</code> mirror.
  In fact, a mirror that carries your architecture and is closer to you as the
  user and, therefore, faster, is almost always preferable to other mirrors
  that are farther away.
</p>

<p>Use the site closest to you for the fastest downloads possible
whether it is a per-country mirror alias or not.
The program
<a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a> can be used to
determine the site with the least latency; use a download program such as
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> or
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a> for determining the site with the most throughput.
Note that geographic proximity often isn't the most important factor for
determining which machine will serve you best.</p>

<p>
If your system moves around a lot, you may be best served by a "mirror"
that is backed by a global <abbr title="Content Delivery Network">CDN</abbr>.
The Debian project maintains
<code>deb.debian.org</code> for this
purpose and you can use this in your apt sources.list &mdash; consult
<a href="http://deb.debian.org/">the service's website for details</a>.

<p>
Everything else you want to know about Debian mirrors:
<url "https://www.debian.org/mirror/">.
</p>

<h2 class="center">Debian per-country mirror aliases</h2>

<table border="0" class="center">
<tr>
  <th>Country</th>
  <th>Site</th>
  <th>Architectures</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">List of mirrors of the Debian archive</h2>

<table border="0" class="center">
<tr>
  <th>Host name</th>
  <th>HTTP</th>
  <th>Architectures</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
