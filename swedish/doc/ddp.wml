#use wml::debian::ddp title="Debian Documentation Project"
#use wml::debian::translation-check translation="e8de3f9543c6c16d99765f9d2b60925bd6495dd0"

<p>Debian Documentation Project startades för att
samordna och samla alla tidigare insatser att skriva mer och bättre
dokumentation för Debiansystemet.
</p>

  <h2>DDPs arbete</h2>

<div class="line">
  <div class="item col50">

    <h3>Manualer</h3>
    <ul>
     <li><strong><a href="user-manuals">Användarmanualer</a></strong></li>
     <li><strong><a href="devel-manuals">Utvecklarmanualer</a></strong></li>
     <li><strong><a href="misc-manuals">Diverse manualer</a></strong></li>
     <li><strong><a href="#other">Problematiska manualer</a></strong></li>
    </ul>
  </div>

  <div class="item col50 lastcol">
      
    <h3>Dokumentationspolicy</h3>
    <ul>
      <li>Manualernas licenser följer DFSG.</li>
      <li>Vi använder Docbook XML för våra dokument.</li>
      <li>Källkoden skall finnas på <a href="https://salsa.debian.org/ddp-team">https://salsa.debian.org/ddp-team</a></li>
      <li><tt>www.debian.org/doc/&lt;manual-namn&gt;</tt> kommer att vara den officiella URLen</li>
      <li>Varje dokument skall ha en aktiv ansvarig.</li>
      <li>Vänligen fråga på <a href="https://lists.debian.org/debian-doc/">debian-doc</a> om du skulle vilja skriva ett nytt dokument</li>
    </ul>
    
    <h3>Git-åtkomst</h3>
    <ul>
     <li><a href="vcs">Hur du kommer åt</a> DDPs git-förrådet.
     </li>
    </ul>

  </div>


</div>

<hr />

<h2><a name="other">Problematiska manualer</a></h2>

<p>Förutom de manualer vi normalt berättar om
tillhandahåller vi följande manualer som har
ett eller annat problem, så vi kan inte rekommendera dem till alla och
envar.
Används på egen risk.</p>

<ul>
   <li><a href="obsolete#tutorial">Debian Tutorial</a>, föråldrad</li>
   <li><a href="obsolete#guide">Debian Guide</a>, föråldrad</li>
   <li><a href="obsolete#userref">Debians användarreferenshandbok</a>,
       avstannad och rätt ofullständig
   </li>
   <li><a href="obsolete#system">Debians systemadministratörshandbok</a>,
       avstannad, nästan tom
   </li>
   <li><a href="obsolete#network">Debians nätverksadministratörshandbok</a>,
       avstannad, ofullständig
   </li>
   <li><a href="obsolete#swprod">Hur programvaruproducenter kan
       distribuera sina program direkt i .deb-formatet</a>,
       avstannad, föråldrad
   </li>
   <li><a href="obsolete#packman">Debians paketeringshandbok</a>,
       delvis inlagd i
       <a href="devel-manuals#policy">Debians policyhandbok</a>,
       resten kommer bli en del av en dpkg-referensmanual som ännu inte
       har skrivits
   </li>
   <li><a href="obsolete#makeadeb">Introduction: Making a Debian Package</a>,
       ersatt av av
       <a href="devel-manuals#maint-guide">Debian New Maintainers' Guide</a>
   </li>
   <li><a href="obsolete#programmers">Debians programmeringshandbok</a>,
       ersatt av
       <a href="devel-manuals#maint-guide">Debian New Maintainers' Guide</a>
	   och
	   <a href="devel-manuals#debmake-doc">Guide för Debian Maintainers</a>
   </li>
   <li><a href="obsolete#repo">Guide till Debianarkiv</a>, föråldrad efter att säker APT introducerats</li>
   <li><a href="obsolete#i18n">Introduktion till i18n</a>, avstannad</li>
   <li><a href="obsolete#sgml-howto">Debian SGML/XML Guide</a>, avstannad, föråldrad</li>
   <li><a href="obsolete#markup">Debiandoc-SGML Markup Manual</a>, avstannad; DebianDoc är på väg att bli borttaget</li>

</ul>
