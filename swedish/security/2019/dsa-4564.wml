#use wml::debian::translation-check translation="0cc922cd661d77cfeed7f482bce1cfba75c197ae" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i Linuxkärnan som kan leda
till utökning av privilegier, överbelastning, eller informationsläckage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12207">CVE-2018-12207</a>

    <p>Man har upptäckt att på Intel-CPUer med stöd för
    hårdvaruvirtualisering med Extended Page Tables (EPT), kan
    en gästmaskin manipulera minneshanteringshårdvaran för att orsaka
    ett Machine Check Error (MCE) och överbelastning (hängning eller krasch).</p>

    <p>Gästen triggar detta fel genom att ändra sidtabeller utan en
    TLB flush, så både 4 KB och 2 MB-registreringar på samma virtuella
    adress laddas in i instruktions-TLBn (iTLB). Denna uppdatering
    implementerar en lindring i KVM som förhindrar gäst-VMs från att
    ladda 2 MB-registreringar i iTLBn. Detta minskar prestandan i
    gäst-VMs.</p>
    
    <p>Ytterligare information om lindringen kan hittas på 
    <url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/multihit.html">
    eller i paketen linux-doc-4.9 eller linux-doc-4.19.</p>

    <p>En qemu-uppdatering som lägger till stöd för funktionen PSCHANGE_MC_NO,
    som tillåter att inaktivera iTLB Multihit-lindringar i nästlade hypervisors
    kommer att tillhandahållas i DSA 4566-1.</p>

    <p>Intels förklaring till problemet kan hittas på
    <url "https://software.intel.com/security-software-guidance/insights/deep-dive-machine-check-error-avoidance-page-size-change-0">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0154">CVE-2019-0154</a>

    <p>Intel upptäckte att på deras 8:de och 9:de generation av GPUer,
    kan läsning av vissa register medan GPUn är i lågeffektsläge
    orsaka systemhängning. En lokal användare med rättigheter att använda
    GPUn kan utnyttja detta för överbelastning.</p>
    
    <p>Denna uppdatering lindrar detta problem genom ändringar i i915-drivrutinen.</p>
    
    <p>De påverkade chippen (gen8 och gen9) listas på
    <url "https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen8">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0155">CVE-2019-0155</a>

    <p>Intel upptäckte att deras 9:de generation och nyare GPUer saknar
    en säkerhetskontroll i Blitter Command Streamer (BCS). En
    lokal användare med rättigheter att använda GPUn kunde utnyttja detta
    för åtkomst till allt minne som GPUn har åtkomst till, vilket kunde
    resultera i överbelastning (minneskorruption eller krasch), läckage
    av känslig information eller utökning av privilegier.</p>

    <p>Denna uppdatering lindrar detta problem genom att lägga till
    säkerhetskontrollen till i915-drivrutinen.</p>

    <p>De påverkade chippen (gen9 och framåt) listas på
    <url "https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen9">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11135">CVE-2019-11135</a>

    <p>Man har upptäckt att på Intel-CPUer med stöd för
    transaktionsminne (TSX), kan en transaktion som är på väg att
    avbrytas forsätta att exekvera spekulativt, och läsa känslig information
    interna buffertar och läcka det genom beroende operationer.
    Intel kallar detta för <q>TSX Asynchronous Abort</q> (TAA).</p>
    
    <p>För CPUer som påverkas av den tidigare publicerade Microarchitectural
    Data Sampling (MDS)-problemet
    (<a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">CVE-2018-12126</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">CVE-2018-12127</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">CVE-2018-12130</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">CVE-2019-11091</a>),
    lindrar den existerande lindringen även detta problem.</p>

    <p>För processorer som är sårbara för TAA men inte MDS, inaktiverar
    denna uppdatering TSX som standard. Denna lindring kräver uppdaterad
    CPU-mikrokod. Ett uppdaterat intel-microcode-paket (som endast finns
    tillgängligt i Debian non-free) kommer att tillhandahållas genom
    DSA 4565-1. Den uppdaterad CPU-miktokoden kan även vara tillgänglig som
    en uppdatering av systemfastprogramvara ("BIOS").</p>
    
    <p>Ytterligare information om lindringen kan hittas på
    <url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/tsx_async_abort.html">
    eller i paketen linux-doc-4.9 eller linux-doc-4.19.</p>

    <p>Intels förklaring av problemet kan hittas på
    <url "https://software.intel.com/security-software-guidance/insights/deep-dive-intel-transactional-synchronization-extensions-intel-tsx-asynchronous-abort">.</p></li>

</ul>

<p>För den gamla stabila utgåvan (Stretch) har dessa problem rättats
i version 4.9.189-3+deb9u2.</p>

<p>För den stabila utgåvan (Buster) har dessa problem rättats i
version 4.19.67-2+deb10u2.</p>

<p>Vi rekommenderar att ni uppgraderar era linux-paket.</p>

<p>För detaljerad säkerhetsstatus om linux vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4564.data"
