#use wml::debian::template title="Personer: vilka vi är och vad vi gör" MAINPAGE="true"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"
#include "$(ENGLISHDIR)/releases/info"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">Hur alltihop började<a>
    <li><a href="#devcont">Utvecklare och bidragslämnare</a>
    <li><a href="#supporters">Individer och organisationer som stödjer Debian</a>
    <li><a href="#users">Debiananvändare</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Eftersom många har frågat:
Debian uttalas <span style="white-space: nowrap;">/&#712;de.bi.&#601;n/.</span> Det är döpt efter skaparen av Debian, Ian Murdock, och hans fru Debra.</p>
</aside>

<h2><a id="history">Hur alltihop började</a></h2>

<p>Det var i augusti 1993, när Ian Murdock började arbeta på ett nytt
operativsystem som skulle skapas öppet, i samma anda som Linux och GNU.
Han skickade ut en öppen inbjudan till andra mjukvaruutvecklare, och bad dom
bidra till en mjukvarudistribution baserad på Linuxkärnan, som var relativt
ny då. Debian var menat att var noggrant och samvetsgrant sammansatt, och
tänkt att underhållas och stödjas med samma omsorg, med en öppen design,
bidrag och stöd från fri mjukvarugemenskapen.</p>

<p>Det startade med en liten, tätt sammansatt grupp av utvecklare av fri
mjukvara och utvecklades gradvis till att bli en stor, välorganiserad gemenskap
av utvecklare, bidragslämnare och användare.</p>


<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="$(DOC)/manuals/project-history/">Läs hela historien</a></button></p>

<h2><a id="devcont">Utvecklare och bidragslämnare</a></h2>

<p>Debian är en organisation baserad på frivillighet. Flera än tusen aktiva 
utvecklare som är utspridda
<a href="$(DEVEL)/developers.loc">över hela världen</a> arbetar på Debian
på sin fritid. 
Få av dessa utvecklarna har faktiskt träffats i verkligheten.
Istället kommunicerar vi primärt genom post (sändlistor på
lists.debian.org) och IRC (kanalen #debian på irc.debian.org).
</p>

<p>Den fullständiga listan över officiella Debianmedlemmar kan hittas på
<a href="https://nm.debian.org/members">nm.debian.org</a> och 
<a href="https://contributors.debian.org">contributors.debian.org</a> visar
en listan på alla bidragslämnare och grupper som jobbar på
Debiandistributionen.</p>

<p>Debianprojektet har en noggrant <a href="organization">organiserad
struktur</a>. För mer information om hur Debian ser ut från insidan,
så är du välkommen att surfa in på <a href="$(DEVEL)/">Debians utvecklarhörna</a>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">Läs om vår filosofi</a></button></p>

<h2><a id="supporters">Individer och organisationer som stödjer Debian</a></h2>

<p>Utöver utvecklare och andra bidragslämnare är många andra individer och 
organisationer del av Debiangemenskapen:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Hosting och hårdvarusponsorer</a></li>
  <li><a href="../mirror/sponsors">Spegelsponsorer</a></li>
  <li><a href="../partners/">Utveckling och tjänstepartners</a></li>
  <li><a href="../consultants">Konsulter</a></li>
  <li><a href="../CD/vendors">Leverantörer av Debians installationsmedia</a></li>
  <li><a href="../distrib/pre-installed">Datorleverantörer som förinstallerar Debian</a></li>
  <li><a href="../events/merchandise">Debianprylar att köpa</a></li>
</ul>

<h2><a name="users">Debiananvändare</a></h2>

<p>
Debian används av en bredd av organisationer, stora och små, så väl som
tusentals individer. Se vår sida <a href="../users/">Vem använder Debian</a>
för en lista över utbildnings-, kommersiella och ideella organisationer
samt statliga myndigheter som har lämnat in korta beskrivningar på hur och
varför dom använder Debian.
</p>

