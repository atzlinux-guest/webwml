#use wml::debian::template title="Anledningar att använda Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="269c9bb8fba913f448196bc64289f477eab6ef9f"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">Debian för användare</a></li>
    <li><a href="#devel">Debian för utvecklare</a></li>
    <li><a href="#enterprise">Debian för företagsmiljöer</a></li>
  </ul>
</div>
 
<p>
Det finns en mängd orsaker att välja Debian som ditt operativsystem -
som användare, utvecklare och till och med i företagsmiljöer. De flesta
uppskattar stabiliteten, och dom smidiga uppgraderingarna av
både paket och hela distributionen. Debian används också av
mjukvaru- och hårdvaruutvecklare eftersom det kör på ett antal
arkitekturer och enheter, tillhandahåller ett publikt felspårningssystem
och andra verktyg för utvecklare. Om du planerar att använda Debian i
en professionell miljö finns det ytterligare fördelar som LTS-versioner
och molnavbildningar.
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span>\
För mig är det den perfekta balansen mellan användarvänlighet och stabilitet.
Jag har använt flera olika distributioner under åren men Debian är den enda
som bara funkar. <a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx?utm_source=share&utm_medium=web2x&context=3">NorhamsFinest på Reddit</a></p>
</aside>

<h2><a id="users">Debian för användare</a></h2>

<p>
<dl>
  <dt><strong>Debian är fri mjukvara.</strong></dt>
  <dd>
    Debian är skapat av fri mjukvara och mjukvara med öppen källkod och kommer
    alltid att vara 100% <a href="free">fritt</a>. Fritt för alla att använda,
    modifiera, och distribuera. Detta är vårt huvudsakliga löfte till
    <a href="../users">våra användare</a>. Det är även kostnadsfritt.
  </dd>
</dl>

<dl>
  <dt><strong>Debian är stabilt och säkert.</strong></dt>
  <dd>
      Debian är ett Linuxbaserat operativsystem för en bredd av enheter inklusive
      laptops, arbetsstationer och servrar. Vi tillhandahåller en rimlig
      standardkonfiguration för varje paket så väl som regelbundna
      säkerhetsuppdateringar under paketens livstid.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har omfattande hårdvarustöd.</strong></dt>
  <dd>
    Det mesta av alla hårdvara stöds redan av Linuxkärnan vilket i sin tur
    betyder att Debian kommer att stödja den. Proprietära drivrutiner
    för hårdvara finns tillgänglig om det är nödvändigt.
  </dd>
</dl>

<dl>
  <dt><strong>Debian erbjuder en flexibel installerare.</strong></dt>
  <dd>
      Vår <a
    href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live
    CD</a> är för alla som vill prova på Debian före dom installerar det. Den
    inkluderar även en Calamares-installerare som gör det lätt att installera
    Debian från Live-systemet. Mer erfarna användare kan använda
    Debianinstalleraren som har fler alternativ och finjusteringar,
    inklusive möjligheten att använda ett automatiserat nätverksinstallationsverktyg.
  </dd>
</dl>

<dl>
  <dt><strong>Debian tillhandahåller smidiga uppgraderingar.</strong></dt>
  <dd>
      Det är lätt att hålla vårt operativsystem uppdaterat, oberoende på om
      du vill uppgradera till en helt ny utgåva eller bara uppgradera ett
      enstaka paket.
  </dd>
</dl>

<dl>
  <dt><strong>Debian är basen för många andra distributioner</strong></dt>
  <dd>
    Många av de mest populära Linuxdistributionerna som Ubuntu, Knoppix, PureOS,
    eller Tails, är baserade på Debian. Vi tillhandahåller alla verktyg
    så att alla kan utöka mjukvarupaketen från Debianarkivet med deras egna
    paket för deras behov.
  </dd>
</dl>

<dl>
  <dt><strong>Debianprojektet är en gemenskap.</strong></dt>
  <dd>
      Alla kan var en del av vår gemenskap; du behöver inte vara en utvecklare
      eller en systemadministratör. Debian har en <a
      href="../devel/constitution">demokratisk ledningsstruktur</a>. Eftersom
      alla medlemmar av Debianprojektet har samma rättigheter kan inte
      Debian styras av ett enda företag. Våra utvecklare är från 60 olika
      länder, och Debian självt översätts till mer än 80 språk.
  </dd>
</dl>
</p>


<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Orsaken att Debian har
ett rykte som ett operativsystem för utvecklare är det stora antalet paket
och fantastiska mjukvarustöd, vilket är viktigt för utvecklare. Det
rekommenderas starkt för avancerade programmerare och systemutevecklare. <a
href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh
Verma, Fossbytes</a></p>
</aside>

<h2><a id="devel">Debian för utvecklare</a></h2>


<dl>
  <dt><strong>Flera hårdvaruarkitekturer.</strong></dt>
  <dd>
    Stöd för en <a href="../ports">lång lista</a> på CPU-arkitekturer
    inklusive amd64, i386, flera versioner av ARM och MIPS, POWER7, POWER8,
    IBM System z, RISC-V. Debian finns även tillgängligt för äldre och
    specifika nichearkitekturer.
  </dd>
</dl>


<dl>
  <dt><strong>IoT och inbäddade enheter.</strong></dt>
  <dd>
    Vi ger stöd till en bredd av enheter så som Raspberry Pi, varianter
    av QNAP, mobila enheter, hemrouters och en mängd enkortsdatorer (SBC).
  </dd>
</dl>


<dl>
  <dt><strong>Enormt antal mjukvarupaket tillgängliga</strong></dt>
  <dd>
    Debian har ett stort antal <a href="$(DISTRIB)/packages">paket</a> tillgängliga
    (för närvarande <packages_in_stable>) som använder <a
    href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">deb
    formatet</a>.
  </dd>
</dl>

<dl>
  <dt><strong>Olika utgåvor.</strong></dt>
  <dd>
    Förutom vår stabila utgåva, kan du installera nyare mjukvara geneom att 
    använda uttestningsutgåvan eller den instabila utgåvan.
  </dd>
</dl>


<dl>
  <dt><strong>Publikt tillgängligt felspårningssystem.</strong></dt>
  <dd>
    Vårt <a href="../Bugs">felspårningssystem</a> (Bug tracking system - BTS)
    är publikt tillgängligt för alla via en webbläsare. Vi gömmer inte våra
    mjukvarufel och du kan enkelt skicka nya felrapporter.
  </dd>
</dl>


<dl>
  <dt><strong>Debian Policy och utvecklarverktyg</strong></dt>
  <dd>
      Debian erbjuder högkvalitativ mjukvara. För att lära sig mer om våra
      standarder, läs <a href="../doc/debian-policy/">policy</a> som
      definierar tekniska krav på varje paket som inkluderas i
      distributionen. Vår Contrinuous Integration-strategi inkluderar
      Autopkgtest (kör tester på paket), Piuparts (testar installation,
      uppgradering och borttagning), och Lintian (kontrollerar paketen
      för inkonsekvenser och fel).
  </dd>
</dl> 
</p>


<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Stabilitet är synonymt med 
Debian. [...]  Säkerhet är en av de mest viktiga Debianfunktionerna. <a
href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">\
Christos Pontikis på pontikis.net</a></p>
</aside>


<h2><a id="enterprise">Debian för företagsmiljö</a></h2>

<p>
<dl>
  <dt><strong>Debian är pålitligt.</strong></dt>
  <dd>
    Debian bevisar sin pålitlighet varje dag i tusentals verkliga
    scenarier från en laptop med en användare till superkolliderare,
    aktiemarknader och fordonsindustrin. Det är även populärt inom
    akademiska världen, inom vetenskap och i den offentliga sektorn.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har många experter.</strong></dt>
  <dd>
    Våra paketansvariga tar inte bara hand om Debianpaketering och
    nya uppströmsversioner. Ofta är de experter i uppströmsmjukvaran
    och bidrar till uppströmsutveckling direkt. Ibland är de även
    en del av uppströmsmjukvaran.
  </dd>
</dl>

<dl>
  <dt><strong>Debian är säkert.</strong></dt>
  <dd>
    Debian har säkerhetsstöd för dess stabila utgåvor. Många andra
    distributioner och säkerhetsforskare är beroende av Debians
    säkerhetsspårare.
  </dd>
</dl>

<dl>
  <dt><strong>Långtidsstöd.</strong></dt>
  <dd>
    Det <a href="https://wiki.debian.org/LTS">Långtidsstöd</a>
    (Long Term Support - LTS) utan extra kostnad. Detta ger dig
    utökat stöd för den stabila utgåvan i 5 år och mer. Utöver detta
    finns det även initiativet för <a 
    href="https://wiki.debian.org/LTS/Extended">Utökad LTS</a> som
    förlänger stödet för en begränsad uppsättning paket i mer än 5 år.
  </dd>
</dl>

<dl>
  <dt><strong>Molnavbildningar.</strong></dt>
  <dd>
    Officiella molnavbildningar finns tillgängliga för alla större
    molnplattformar. Vi tillhandahåller även verktygen och konfigurationen så
    att du kan bygga din egen anpassade molnavbildning. Du kan även använda
    Debian i virtuella maskiner på skrivbordet eller i en behållare.
  </dd>
</dl>
<p>



