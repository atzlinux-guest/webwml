# countries.cs.po
# Copyright (C) 2004 Free Software Foundation, Inc.
# Michal Simunek <michal.simunek@gmail.com>, 2009 - 2011
#
msgid ""
msgstr ""
"Project-Id-Version: countries 1.25\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2015-04-18 14:10+0200\n"
"Last-Translator: Michal Simunek <michal.simunek@gmail.com>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Spojené arabské emiráty"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "Albánie"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Arménie"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Argentina"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Rakousko"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Austrálie"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Bosna a Hercegovina"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Bangladéš"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "Belgie"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Bulharsko"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Brazílie"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Bahamy"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Bělorusko"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Kanada"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Švýcarsko"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Chile"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "Čína"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Kolumbie"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Kostarika"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "Česká republika"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Německo"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Dánsko"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "Dominikánská republika"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "Alžírsko"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Ekvádor"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Estonsko"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Egypt"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "Španělsko"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "Etiopie"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Finsko"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Faerské ostrovy"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "Francie"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Spojené království"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Grenada"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Gruzie"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Grónsko"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Řecko"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Guatemala"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Hong Kong"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Honduras"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Chorvatsko"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Maďarsko"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "Indonésie"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "Írán"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Irsko"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Izrael"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "Indie"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "Island"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Itálie"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Jordánsko"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Japonsko"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Keňa"

#: ../../english/template/debian/countries.wml:267
#, fuzzy
msgid "Kyrgyzstan"
msgstr "Kazachstán"

#: ../../english/template/debian/countries.wml:270
msgid "Cambodia"
msgstr ""

#: ../../english/template/debian/countries.wml:273
msgid "Korea"
msgstr "Korea"

#: ../../english/template/debian/countries.wml:276
msgid "Kuwait"
msgstr "Kuvajt"

#: ../../english/template/debian/countries.wml:279
msgid "Kazakhstan"
msgstr "Kazachstán"

#: ../../english/template/debian/countries.wml:282
msgid "Sri Lanka"
msgstr "Srí Lanka"

#: ../../english/template/debian/countries.wml:285
msgid "Lithuania"
msgstr "Litva"

#: ../../english/template/debian/countries.wml:288
msgid "Luxembourg"
msgstr "Lucembursko"

#: ../../english/template/debian/countries.wml:291
msgid "Latvia"
msgstr "Lotyšsko"

#: ../../english/template/debian/countries.wml:294
msgid "Morocco"
msgstr "Maroko"

#: ../../english/template/debian/countries.wml:297
msgid "Moldova"
msgstr "Moldavsko"

#: ../../english/template/debian/countries.wml:300
msgid "Montenegro"
msgstr "Černá Hora"

#: ../../english/template/debian/countries.wml:303
msgid "Madagascar"
msgstr "Madagaskar"

#: ../../english/template/debian/countries.wml:306
msgid "Macedonia, Republic of"
msgstr "Makedonie, republika"

#: ../../english/template/debian/countries.wml:309
msgid "Mongolia"
msgstr "Mongolsko"

#: ../../english/template/debian/countries.wml:312
msgid "Malta"
msgstr "Malta"

#: ../../english/template/debian/countries.wml:315
msgid "Mexico"
msgstr "Mexiko"

#: ../../english/template/debian/countries.wml:318
msgid "Malaysia"
msgstr "Malajsie"

#: ../../english/template/debian/countries.wml:321
msgid "New Caledonia"
msgstr "Nová Kaledonie"

#: ../../english/template/debian/countries.wml:324
msgid "Nicaragua"
msgstr "Nikaragua"

#: ../../english/template/debian/countries.wml:327
msgid "Netherlands"
msgstr "Nizozemí"

#: ../../english/template/debian/countries.wml:330
msgid "Norway"
msgstr "Norsko"

#: ../../english/template/debian/countries.wml:333
msgid "New Zealand"
msgstr "Nový Zéland"

#: ../../english/template/debian/countries.wml:336
msgid "Panama"
msgstr "Panama"

#: ../../english/template/debian/countries.wml:339
msgid "Peru"
msgstr "Peru"

#: ../../english/template/debian/countries.wml:342
msgid "French Polynesia"
msgstr "Francouzská Polynésie"

#: ../../english/template/debian/countries.wml:345
msgid "Philippines"
msgstr "Filipíny"

#: ../../english/template/debian/countries.wml:348
msgid "Pakistan"
msgstr "Pákistán"

#: ../../english/template/debian/countries.wml:351
msgid "Poland"
msgstr "Polsko"

#: ../../english/template/debian/countries.wml:354
msgid "Portugal"
msgstr "Portugalsko"

#: ../../english/template/debian/countries.wml:357
msgid "Réunion"
msgstr ""

#: ../../english/template/debian/countries.wml:360
msgid "Romania"
msgstr "Rumunsko"

#: ../../english/template/debian/countries.wml:363
msgid "Serbia"
msgstr "Srbsko"

#: ../../english/template/debian/countries.wml:366
msgid "Russia"
msgstr "Rusko"

#: ../../english/template/debian/countries.wml:369
msgid "Saudi Arabia"
msgstr "Saudská Arábie"

#: ../../english/template/debian/countries.wml:372
msgid "Sweden"
msgstr "Švédsko"

#: ../../english/template/debian/countries.wml:375
msgid "Singapore"
msgstr "Singapur"

#: ../../english/template/debian/countries.wml:378
msgid "Slovenia"
msgstr "Slovinsko"

#: ../../english/template/debian/countries.wml:381
msgid "Slovakia"
msgstr "Slovensko"

#: ../../english/template/debian/countries.wml:384
msgid "El Salvador"
msgstr "Salvador"

#: ../../english/template/debian/countries.wml:387
msgid "Thailand"
msgstr "Thajsko"

#: ../../english/template/debian/countries.wml:390
msgid "Tajikistan"
msgstr "Tádžikistán"

#: ../../english/template/debian/countries.wml:393
msgid "Tunisia"
msgstr "Tunisko"

#: ../../english/template/debian/countries.wml:396
msgid "Turkey"
msgstr "Turecko"

#: ../../english/template/debian/countries.wml:399
msgid "Taiwan"
msgstr "Tchai-wan"

#: ../../english/template/debian/countries.wml:402
msgid "Ukraine"
msgstr "Ukrajina"

#: ../../english/template/debian/countries.wml:405
msgid "United States"
msgstr "Spojené státy"

#: ../../english/template/debian/countries.wml:408
msgid "Uruguay"
msgstr "Uruguay"

#: ../../english/template/debian/countries.wml:411
msgid "Uzbekistan"
msgstr "Uzbekistán"

#: ../../english/template/debian/countries.wml:414
msgid "Venezuela"
msgstr "Venezuela"

#: ../../english/template/debian/countries.wml:417
msgid "Vietnam"
msgstr "Vietnam"

#: ../../english/template/debian/countries.wml:420
msgid "Vanuatu"
msgstr "Vanuatu"

#: ../../english/template/debian/countries.wml:423
msgid "South Africa"
msgstr "Jižní Afrika"

#: ../../english/template/debian/countries.wml:426
msgid "Zimbabwe"
msgstr "Zimbabwe"

#~ msgid "Great Britain"
#~ msgstr "Velká Británie"
