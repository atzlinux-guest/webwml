<define-tag pagetitle>Lançamento do Instalador Debian Bullseye Alpha 3</define-tag>
<define-tag release_date>2020-12-06</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="1d19e4a312c029bd7140e49d3b9ba543127f44c2"

<p>
A <a
href="https://wiki.debian.org/DebianInstaller/Team">equipe</a> Debian Installer
(Instalador Debian) tem o prazer de anunciar o terceiro lançamento
alpha do instalador para o Debian 11 <q>Bullseye</q>.
</p>


<h2>Aperfeiçoamentos neste lançamento</h2>

<ul>
  <li>apt-setup:
    <ul>
      <li>Remove a menção de repositório volatile do arquivo sources.list
        gerado (<a href="https://bugs.debian.org/954460">#954460</a>).</li>
    </ul>
  </li>
  <li>base-installer:
    <ul>
      <li>Melhora do teste de arquitetura, adicionando suporte para as versões
	Linux 5.x.</li>
    </ul>
  </li>
  <li>brltty:
    <ul>
      <li>Melhora na detecção de hardware e no suporte a driver.</li>
    </ul>
  </li>
  <li>cdebconf:
    <ul>
      <li>A interface de texto informa o progresso de forma mais precisa: 
        desde o início, e também assim que uma resposta for dada.</li>
    </ul>
  </li>
  <li>choose-mirror:
    <ul>
      <li>Atualizada Mirrors.masterlist.</li>
    </ul>
  </li>
  <li>console-setup:
    <ul>
      <li>Melhora do suporte a caracteres para desenho de caixas (<a href="https://bugs.debian.org/965029">#965029</a>).</li>
      <li>Sincronização da fonte Terminus com o pacote xfonts-terminus.</li>
      <li>Correção do layout lituano (<a href="https://bugs.debian.org/951387">#951387</a>).</li>
    </ul>
  </li>
  <li>debian-cd:
    <ul>
      <li>Inclui apenas udebs Linux para o ABI mais recente, tornando imagens
        menores de instalação mais úteis.</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Atualizado kernel Linux ABI para 5.9.0-4</li>
      <li>Descartados ajustes no fontconfig introduzidos no lançamento do
        instalador Debian Buster Alpha 1 (veja <a href="https://bugs.debian.org/873462">#873462</a>).</li>
      <li>Instala kmod-udeb em vez de libkmod2-udeb.</li>
      <li>Emula a manipulação da libgcc1, para libgcc-s1.</li>
      <li>Limpeza da lista de pacotes falsos.</li>
      <li>Substituição do pass de redução da biblioteca mklibs library com um 
	hack, copiando libgcc_s.so.[124] do sistema de arquivos do host por
	enquanto.</li>
      <li>Adiciona de forma explícita build-depends para fdisk em arm64, amd64 e
        i386, agora que util-linux não depende mais disso.</li>
      <li>Adicionado grub2 no built-using (<a href="https://bugs.debian.org/968998">#968998</a>).</li>
      <li>Correção do FTBFS com fakeroot através de ajuste na checagem do
        /dev/console (veja <a href="https://bugs.debian.org/940056">#940056</a>).</li>
    </ul>
  </li>
  <li>debian-installer-utils:
    <ul>
      <li>Ajuste no uso de descritores de arquivos pelo fetch-url para as 
	versões mais recentes do udev (<a href="https://bugs.debian.org/967546">#967546</a>).</li>
    </ul>
  </li>
  <li>debootstrap:
    <ul>
      <li>Instala apenas apt-transport-https no stretch e anteriores,
        suporte HTTPS foi incorporado no núcleo do pacote apt para o buster
        (<a href="https://bugs.debian.org/920255">#920255</a>, <a href="https://bugs.debian.org/879755">#879755</a>).</li>
    </ul>
  </li>
  <li>finish-install:
    <ul>
      <li>Descarta completamente o suporte ao upstart (<a href="https://bugs.debian.org/923845">#923845</a>).</li>
    </ul>
  </li>
  <li>fonts-noto:
    <ul>
      <li>Corrige o suporte a cingalês na instalação (<a href="https://bugs.debian.org/954948">#954948</a>).</li>
    </ul>
  </li>
  <li>grub-installer:
    <ul>
      <li>Atualização de modelos para ajustá-los a sistemas UEFI e novos tipos
	de mídia de armazenamento de sistema (<a href="https://bugs.debian.org/954718">#954718</a>).</li>
    </ul>
  </li>
  <li>kmod:
    <ul>
      <li>Divisão de kmod-udeb a partir da libkmod2-udeb e distribui de fato as
        bibliotecas na libkmod2-udeb (<a href="https://bugs.debian.org/953952">#953952</a>).</li>
    </ul>
  </li>
  <li>locale-chooser:
    <ul>
      <li>Novas línguas ativadas: cabila, occitana.</li>
    </ul>
  </li>
  <li>partman-auto:
    <ul>
      <li>Incrementar o tamanho de /boot na maioria das receitas entre 128 a
        256M para 512 a 768M (<a href="https://bugs.debian.org/893886">#893886</a>, <a href="https://bugs.debian.org/951709">#951709</a>).</li>
      <li>Importar o suporte partman-auto/cap-ram do Ubuntu, para permitir 
        limitar o tamanho da RAM utilizada para o cálculo de partições de swap 
        (<a href="https://bugs.debian.org/949651">#949651</a>, <a href="https://bugs.debian.org/950344">#950344</a>).
        Isso nos permite limitar o tamanho mínimo de partições de swap para 
        1*CAP, e o tamanho máximo de 2 ou 3*CAP dependendo da arquitetura. O 
	padrão foi configurado para 1024, limitando dessa forma as partições de
	swap entre 1 e 3GB.</li>
    </ul>
  </li>
  <li>partman-efi:
    <ul>
      <li>Remonta /cdrom como leitura-escrita se também for utilizado como 
        /boot/efi (<a href="https://bugs.debian.org/967918">#967918</a>).</li>
      <li>Remove a utilização do módulo efivar e suspende a busca por
        /proc/efi. efivarfs é a interface atual e /proc/efi
        foi deixado de lado há muito tempo.</li>
    </ul>
  </li>
  <li>partman-partitioning:
    <ul>
      <li>Inclui ntfs-3g-udeb para arm64.</li>
    </ul>
  </li>
  <li>partman-target:
    <ul>
      <li>Adicionada dica para a nova fstab sobe o uso do <code>systemctl daemon-reload</code>
        após alteração do /etc/fstab (<a href="https://bugs.debian.org/963573">#963573</a>).</li>
    </ul>
  </li>
  <li>systemd:
    <ul>
      <li>Instala 60-block.rules no udev-udeb (<a href="https://bugs.debian.org/958397">#958397</a>). 
	As regras de dispositivo de bloco foram separadas de
	60-persistent-storage.rules na v220. Isso corrige um bug onde UUIDs não
	seriam utilizadas para sistemas de arquivos na instalação inicial.</li>
    </ul>
  </li>
  <li>util-linux:
    <ul>
      <li>Toma o lugar de eject-udeb (<a href="https://bugs.debian.org/737658">#737658</a>).</li>
    </ul>
  </li>
  <li>win32-loader:
    <ul>
      <li>Introduz o gerenciamento de boot UEFI e suporte a Secure Boot 
        (<a href="https://bugs.debian.org/918863">#918863</a>).</li>
    </ul>
  </li>
</ul>


<h2>Mudanças no suporte a hardware</h2>

<ul>
  <li>debian-cd:
    <ul>
      <li>Habilitada instalação em ambiente gráfico para arm64.</li>
      <li>Exclusão dos udebs lilo-installer e elilo-installer em todas as
        arquiteturas.</li>
      <li>Encerramento das imagens de CD único para XFCE.</li>
      <li>Encerramento das imagens DVD ISO 2 e 3 para amd64/i386 (ainda
        disponíveis via	jigdo).</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Atualização da imagem Firefly-RK3288 para a nova versão u-boot.</li>
      <li>[arm64] Adicionado suporte firefly-rk3399, pinebook-pro-rk3399,
        rockpro64-rk3399, rock64-rk3328 e rock-pi-4-rk3399 para imagens u-boot
        e imagens netboot em cartão SD.</li>
      <li>[arm64] Faz todas as imagens sdcard netboot iniciarem no offset
        32768, para compatibilidade com a plataforma rockchip.</li>
      <li>Adicionado suporte a laptop OLPC XO-1.75 (<a href="https://bugs.debian.org/949306">#949306</a>).</li>
      <li>Habilitado build GTK para arm64.</li>
      <li>Suporte adicionado para NanoPi NEO Air (<a href="https://bugs.debian.org/928863">#928863</a>).</li>
      <li>Adicionado wireless-regdb-udeb para construções Linux que incluam
        nic-wireless-modules.</li>
      <li>efi-image: Melhoria no cálculo de tamanho para diminuir o desperdício
        de espaço.</li>
      <li>efi-image: Inclusão de arquivos DTB no ESP para sistemas armhf e
        arm64. Isso fará que sistemas baseados em U-Boot funcionem melhor quando
        inicializados via UEFI.</li>
    </ul>
  </li>
  <li>flash-kernel:
    <ul>
      <li>Adicionado FriendlyARM NanoPi NEO Plus2 (<a href="https://bugs.debian.org/955374">#955374</a>).</li>
      <li>Adicionado Pinebook (<a href="https://bugs.debian.org/930098">#930098</a>).</li>
      <li>Adicionado Pinebook Pro.</li>
      <li>Adicionado Olimex A64-Olinuxino e A64-Olinuxino-eMMC
        (<a href="https://bugs.debian.org/931195">#931195</a>).</li>
      <li>Adicionado SolidRun LX2160A Honeycomb e Clearfog CX
        (<a href="https://bugs.debian.org/958023">#958023</a>).</li>
      <li>Adicionadas variantes SolidRun Cubox-i Solo/DualLite (<a href="https://bugs.debian.org/939261">#939261</a>).</li>
      <li>Adicionado Turris MOX (<a href="https://bugs.debian.org/961303">#961303</a>).</li>
    </ul>
  </li>
  <li>linux:
    <ul>
      <li>Movidos módulos de compressão para udeb kernel-image; descartado o
        udeb compress-modules.</li>
      <li>Torna o udeb input-modules dependente de crc-modules.</li>
      <li>[arm64] Adicionado udeb i2c_mv64xxx a i2c-modules.</li>
      <li>[arm64] Adicionado udeb drivers/pinctrl a kernel-image.</li>
      <li>[arm64] Adicionado analogix-anx6345, pwm-sun4i, sun4i-drm e
        sun8i-mixer ao udeb fb-modules.</li>
      <li>[arm64] Adicionado pwm-sun4i ao udeb fb-modules.</li>
      <li>[arm64] Adicionado armada_37xx_wdt ao udeb kernel-image
        (<a href="https://bugs.debian.org/961086">#961086</a>).</li>
      <li>[mips*] Descarte do udeb hfs-modules.</li>
      <li>[x86] Adicionado crc32_pclmul ao udeb crc-modules.</li>
      <li>Adicionado crc32_generic ao udeb crc-modules.</li>
      <li>Inversão da ordem dos udebs cdrom-core e isofs/udf: o último agora
        requer o primeiro.</li>
      <li>Descarte do udeb zlib-modules (zlib_deflate está sempre incluso agora).</li>
      <li>Adicionado udeb f2fs-modules.</li>
    </ul>
  </li>
</ul>


<h2>Status de localização</h2>

<ul>
  <li>78 línguas são suportadas neste lançamento.</li>
  <li>Novas línguas: cabila, occitana.</li>
  <li>Tradução completa para 16 delas.</li>
</ul>


<h2>Problemas conhecidos neste lançamento</h2>

<p>
Veja a <a href="$(DEVEL)/debian-installer/errata">errata</a> para
delahes e uma listagem completa dos problemas conhecidos.
</p>


<h2>Feedback para este lançamento</h2>

<p>
Precisamos da sua ajuda para achar bugs e melhorar ainda mais o instalador,
então, por favor teste-o. CDs de instalação, outras mídias e qualquer coisa que
você precisar estão disponíveis em nosso
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Agradecimentos</h2>

<p>
O time Debian Installer agradece a todas as pessoas que contribuíram para este
lançamento.
</p>
