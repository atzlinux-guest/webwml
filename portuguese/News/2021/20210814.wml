<define-tag pagetitle>Debian 11 <q>bullseye</q> lançado</define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff" maintainer="Thiago Pezzo (tico), Charles Melara"


<p>Após 2 anos, 1 mês e 9 dias de desenvolvimento, o projeto
Debian orgulhosamente apresenta sua nova versão estável (stable) 11 (codinome
<q>bullseye</q>), a qual será suportada pelos próximos 5 anos graças ao esforço
combinado da
<a href="https://security-team.debian.org/">Equipe de Segurança</a>
e da <a href="https://wiki.debian.org/LTS">Equipe de Suporte de Longo Prazo
do Debian.</a>
</p>

<p>
O Debian 11 <q>bullseye</q> é distribuído com diversas aplicações e ambientes de
área de trabalho. Entre outros, estão incluídos os seguintes ambientes de área
de trabalho:
</p>
<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24,</li>
<li>Xfce 4.16.</li>
</ul>


<p>Este lançamento contém 11.294 novos pacotes de um total de 59.551
pacotes, junto à uma redução significativa de 9.519 pacotes que foram marcados
como <q>obsoletos</q> e removidos. Foram 42.821 pacotes atualizados e 5.434
pacotes permaneceram sem alterações.
</p>

<p>
O <q>bullseye</q> é nosso primeiro lançamento que fornece um núcleo Linux com
suporte ao sistema de arquivos exFAT e que permite por padrão utilizá-lo para
montar sistemas de arquivo exFAT. Consequentemente, não é mais requerido usar a
implementação sistema-de-arquivo-no-espaço-de-usuário(a) através do
pacote exfat-fuse. As ferramentas para criar e verificar um sistema de arquivos
exFAT são fornecidas no pacote exfatprogs.
</p>


<p>
Impressoras mais modernas são capazes de utilizar impressão e escaneamento sem
drivers, sem a necessidade de drivers de fabricantes específicos (frequentemente
não livres).

O <q>bullseye</q> apresenta um novo pacote, ipp-usb, que usa o protocolo IPP-sobre-USB,
neutro em relação a fabricantes e suportado por muitas impressoras modernas.
Isto permite que um dispositivo USB seja tratado como um dispositivo de rede. A
infraestrutura oficial e sem driver SANE é fornecida por sane-escl em libsane1,
que usa o protocolo eSCL.
</p>

<p>
O Systemd no <q>bullseye</q> ativa sua funcionalidade journal
persistente, por padrão, com uma salvaguarda implícita para armazenamento
volátil. Isto permite que usuários(as) que não estejam amparados(as) por
recursos especiais possam desinstalar daemons tradicionais de log e
trocá-los usando somente o journal do Systemd.
</p>

<p>
A equipe Debian Med tem participado da luta contra o COVID-19
empacotando software para pesquisa e sequenciamento do vírus, e lutando
contra a pandemia com ferramentas usadas na epidemiologia. Esse trabalho
de empacotamento continuará com o foco em ferramentas de ambos os campos
que utilizam aprendizado de máquina. Seu trabalho junto à equipe de
Garantia de Qualidade e Integração Contínua é crítica para os resultados
reproduzíveis e consistentes que são exigidos pelas ciências.

O Blend Debian Med possui uma variedade de aplicações críticas de desempenho
que agora se beneficiam do SIMD Everywhere. Para instalar pacotes mantidos
pela equipe Debian Med, instale os metapacotes med-*, os quais estão na
versão 3.6.x.
</p>

<p>
Chinês, japonês, coreano e muitos outros idiomas agora têm um novo método de
entrada Fcitx 5, que é o sucessor do popular Fcitx4 no buster. Esta nova versão
tem um suporte muito melhor de extensão ao Wayland (gerenciador de exibição
padrão).
</p>

<p>
O Debian 11 <q>bullseye</q> inclui muitos pacotes de software atualizados (mais
de 72% de todos os pacotes da versão anterior), tais como:
</p>
<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>kernel Linux série 5.10</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>mais de 59.000 pacotes de software prontos para usar, construídos a partir
de mais de 30.000 pacotes-fonte.</li>
</ul>

<p>
Com esta ampla seleção de pacotes e seu vasto e tradicional suporte a
arquiteturas, o Debian mais uma vez mantém-se verdadeiro a seu objetivo de ser
<q>O Sistema Operacional universal</q>. Ele é adequado para muitos usos: de sistemas
desktops a netbooks; de servidores de desenvolvimento a sistemas em cluster; e
para servidores de bancos de dados, web e de armazenamento. Ao mesmo tempo,
esforços adicionais de garantia de qualidade, como testes de instalação
e atualização automáticos para todos os pacotes do repositório Debian, asseguram
que o <q>bullseye</q> alcance as elevadas expectativas que os(as) usuários(as)
têm de uma versão Debian estável.
</p>

<p>
Um total de nove arquiteturas são suportadas:
64-bit PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-bit PC / Intel IA-32 (<code>i386</code>),
64-bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bit IBM S/390 (<code>s390x</code>),
para ARM, <code>armel</code>
e <code>armhf</code> para hardware 32-bit mais antigos e mais recentes,
mais <code>arm64</code> para a arquitetura 64-bit <q>AArch64</q>,
e para MIPS, arquitetura <code>mipsel</code> (little-endian) para hardware 32-bit
e arquitetura <code>mips64el</code> para hardware 64-bit little-endian.
</p>

<h3>Quer experimentar?</h3>
<p>
Se você simplesmente quer experimentar o Debian 11 <q>bullseye</q> sem
instalá-lo, você pode usar uma das <a href="$(HOME)/CD/live/">imagens live</a>
disponíveis, que carregam e executam o sistema operacional inteiro em um
estado de somente leitura através da memória do computador.
</p>

<p>
Essas imagens live são fornecidas para as arquiteturas <code>amd64</code> e
<code>i386</code>, e estão disponíveis em DVDs, pendrives USB
e configurações netboot. O(A) usuário(a) pode escolher diferentes
ambientes de área de trabalho para testar: GNOME, KDE Plasma, LXDE, LXQt, MATE e
Xfce. O Debian Live <q>bullseye</q> tem uma imagem live padrão, então também é
possível experimentar um sistema Debian básico sem qualquer interface gráfica
de usuário(a).
</p>

<p>
Se gostar do sistema operacional, você tem a opção de instalar a partir da
imagem live no disco rígido do seu computador. A imagem live inclui o instalador
independente Calamares, como também o instalador Debian padrão.
Mais informações estão disponíveis nas
<a href="$(HOME)/releases/bullseye/releasenotes">notas de lançamento</a> e nas
seções de <a href="$(HOME)/CD/live/">imagens de instalação live</a>  do
site web do Debian.
</p>

<p>
Para instalar o Debian 11 <q>bullseye</q> diretamente no
disco rígido do computador, você pode escolher dentre uma variedade de mídias
de instalação como disco Blu-ray, DVD, CD, pendrive USB, ou via uma conexão de
rede. Diversos ambientes de área de trabalho &mdash; Cinnamon, GNOME, KDE Plasma
Desktop e Applications, LXDE, LXQt, MATE e Xfce &mdash; podem ser instalados
através dessas imagens.
Além disso, CDs <q>multiarquitetura</q> estão disponíveis, os quais suportam
a instalação de diversas arquiteturas a partir de um único disco. Ou você pode
criar mídias de instalação USB inicializáveis
(veja o <a href="$(HOME)/releases/bullseye/installmanual">guia de instalação</a>
para mais detalhes).
</p>

# Translators: some text taken from /devel/debian-installer/News/2021/20210802

<p>
Houve muito desenvolvimento no instalador do Debian, resultando em suporte de
hardware aprimorado e outros novos recursos.
</p> 

<p>
Em alguns casos, uma instalação bem-sucedida ainda pode apresentar problemas de
exibição ao reinicializar no sistema instalado; para esses casos, existem
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">algumas soluções alternativas</a>
que podem ajudar a fazer login de qualquer maneira.
Há também um
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">procedimento baseado em isenkram</a>
que permite aos usuários detectar e corrigir firmware ausente em seus sistemas,
de forma automatizada. Claro, é preciso pesar os prós e os contras de usar essa
ferramenta, pois é muito provável que ela precisará instalar pacotes não-livres.</p> 

<p>
  Além disso, as
  <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">imagens de instalação não-livres que incluem pacotes de firmware</a>
  foram aprimorados para que possam prever a necessidade de firmware no sistema
  instalado (por exemplo, firmware para placas gráficas AMD ou Nvidia, ou novas
  gerações de hardware de áudio Intel).
</p> 

<p>
Para usuários(as) de nuvem, o Debian oferece suporte direto para muitas das
mais conhecidas plataformas de nuvem. Imagens oficiais do Debian são selecionadas
facilmente através de cada marketplace. O Debian também publica <a
href="https://cloud.debian.org/images/openstack/current/">imagens
OpenStack pré-construídas</a> para arquiteturas <code>amd64</code> e
<code>arm64</code>, prontas para download e utilização em configurações locais
em nuvem.
</p>

<p>
O Debian agora pode ser instalado em 76 idiomas, e a maioria deles disponíveis
tanto em interfaces de usuário(a) baseadas em texto quanto gráficas.
</p>

<p>
As imagens de instalação podem ser baixadas diretamente via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (o método recomendado),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> ou
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; veja
<a href="$(HOME)/CD/">Debian em CDs</a> para mais informações. O <q>bullseye</q>
logo estará disponível em discos físicos de DVD, CD-ROM e Blu-ray por
diversos(as) <a href="$(HOME)/CD/vendors">fabricantes</a>.
</p>


<h3>Atualizando o Debian</h3>
<p>
As atualizações para o Debian 11 da versão anterior, Debian 10
(codinome <q>buster</q>), são automaticamente tratadas pela ferramenta de
gerenciamento de pacotes APT na maior parte das configurações.
</p>

<p>
Para o bullseye, o repositório de segurança agora é denominado bullseye-security
e os(as) usuários(as) devem adaptar seus arquivos source-list do APT de acordo
com a atualização.
Se sua configuração do APT também envolve pinning ou <code>APT::Default-Release</code>,
é provável que também exija ajustes. Veja a seção
<a href="https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#security-archive">alteração do layout do arquivo de segurança</a>
das notas de lançamento para mais detalhes.
</p> 

<p>
Se você estiver atualizando remotamente, esteja ciente da seção
<a href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available">nenhuma nova conexão SSH possível durante a atualização</a>.
</p> 

<p>
Como sempre, sistemas Debian podem ser atualizados sem dores de cabeça,
imediatamente, sem tempos forçados de inatividade. Mas recomenda-se
fortemente que
as <a href="$(HOME)/releases/bullseye/releasenotes">notas de lançamento</a>,
e o <a href="$(HOME)/releases/bullseye/installmanual">guia de
instalação</a> sejam lidos para identificar possíveis problemas e para fornecer
instruções detalhadas sobre instalações e atualizações. As notas de lançamento
serão melhoradas futuramente e traduzidas para idiomas adicionais nas semanas
posteriores ao lançamento.
</p>


<h2>Sobre o Debian</h2>

<p>
O Debian é um sistema operacional livre, desenvolvido por
milhares de voluntários(as) de todo o mundo que colaboram através da
Internet. Os pontos fortes do projeto Debian são sua base de voluntários(as),
sua dedicação ao Contrato Social do Debian e ao Software Livre, e seu
compromisso em fornecer o melhor sistema operacional possível. Esta nova
versão é mais um passo importante nessa direção.
</p>


<h2>Informações para contato</h2>

<p>
Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a> ou envie um e-mail em inglês para
&lt;press@debian.org&gt;.
</p>
