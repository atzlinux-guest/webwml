#use wml::debian::template title="WML 사용하기"
#use wml::debian::translation-check translation="24a8bbc5bf2fd2fbe025f0baa536bf1126f83723" maintainer="Seunghun Han (kkamagui)"

<p>WML은 Web Site Meta Language를 뜻합니다. WML은 입력.wml을 얻어서, 그 안에
있는 무엇이든(기본 HTML에서 Perl 코드까지 어떤 것이든!) 처리하고,
여러분이 원하는 무엇이든(예를 들어.html 이나 .php 같은) 출력합니다.</p>

<p>WML 문서화는 배우기 쉽지 않습니다. 사실 WML은 아주 완벽하지만, 어떻게 동작하고
아주 강력한지를 이해하기 시작하기 전에는 예제를 통해 배우는 게 가장 쉽습니다.
여러분은 데비안 사이트에 사용되었던 유용한 템플릿을 찾을 지도 모릅니다.
템플릿 파일은
<code><a href="https://salsa.debian.org/webmaster-team/webwml/tree/master/english/template/debian">\
webwml/english/template/debian/</a></code>에 있습니다.</p>

<p>이 문서는 여러분의 컴퓨터에 WML이 설치되어 있다고 가정합니다.
WML은
<a href="https://packages.debian.org/wml">데비안 패키지</a>에
있습니다.

<h2>WML 소스 편집</h2>

<p>모든 .wml 파일들은 <code>#use</code>가 한 줄 이상 있습니다. 여러분은 그 문법을
바꾸거나 번역하면 안 되며, <code>title=</code> 뒤의 따옴표된 문자열만 바꿀 수
있습니다. 해당 문자열이 바뀌면 출력 파일 안의 &lt;title&gt;이 바뀝니다.</p>

<p>헤더 행 빼면, .wml 페이지의 대부분에는 간단한 HTML이 들어 있습니다.
여러분이 &lt;define-tag&gt; or &lt;: ... :&gt;를 보면 주의해야 하는데,
WML의 특수한 단계 중 하나에서 앞서 경계로 구분된 코드를 처리하기 때문입니다.
자세한 내용은 아래를 보세요.</p>

<h2>데비안 웹페이지 빌드</h2>

<p>webwml/&lt;언어&gt;에서 그냥 <kbd>make</kbd>만 치세요.
우리가 적절한 인수로 <kbd>wml</kbd>을 실행하도록 makefile을 설정해두었습니다.</p>

<p> 여러분이 <kbd>make install</kbd>를 실행하면, HTML 파일들이 만들어져서
 <kbd>../../www/</kbd> 디렉터리에 놓입니다.</p>

<h2>우리가 사용하는 WML 부가 기능</h2>

<p>WML 기능 중 가장 많이 쓰는 것은 Perl 입니다. 기억하세요, 동적 페이지가 아닙니다.
Perl은 여러분이 좋아하는 무엇이든 HTML 페이지를 만들 때 씁니다.
페이지에서 Perl을 사용하는 두 가지 좋은 예제는
메인 페이지에 대한 최근 뉴스 목록을 만드는 것과 페이지 끝에 번역 링크를 만드는
것입니다.

# TODO: add the basic stuff from webwml/english/po/README here

<p>우리 웹 사이트의 템플릿을 새로 빌드하려면, wml 버전 2.0.6 이상이 필요합니다.
영어 아닌 번역의 gettext 템플릿을 새로 빌드하려면 mp4h 1.3.0 이상이 필요합니다.</p>

<h2>WML 특정 이슈</h2>

<p>.wml 파일의 문자 집합을 적절히 다루려면, 멀티 바이트 언어는 특별한 전처리
또는 후처리가 필요할 수 있습니다.
이러한 처리를 하려면 <kbd>webwml/&lt;언어&gt;/Make.lang</kbd> 안에 있는
<kbd>WMLPROLOG</kbd> 변수와 <kbd>WMLEPILOG</kbd> 변수를 적절하게 바꾸면 됩니다.
<kbd>WMLEPILOG</kbd> 프로그램이 어떻게 동작하느냐에 따라,
<kbd>WMLOUTFILE</kbd> 값을 바꾸어야 할 수도 있습니다.
<br>
예제로 중국어 또는 일본어 번역을 보세요.
</p>
