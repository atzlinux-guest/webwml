# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Song Woo-il, 2006
# Sebul <sebuls@gmail.com>, 2019
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-05-23 20:12+0900\n"
"Last-Translator: Sebul <sebuls@gmail.com>\n"
"Language-Team: debian-l10n-korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#. List of weekday names (used in modification dates)
#: ../../english/template/debian/ctime.wml:11
msgid "Sun"
msgstr "일요일"

#: ../../english/template/debian/ctime.wml:12
msgid "Mon"
msgstr "월요일"

#: ../../english/template/debian/ctime.wml:13
msgid "Tue"
msgstr "화요일"

#: ../../english/template/debian/ctime.wml:14
msgid "Wed"
msgstr "수요일"

#: ../../english/template/debian/ctime.wml:15
msgid "Thu"
msgstr "목요일"

#: ../../english/template/debian/ctime.wml:16
msgid "Fri"
msgstr "금요일"

#: ../../english/template/debian/ctime.wml:17
msgid "Sat"
msgstr "토요일"

#. List of month names (used in modification dates, and may be used in news 
#. listings)
#: ../../english/template/debian/ctime.wml:23
msgid "Jan"
msgstr "1월"

#: ../../english/template/debian/ctime.wml:24
msgid "Feb"
msgstr "2월"

#: ../../english/template/debian/ctime.wml:25
msgid "Mar"
msgstr "3월"

#: ../../english/template/debian/ctime.wml:26
msgid "Apr"
msgstr "4월"

#: ../../english/template/debian/ctime.wml:27
msgid "May"
msgstr "5월"

#: ../../english/template/debian/ctime.wml:28
msgid "Jun"
msgstr "6월"

#: ../../english/template/debian/ctime.wml:29
msgid "Jul"
msgstr "7월"

#: ../../english/template/debian/ctime.wml:30
msgid "Aug"
msgstr "8월"

#: ../../english/template/debian/ctime.wml:31
msgid "Sep"
msgstr "9월"

#: ../../english/template/debian/ctime.wml:32
msgid "Oct"
msgstr "10월"

#: ../../english/template/debian/ctime.wml:33
msgid "Nov"
msgstr "11월"

#: ../../english/template/debian/ctime.wml:34
msgid "Dec"
msgstr "12월"

#. List of long month names (may be used in "spoken" dates and date ranges).
#: ../../english/template/debian/ctime.wml:39
msgid "January"
msgstr "1월"

#: ../../english/template/debian/ctime.wml:40
msgid "February"
msgstr "2월"

#: ../../english/template/debian/ctime.wml:41
msgid "March"
msgstr "3월"

#: ../../english/template/debian/ctime.wml:42
msgid "April"
msgstr "4월"

#. The <void> tag is to distinguish short and long forms of May.
#. Do not put it in msgstr.
#: ../../english/template/debian/ctime.wml:45
msgid "<void id=\"fullname\" />May"
msgstr "5월"

#: ../../english/template/debian/ctime.wml:46
msgid "June"
msgstr "6월"

#: ../../english/template/debian/ctime.wml:47
msgid "July"
msgstr "7월"

#: ../../english/template/debian/ctime.wml:48
msgid "August"
msgstr "8월"

#: ../../english/template/debian/ctime.wml:49
msgid "September"
msgstr "9월"

#: ../../english/template/debian/ctime.wml:50
msgid "October"
msgstr "10월"

#: ../../english/template/debian/ctime.wml:51
msgid "November"
msgstr "11월"

#: ../../english/template/debian/ctime.wml:52
msgid "December"
msgstr "12월"

#. $dateform: Date format (sprintf) for modification dates.
#. Available variables are: $mday = day-of-month, $monnr = month number,
#. $mon = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:60
msgid ""
"q{[%]s, [%]s [%]2d [%]02d:[%]02d:[%]02d [%]s [%]04d}, $wday, $mon, $mday, "
"$hour, $min, $sec, q{UTC}, 1900+$year"
msgstr ""
"q{[%]04d년 [%]d월 [%]2d일 [%]s [%]02d:[%]02d:[%]02d [%]s}, 1900+$year, $monnr"
"+1, $mday, $wday, $hour, $min, $sec, q{UTC}"

#. $newsdateform: Date format (sprintf) for news items.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:68
msgid "q{[%]02d [%]s [%]04d}, $mday, $mon_str, $year"
msgstr "q{[%]04d년 [%]02d월 [%]02d일}, $year, $mon, $mday"

#. $spokendateform: Date format (sprintf) for "spoken" dates
#. (such as the current release date).
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:79
msgid "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"
msgstr "q{[%]d년 [%]d월 [%]d일}, $year, $mon, $mday"

#. $spokendateform_noyear: Date format (sprintf) for "spoken" dates
#. (such as the current release date), without the year.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy).
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:90
msgid "q{[%]d [%]s}, $mday, $mon_str"
msgstr "q{[%]d월 [%]d일}, $mon+1, $mday"

#. $spokendateform_noday: Date format (sprintf) for "spoken" dates
#. (such a conference event), without the day.
#. Available variables are: $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:99
msgid "q{[%]s [%]s}, $mon_str, $year"
msgstr "q{[%]d년 [%]d월}, $year, $monnr+1"

#. $rangeform_samemonth: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges within the same month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday = end
#. day-of-month, $smon = month number, $smon_str = month string (from @longmoy)
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:110
msgid "q{[%]d-[%]d [%]s}, $sday, $eday, $smon_str"
msgstr "q{[%]d월 [%]d일-[%]d일}, $smon+1, $sday, $eday"

#. $rangeform_severalmonths: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges spanning the end of a month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday, end
#. day-of-month, $smon = start month number, $emon = end month number,
#. $smon_str = start month string (from @longmoy), $emon_str = end month string
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:122
msgid "q{[%]d [%]s-[%]d [%]s}, $sday, $smon_str, $eday, $emon_str"
msgstr "q{[%]d월 [%]d일-[%]d월 [%]d일}, $smon+1, $sday, $emon+1, $eday"
