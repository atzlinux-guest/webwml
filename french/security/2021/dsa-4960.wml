#use wml::debian::translation-check translation="0ef64561227e59f8f24772a07d8b7a05db0d0e31" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans HAProxy, un serveur
mandataire inverse de répartition de charge rapide et fiable, qui peuvent
avoir pour conséquence une dissimulation de requête HTTP. En contrefaisant
des requêtes HTTP/2, il est possible de dissimuler une autre requête HTTP
au dorsal sélectionné par la requête HTTP/2. Avec certaines configurations,
cela permet à un attaquant d'envoyer une requête HTTP à un dorsal, en
contournant la logique de sélection de dorsal.</p>

<p>La solution connue est de désactiver HTTP/2 et de régler
« tune.h2.max-concurrent-streams » à 0 dans la section <q>global</q>.</p>

<p>global
tune.h2.max-concurrent-streams 0</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.2.9-2+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets haproxy.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de haproxy, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/haproxy">\
https://security-tracker.debian.org/tracker/haproxy</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4960.data"
# $Id: $
