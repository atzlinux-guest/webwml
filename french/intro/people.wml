#use wml::debian::template title="Les acteurs de Debian : qui sommes-nous, que faisons-nous"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e" maintainer="Cyrille Bollu"

# translators: some text is taken from /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">Comment tout cela a-t-il commencé ?</a>
    <li><a href="#devcont">Développeurs et contributeurs</a>
    <li><a href="#supporters">Personnes et organisations soutenant Debian</a>
    <li><a href="#users">Qui utilise Debian ?</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Comme beaucoup de gens le demandent, Debian se prononce « dé-byanne » (ou en notation phonétique /&#712;de.bi.&#601;n/). Cela vient des prénoms du créateur de Debian, Ian Murdock, et de sa femme, Debra.</p>
</aside>

<h2><a id="history">Comment tout cela a-t-il commencé ?</a></h2>

<p>Cela s'est passé en août 1993 quand Ian Murdock a commencé à travailler à un
nouveau système d'exploitation qui serait fait de façon ouverte, dans l'esprit
de Linux et de GNU. Il a envoyé une invitation ouverte à d'autres développeurs
de logiciels, leur demandant de contribuer à une distribution de logiciels
basée sur le noyau Linux, ce qui était relativement nouveau à ce moment-là.
Debian était censée être soigneusement et consciencieusement mise en place,
entretenue et gérée avec beaucoup de soins, adoptant une conception ouverte,
avec des contributions et le soutien de la communauté du logiciel libre.</p>

<p>Cela a commencé comme un groupe petit et très soudé
de <i>hackers</i> de logiciel libre, et graduellement cela s'est
développé pour devenir une communauté de développeurs et
d'utilisateurs vaste et bien organisée.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="$(DOC)/manuals/project-history/">Consultez l'histoire détaillée</a></button></p>

<h2><a id="devcont">Développeurs et contributeurs</a></h2>

<p>Debian est une organisation composée de bénévoles uniquement. Plus d'un
millier de développeurs éparpillés <a href="$(DEVEL)/developers.loc">autour du monde</a>
travaillent sur Debian pendant leur temps libre. Peu de développeurs se sont en
fait rencontrés physiquement. En fait, la communication se fait principalement
par courrier électronique (listes de diffusion sur
<a href="https://lists.debian.org/">lists.debian.org</a>) et IRC (canal #debian
sur irc.debian.org).
</p>

<p>
La liste complète des membres officiels de Debian peut être consultée sur le
site <a href="https://nm.debian.org/members">nm.debian.org</a> et
<a href="https://contributors.debian.org">contributors.debian.org</a> affiche
une liste de tous les contributeurs et de toutes les équipes qui travaillent sur
la distribution Debian.</p>

<p>Le projet Debian a une <a href="organization">structure</a> soigneusement
organisée. Pour plus d'informations sur ce à quoi ressemble le projet Debian
vu de l'intérieur, n'hésitez pas à naviguer dans le
<a href="$(DEVEL)/">coin des développeurs</a>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">Découvrez notre philosophie</a></button></p>

<h2><a id="supporters">Personnes et organisations soutenant Debian</a></h2>

<p>Beaucoup d'autres personnes et d'organisations font partie de la
communauté Debian :</p>
<ul>
  <li><a href="https://db.debian.org/machines.cgi">hébergement et sponsors matériels</a></li>
  <li><a href="../mirror/sponsors">sponsors des miroirs</a></li>
  <li><a href="../partners/">partenaires <q>développement et services</q></a></li>
  <li><a href="../consultants">consultants</a></li>
  <li><a href="../CD/vendors">vendeurs de support d’installation de Debian</a></li>
  <li><a href="../distrib/preinstalled">vendeurs d'ordinateurs avec Debian préinstallée</a></li>
  <li><a href="../events/merchandise">vendeurs de produits dérivés</a></li>
</ul>

<h2><a name="users">Qui utilise Debian ?</a></h2>

<p>Debian est utilisée par une grande variété d'organisations, petites ou
grandes, ainsi que par plusieurs milliers d'individus. Consultez notre page
<a href="../users/">Qui utilise Debian ?</a> pour une liste des organisations
éducatives, commerciales ou à but non lucratif, aussi bien que des organisations
gouvernementales qui ont fourni de courtes descriptions sur comment et pourquoi
elles utilisent Debian.</p>
