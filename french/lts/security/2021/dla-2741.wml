#use wml::debian::translation-check translation="bb4c44eb3bf46b6c3593e6f230aa25093d0ae842" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Lukas Euler a découvert une vulnérabilité de traversée de répertoires dans
commons-io, une bibliothèque Java pour les classes couramment utiles et
relatives à IO. Lors de l’invocation de la méthode FileNameUtils.normalize
avec une chaîne d’entrée non conforme, telle que « //../foo » ou « \\..\foo »,
le résultat pourrait être la même valeur, par conséquent fournissant
éventuellement un accès à des fichiers dans le répertoire parent, mais pas
au-delà (donc traversée de répertoires <q>limitée</q>), si le code appelant
utilise le résultat pour construire une valeur de chemin.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 2.5-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets commons-io.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de commons-io,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/commons-io">\
https://security-tracker.debian.org/tracker/commons-io</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2741.data"
# $Id: $
