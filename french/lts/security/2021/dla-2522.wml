#use wml::debian::translation-check translation="d78d6786839a8f2bf330a8eeb13d36435d80285e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un défaut a été découvert dans coturn, un serveur TURN et STUN de voix
sur IP. Par défaut coturn n'autorise pas de pairs avec les adresses de la
boucle locale (127.x.x.x et ::1). Un attaquant distant peut contourner la
protection à l'aide d'une requête contrefaite pour l'occasion utilisant une
adresse de pair de <q>0.0.0.0</q> pour forcer coturn à créer un relais vers
l'interface de la boucle locale. Si l'interface est à l'écoute sur IPv6,
elle peut aussi être atteinte en utilisant [::1] ou [::] comme adresse.</p>
<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 4.5.0.5-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets coturn.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de coturn, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/coturn">https://security-tracker.debian.org/tracker/coturn</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2522.data"
# $Id: $
