#use wml::debian::translation-check translation="1a61f76f075466f3817f496d700117dcab783aab" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une possibilité d’attaque par
empoisonnement du cache web dans Django, un cadriciel de développement web
populaire basé sur Python.</p>

<p>Cela était dû à un traitement non sûr de caractères « ; » dans
<tt>urllib.parse.parse_qsl</tt> de la méthode Python rétroportée vers le code de
base pour corriger d’autres problèmes de sécurité dans le passé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23336">CVE-2021-23336</a>
<p>Les paquets python/cpython de la version 0 et avant la version 3.6.13,
de la version 3.7.0 et avant la version 3.7.10, de la version 3.8.0 et avant la
version 3.8.8, de la version 3.9.0 et avant la version 3.9.2 étaient vulnérables
à un empoisonnement de cache web à l’aide de urllib.parse.parse_qsl et
urllib.parse.parse_qs en utilisant une dissimulation du paramètre appelé de
vecteur. Quand l’attaquant peut séparer les paramètres de requête en utilisant
un point-virgule (;), il peut provoquer une différence dans l’interprétation de
la requête entre le mandataire (avec la configuration par défaut) et le serveur.
Cela peut conduire à des requêtes malveillantes mis en cache comme sûres, car le
mandataire ne voit habituellement pas le deux-points comme un séparateur, et
par conséquent ne l’inclut pas dans une clé de cache d’un paramètre sans clé.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:1.10.7-2+deb9u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2569.data"
# $Id: $
