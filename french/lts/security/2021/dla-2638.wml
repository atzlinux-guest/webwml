#use wml::debian::translation-check translation="f84818a3726e87ac7e49c84481e2d75d55c84983" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilité de sécurité ont été trouvées dans Jackson Databind.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24616">CVE-2020-24616</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.6, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à com.anteros.dbcp.AnterosDBCPDataSource (c’est-à-dire, Anteros-DBCP).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24750">CVE-2020-24750</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.6, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à com.pastdev.httpcomponents.configuration.JndiConfiguration.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25649">CVE-2020-25649</a>

<p>Un défaut a été découvert dans FasterXML Jackson Databind, dans lequel une
expansion d’entité n’était pas sécurisée correctement. Ce défaut crée une
vulnérabilité pour une attaque par entité externe XML (XXE). Le principal danger
créé par cette vulnérabilité concerne l’intégrité des données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35490">CVE-2020-35490</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à org.apache.commons.dbcp2.datasources.PerUserPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35491">CVE-2020-35491</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à org.apache.commons.dbcp2.datasources.SharedPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35728">CVE-2020-35728</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à com.oracle.wls.shaded.org.apache.xalan.lib.sql.JNDIConnectionPool
(c’est-à-dire, Xalan embarqué dans org.glassfish.web/javax.servlet.jsp.jstl).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36179">CVE-2020-36179</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à oadd.org.apache.commons.dbcp.cpdsadapter.DriverAdapterCPDS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36180">CVE-2020-36180</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à org.apache.commons.dbcp2.cpdsadapter.DriverAdapterCPDS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36181">CVE-2020-36181</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à org.apache.tomcat.dbcp.dbcp.cpdsadapter.DriverAdapterCPDS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36182">CVE-2020-36182</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à org.apache.tomcat.dbcp.dbcp.cpdsadapter.DriverAdapterCPDS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36183">CVE-2020-36183</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à org.docx4j.org.apache.xalan.lib.sql.JNDIConnectionPool.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36184">CVE-2020-36184</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à org.apache.tomcat.dbcp.dbcp2.datasources.PerUserPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36185">CVE-2020-36185</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à org.apache.tomcat.dbcp.dbcp2.datasources.SharedPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36186">CVE-2020-36186</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à org.apache.tomcat.dbcp.dbcp.datasources.PerUserPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36187">CVE-2020-36187</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à org.apache.tomcat.dbcp.dbcp.datasources.SharedPoolDataSource.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36188">CVE-2020-36188</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à org.apache.tomcat.dbcp.dbcp.datasources.SharedPoolDataSource.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36189">CVE-2020-36189</a>

<p>FasterXML jackson-databind, versions 2.x avant 2.9.10.8, gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, relative
à com.newrelic.agent.deps.ch.qos.logback.core.db.DMCS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20190">CVE-2021-20190</a>

<p>Un défaut a été découvert dans jackson-databind avant la version 2.9.10.7.
FasterXML gère incorrectement l’interaction entre les gadgets de sérialisation
et la saisie. Le principal danger créé par cette vulnérabilité concerne la
confidentialité et l’intégrité des données ainsi que la disponibilité du
système.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.8.6-1+deb9u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jackson-databind.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jackson-databind, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/jackson-databind">\
https://security-tracker.debian.org/tracker/jackson-databind</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2638.data"
# $Id: $
