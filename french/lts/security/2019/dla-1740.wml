#use wml::debian::translation-check translation="97582adfd4d4f81eb75f29bf8d5518f0d8035cb9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes de sécurité ont été corrigés dans plusieurs
démultiplexeurs et décodeurs de la bibliothèque multimédia libav.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1872">CVE-2015-1872</a>

<p>La fonction ff_mjpeg_decode_sof dans libavcodec/mjpegdec.c ne vérifiait pas
le nombre de composants dans le segment démarrage de trame JPEG-LS.
Cela permettait à des attaquants distants de provoquer un déni de service
(accès tableau hors limites) ou éventuellement d’avoir un impact non précisé
à l’aide de données Motion JPEG contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14058">CVE-2017-14058</a>

<p>La fonction read_data dans libavformat/hls.c ne restreignait pas les essais
de rechargement pour une liste insuffisante. Cela permettait à des attaquants
distants de provoquer un déni de service (boucle infinie).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000460">CVE-2017-1000460</a>

<p>Dans get_last_needed_nal() (libavformat/h264.c) la valeur de retour de
init_get_bits était ignorée et get_ue_golomb(&amp;gb) était appelée dans un contexte
get_bits non initialisé. Cela causait une exception de déréférencement de NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6392">CVE-2018-6392</a>

<p>La fonction filter_slice dans libavfilter/vf_transpose.c permettait à des
attaquants distants de provoquer un déni de service (accès hors tableau)
à l'aide d'un fichier MP4 contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1999012">CVE-2018-1999012</a>

<p>libav contenait un CWE-835 : vulnérabilité de boucle infinie dans un
démultiplexeur de format pva. Cela pouvait aboutir à une vulnérabilité qui
permettait à des attaquants de consommer des montants excessifs de ressources
telles que le CPU ou RAM. Cette attaque semble être exploitable à l’aide
d’un fichier PVA contrefait pour l’occasion fourni en entrée.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 6:11.12-1~deb8u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1740.data"
# $Id: $
