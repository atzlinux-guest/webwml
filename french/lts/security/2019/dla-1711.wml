#use wml::debian::translation-check translation="f5e26ef05fba608f19846f8593e3bd548c8574f4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une fuite de mémoire a été découverte dans le rétroportage de correctifs pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-16864">CVE-2018-16864</a>
 dans systemd-journald.</p>

<p>La fonction dispatch_message_real() dans journald-server.c ne libère pas la
mémoire allouée pour stocker l’entrée « _CMDLINE= ». Un attaquant local pourrait
utiliser ce défaut pour provoquer le plantage de systemd-journald.</p>

<p>Notez que le service systemd-journald n’est pas redémarré automatiquement. Un
redémarrage du service ou, plus sûrement, un redémarrage du système sont
conseillés.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 215-17+deb8u11.</p>
<p>Nous vous recommandons de mettre à jour vos paquets systemd.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1711.data"
# $Id: $
