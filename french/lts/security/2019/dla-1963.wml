#use wml::debian::translation-check translation="fd50420e24a1a349ba9c2ab04e1b151acd8a14f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes d’allocation de tampon ont été découverts dans poppler.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9959">CVE-2019-9959</a>

<p>Une valeur inattendue de longueur négative peut provoquer un dépassement
d'entier, qui à son tour rend possible d’allouer un grand morceau de mémoire
pour le tas avec une taille contrôlée par l’attaquant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10871">CVE-2019-10871</a>

<p>Les données RGB sont considérées comme des données CMYK et en conséquence
quatre octets sont lus au lieu de trois à la fin de l’image. La version corrigée
règle SPLASH_CMYK, ce qui est la solution recommandée par l’amont.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.26.5-2+deb8u12.</p>
<p>Nous vous recommandons de mettre à jour vos paquets poppler.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1963.data"
# $Id: $
