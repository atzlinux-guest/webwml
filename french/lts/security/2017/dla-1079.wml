#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La bibliothèque Perl pour communiquer avec la base de données MySQL, utilisée
dans le client <q>mysql</q> en ligne de commande, est vulnérable à une attaque
d’homme du milieu dans les configurations SSL et à un plantage distant lors de
la connexion à un serveur malveillant.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10788">CVE-2017-10788</a>

<p>Le module DBD::mysql jusqu’à 4.042 pour Perl permet à des attaquants distants
de provoquer un déni de service (utilisation de mémoire après libération et
plantage d'application) ou, éventuellement, d’avoir un impact non précisé en
déclenchant (1) certaines réponses à des erreurs d’un serveur MySQL ou
(2) une perte de connexion réseau à un serveur MySQL. Le défaut d’utilisation de
mémoire après libération était introduit en s’appuyant sur une documentation
incorrecte de mysql_stmt_close et des exemples de code d’Oracle.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10789">CVE-2017-10789</a>

<p>Le module DBD::mysql jusqu’à 4.042 pour Perl utilise le réglage mysql_ssl=1
pour signifier que SSL est facultatif (même si la documentation du réglage
possède une déclaration <q>your communication with the server will be
encrypted</q>). Cela permet à des attaquants de type « homme du milieu »
d’usurper des serveurs à l'aide d'une attaque cleartext-downgrade, un problème
relatif
à <a href="https://security-tracker.debian.org/tracker/CVE-2015-3152">CVE-2015-3152</a>.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.021-1+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libdbd-mysql-perl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1079.data"
# $Id: $
