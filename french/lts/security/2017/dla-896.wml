#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le client de
messagerie Thunderbird de Mozilla. Plusieurs erreurs de sécurité de mémoire, des
dépassements de tampon et d’autres erreurs d’implémentation pourraient conduire
à l'exécution de code arbitraire ou une usurpation.</p>

<p>Avec la version 45.8, Debian abandonne son marquage personnalisé du paquet
Icedove et fournit de nouveau le client Thunderbird. Veuillez consulter le lien
suivant pour plus d’informations :
 <a href="https://wiki.debian.org/Thunderbird">https://wiki.debian.org/Thunderbird</a></p>

<p>Les paquets de transition pour les paquets Icedove sont fournis et mettent
à niveau automatiquement vers la nouvelle version. Puisque de nouveaux paquets
binaires doivent être installés, assurez-vous d’autoriser la procédure de mise
à niveau (en utilisant par exemple, « apt-get dist-upgrade » au lieu de
« apt-get upgrade »).</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:45.8.0-3~deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets icedove.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-896.data"
# $Id: $
