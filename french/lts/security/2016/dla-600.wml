#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La bibliothèque de chiffrement libgcrypt11 comporte une faiblesse dans
le générateur de nombres aléatoires.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6313">CVE-2016-6313</a>

<p>Felix Dörre et Vladimir Klebanov de l'Institut de Technologie de
Karlsruhe ont découvert un bogue dans les fonctions de mélange du
générateur de nombres aléatoires (random number generator – RNG) de
Libgcrypt. Un attaquant qui obtient 4640 bits du RNG peut prévoir de façon
banale les 160 bits suivants de la sortie.</p></li>

</ul>

<p>Une première analyse de l'impact de ce bogue de GnuPG montre que les
clés RSA existantes ne sont pas affaiblies. En ce qui concerne les clés
DSA et Elgamal, il est aussi peu probable que la clé privée puisse être
prédite à partir d'autres informations publiques.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.5.0-5+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgcrypt11.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-600.data"
# $Id: $
