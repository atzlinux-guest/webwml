#use wml::debian::translation-check translation="5405400499b21ba8167cfd0226e9001d8e28862a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans la servlet Tomcat
et le moteur JSP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13934">CVE-2020-13934</a>

<p>Une connexion h2c directe vers Apache Tomcat ne supprime pas le processus
HTTP/1.1 après la mise à niveau vers HTTP/2. Si un nombre suffisant de ce genre
de requête était fait, une OutOfMemoryException pouvait se produire,
conduisant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13935">CVE-2020-13935</a>

<p>La longueur des données utiles dans une trame WebSocket n’était pas validée
correctement dans Apache Tomcat. Des longueurs non autorisées pouvaient
déclencher une boucle infinie. Plusieurs requêtes avec des longueurs non
autorisées de données utiles pouvaient conduire à un déni de service.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 8.5.54-0+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat8.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat8, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat8">https://security-tracker.debian.org/tracker/tomcat8</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2286.data"
# $Id: $
