#use wml::debian::translation-check translation="e188e8a512fe3b7bd7b18c9acfcd5253ac02284e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à l'exécution de code arbitraire, une élévation des
privilèges, un déni de service ou une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19039">CVE-2019-19039</a>

<p><q>Team bobfuzzer</q> a signalé un bogue dans Btrfs qui pourrait conduire
à un échec d’assertion (WARN). Un utilisateur autorisé à monter et accéder à des
systèmes de fichiers arbitraires pourrait utiliser cela pour provoquer un déni
de service (plantage) si le paramètre panic_on_warn kernel est activé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19377">CVE-2019-19377</a>

<p><q>Team bobfuzzer</q> a signalé un bogue dans Btrfs qui pourrait conduire
à une utilisation de mémoire après libération. Un utilisateur autorisé à monter
et accéder à des systèmes de fichiers arbitraires pourrait utiliser cela pour
provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19770">CVE-2019-19770</a>

<p>L’outil syzbot a découvert une situation de compétition dans le traceur de
bloc E/S (blktrace) qui pourrait conduire à un plantage du système. Puisque
blktrace peut être contrôlé uniquement par des utilisateurs privilégiés,
l’impact de sécurité n’est pas clairement identifié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19816">CVE-2019-19816</a>

<p><q>Team bobfuzzer</q> a signalé un bogue dans Btrfs qui pourrait conduire
à une écriture hors limites. Un utilisateur autorisé à monter et accéder à des
systèmes de fichiers arbitraires pourrait utiliser cela pour provoquer un déni
de service (plantage ou corruption de mémoire) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0423">CVE-2020-0423</a>

<p>Une situation de compétition a été découverte dans le pilote Android binder,
qui pourrait aboutir à une utilisation de mémoire après libération. Sur les
systèmes utilisant ce pilote, un utilisateur local pourrait utiliser cela pour
provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8694">CVE-2020-8694</a>

<p>Plusieurs chercheurs ont découvert que le sous-système powercap permettait
par défaut à tous les utilisateurs de lire le mesureur d’énergie du CPU. Sur les
systèmes utilisant les CPU d’Intel, cela fournissait un canal auxiliaire
permettant de divulguer des informations sensibles entre les processus
d’utilisateur, ou du noyau vers les processus d’utilisateurs. Ce mesureur est
par défaut désormais lisible uniquement par le superutilisateur.</p>

<p>Ce problème peut être mitigé en exécutant :</p>

<code>chmod go-r /sys/devices/virtual/powercap/*/*/energy_uj</code>

<p>Cela doit être répété à chaque redémarrage du système avec une version du
noyau non précisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14351">CVE-2020-14351</a>

<p>Une situation de compétition a été découverte dans le sous-système
évènements de performance qui pourrait conduire à une utilisation de mémoire
après libération. Un utilisateur local autorisé à y accéder pourrait utiliser
cela pour provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p>

<p>La configuration Debian du noyau ne permet pas par défaut aux utilisateurs
non privilégiés cet accès, ce qui atténue complètement ce défaut.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25656">CVE-2020-25656</a>

<p>Yuan Ming et Bodong Zhao ont découvert une situation de compétition dans le
pilote de terminal virtuel (vt) qui pourrait conduire à une utilisation de
mémoire après libération. Un utilisateur local avec la capacité
<tt>CAP_SYS_TTY_CONFIG</tt> pourrait utiliser cela pour provoquer un déni de
service (plantage ou corruption de mémoire) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25668">CVE-2020-25668</a>

<p>Yuan Ming et Bodong Zhao ont découvert une situation de compétition dans le
pilote de terminal virtuel (vt) qui pourrait conduire à une utilisation de
mémoire après libération. Un utilisateur local avec accès au terminal virtuel ou
avec la capacité <tt>CAP_SYS_TTY_CONFIG</tt> pourrait utiliser cela pour
provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25669">CVE-2020-25669</a>

<p>Bodong Zhao a découvert un bogue dans le pilote de clavier Sun (sunkbd) qui
pourrait conduire à une utilisation de mémoire après libération. Sur un système
utilisant ce pilote, un utilisateur local pourrait utiliser cela pour provoquer
un déni de service (plantage ou corruption de mémoire) ou, éventuellement, pour
une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25704">CVE-2020-25704</a>

<p>kiyin a découvert une fuite de mémoire potentielle dans le
sous-système évènements de performance. Un utilisateur local y ayant accès
pourrait utiliser cela pour provoquer un déni de service (épuisement de mémoire).</p>

<p>La configuration Debian du noyau ne permet pas par défaut aux utilisateurs
non privilégiés cet accès, ce qui atténue complètement ce défaut.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25705">CVE-2020-25705</a>

<p>Keyu Man a signalé que la limitation stricte de débit pour la transmission
de paquets ICMP fournit un canal auxiliaire aidant des attaquants réseau à
conduire une usurpation de paquet (spoofing). En particulier, cela est pratique
pour des attaquants réseau hors chemin pour <q>empoisonner</q> les caches DNS
avec des réponses usurpées (attaque <q>SAD DNS</q>).</p>

<p>Ce problème a été mitigé en rendant aléatoire le fait de compter les paquets
selon la limite de débit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27673">CVE-2020-27673</a> / <a href="https://xenbits.xen.org/xsa/advisory-332.html">XSA-332</a>

<p>Julien Grall d’Arm a découvert un bogue dans le code de traitement
d’évènement de Xen. Où Linux était utilisé dans un dom0 Xen, des invités non
privilégiés (domU) pourraient causer un déni de service (utilisation excessive
du CPU usage ou plantage) dans dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27675">CVE-2020-27675</a> / <a href="https://xenbits.xen.org/xsa/advisory-331.html">XSA-331</a>

<p>Jinoh Kang de Theori a découvert une situation de compétition dans le code de
traitement d’évènement Xen. Où Linux était utilisé dans un dom0 Xen, des invités
non privilégiés (domU) pourraient causer un déni de service (utilisation
excessive du CPU usage ou plantage) dans dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28941">CVE-2020-28941</a>

<p>Shisong Qin et Bodong Zhao a découvert un bogue dans le sous-système de
lecture d’écran Speakup. Ce dernier suppose qu’il est limité uniquement à un
périphérique terminal (tty) à la fois, mais ne l’impose pas. Un utilisateur
local pourrait exploiter ce bogue pour provoquer un déni de service
(plantage ou épuisement de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28974">CVE-2020-28974</a>

<p>Yuan Ming a découvert un bogue dans le pilote de terminal virtuel (vt) qui
pourrait conduire à une lecture hors limites. Un utilisateur local avec accès
à un terminal virtuel, ou avec la capacité <tt>CAP_SYS_TTY_CONFIG</tt>, pourrait
éventuellement utiliser cela pour obtenir des informations sensibles du noyau
ou pour provoquer un déni de service (plantage).</p>

<p>L’opération spécifique ioctl touchée par ce bogue (<tt>KD_FONT_OP_COPY</tt>)
a été désactivée, car personne ne pense qu’un programme n'en dépende.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.19.160-2~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-4.19.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-4.19, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-4.19">https://security-tracker.debian.org/tracker/linux-4.19</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2483.data"
# $Id: $
