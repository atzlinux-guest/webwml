#use wml::debian::translation-check translation="f73042dab9d3f11b7cfd9c029c78e63d308c5b6c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un certain nombre de vulnérabilités de
script intersite (XSS) dans cacti, une interface web pour la supervision de systèmes.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7106">CVE-2020-7106</a>

<p>Cacti 1.2.8 possède des XSS stockés dans data_sources.php,
color_templates_item.php, graphs.php, graph_items.php, lib/api_automation.php,
user_admin.php et user_group_admin.php, comme prouvé par le paramètre de
description dans data_sources.php (une chaîne brute de la base de données
affichée par $header pour déclencher le XSS).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.8.8b+dfsg-8+deb8u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cacti.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2069.data"
# $Id: $
