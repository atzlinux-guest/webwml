#use wml::debian::translation-check translation="7cdd275781d3e7d1cada57d742b9f7cebdf7a845" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les CVE suivants ont été signalés à l’encontre de src:wpa.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10064">CVE-2019-10064</a>

<p>hostapd avant la version 2.6, dans le mode EAP, appelait les fonctions
rand() et random() de la bibliothèque standard sans appel préalable
à srand() ou srandom(), ce qui aboutissait à une utilisation inappropriée de
valeurs déterministes. Cela a été corrigé en conjonction avec
<a href="https://security-tracker.debian.org/tracker/CVE-2016-10743">CVE-2016-10743</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12695">CVE-2020-12695</a>

<p>La spécification UPnP d’Open Connectivity Foundation avant la
version 2020-04-17 n’interdisait pas l’acceptation d’une requête
d’abonnement avec une URL d’expédition sur une partie de réseau différente
de l’URL complètement qualifiée de souscription d’évènements, c'est-à-dire
le problème « CallStranger ».</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2:2.4-1+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpa.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wpa, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wpa">https://security-tracker.debian.org/tracker/wpa</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2318.data"
# $Id: $
