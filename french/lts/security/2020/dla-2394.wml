#use wml::debian::translation-check translation="5627cf9684f8d8c3415b63a36a5295092a30a90c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Squid, un
serveur et mandataire cache de haute performance pour des clients web.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15049">CVE-2020-15049</a>

<p>Un problème a été découvert dans http/ContentLengthInterpreter.cc dans Squid.
Une attaque par dissimulation de requête et empoisonnement peut réussir
à l’encontre d’un cache HTTP. Le client envoie une requête avec un en-tête
ContentLength contenant "+\ "-" ou un préfixe de caractère blanc non habituel
d’interpréteur à la valeur du champ longueur. Cette mise à jour fournit aussi
plusieurs autres améliorations au code d’analyse de HttpHeader.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15810">CVE-2020-15810</a>

<p>et <a href="https://security-tracker.debian.org/tracker/CVE-2020-15811">CVE-2020-15811</a>.</p>

<p>Due à une validation incorrecte de données, des attaques par dissimulation de
requête HTTP peuvent réussir à l’encontre de trafic HTTP ou HTTPS. Cela conduit
à un empoisonnement du cache et permet à n’importe quel client, y compris les
scripts de navigateur, de contourner la sécurité locale et d’empoissonner le
mandataire cache, et n’importe quels caches en aval, avec le contenu d’une source
arbitraire. Lorsqu’il est configuré pour une analyse assouplie d’en-tête (par
défaut), Squid relaie les en-têtes contenant des caractères blancs aux serveurs
amont. Lorsque cela se produit sous forme de préfixe à un en-tête Content-Length,
la longueur de trame indiquée sera ignorée par Squid (permettant une longueur
conflictuelle pouvant être utilisée à partir d’un autre en-tête Content-Length)
mais relayée en amont.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24606">CVE-2020-24606</a>

<p>Squid permet à un pair autorisé de réaliser un déni de service en utilisant
tous les cycles disponibles de CPU lors du traitement de message contrefait de
réponse <q>Cache Digest</q>. Cela ne se produit lorsque cache_peer est utilisé
avec la fonctionnalité <q>cache digests</q>. Le problème existe parce que le
blocage opérationnel (livelocking) de peerDigestHandleReply() dans
peer_digest.cc gère incorrectement l’EOF.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.5.23-5+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets squid3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de squid3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/squid3">https://security-tracker.debian.org/tracker/squid3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2394.data"
# $Id: $
