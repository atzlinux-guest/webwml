#use wml::debian::translation-check translation="48d2e49e08176a66a173f2cf6a373460824459f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans qt4-x11, la version
patrimoniale de la boîte à outils Qt.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15518">CVE-2018-15518</a>

<p>Double libération de zone de mémoire ou corruption dans QXmlStreamReader
lors de l’analyse d’un document XML non valable, contrefait pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19869">CVE-2018-19869</a>

<p>Une image SVG mal formée provoque une erreur de segmentation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19870">CVE-2018-19870</a>

<p>Une image GIF mal formée provoque un déréférencement de pointeur NULL dans
QGifHandler aboutissant à une erreur de segmentation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19871">CVE-2018-19871</a>

<p>Consommation de ressources non contrôlée dans QTgaFile.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19872">CVE-2018-19872</a>

<p>Une image PPM mal formée provoque un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19873">CVE-2018-19873</a>

<p>Erreur de segmentation pour QBmpHandler pour un fichier BMP mal formé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17507">CVE-2020-17507</a>

<p>Lecture excessive de tampon dans l’analyseur XBM.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4:4.8.7+dfsg-11+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qt4-x11.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qt4-x11, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qt4-x11">https://security-tracker.debian.org/tracker/qt4-x11</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2377.data"
# $Id: $
