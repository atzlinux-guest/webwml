#use wml::debian::template title="Motivi per scegliere Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="269c9bb8fba913f448196bc64289f477eab6ef9f"

<p>Sono molte le ragioni che portano le persone a scegliere Debian come sistema operativo.</p>

<h1>Motivi principali</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>Debian è Software Libero.</strong></dt>
  <dd>
    Debian è realizzato con Software Libero e open source e rimarrà sempre <a
    href="free">Libero</a> al 100%. Libertà per chiunque di utilizzarlo, modificarlo e distribuirlo.
	Questa è la principale promessa fatta ai <a href="../users">nostri utenti</a>. È anche gratuito.
  </dd>
</dl>

<dl>
  <dt><strong>Debian è un sistema operativo basato su Linux stabile e sicuro.</strong></dt>
  <dd>
    Debian è un sistema operativo per un'ampia gamma di dispositivi che comprende
	laptop, desktop e server. Dal 1993 piace agli utenti per la sua stabilità e
	affidabilità. Ogni pacchetto viene fornito con una configurazione predefinita.
    Gli sviluppatori forniscono gli aggiornamenti di sicurezza di tutti i pacchetti per 
    per l'intero ciclo di vita ovunque risulti possibile.
  </dd>
</dl>

<dl>
  <dt><strong>Debian ha un vasto supporto hardware.</strong></dt>
  <dd>
    La maggior parte dell'hardware è già gestito dal kernel Linux. Quando il
    software libero non è sufficiente sono disponibili i driver proprietari.
  </dd>
</dl>

<dl>
  <dt><strong>Debian garantisce aggiornamenti agevoli.</strong></dt>
  <dd>
    Debian è famosa per la semplicità e agevolezza degli aggiornamenti all'interno
    di un ciclo di rilascio e anche del passaggio al successivo rilascio.
  </dd>
</dl>

<dl>
  <dt><strong>Molte distribuzioni sono basate su Debian.</strong></dt>
  <dd>
    Molte delle distribuzioni Linux più popolari come Ubuntu, Knoppix, PureOS
    e Tails, scelgono Debian come base per il proprio software.
    Debian fornisce tutti gli strumenti, chiunque può ampliare il software
    nell'archivio Debian con i propri pacchetti, per soddisfare le proprie necessità.
  </dd>
</dl>

<dl>
  <dt><strong>Il progetto Debian è una comunità.</strong></dt>
  <dd>
    Debian non è solo un sistema operativo Linux. Il software è co-prodotto
    da centinaia di volontari di tutto il mondo. È possibile far parte della
    comunità Debian anche se non si è uno sviluppatore o un amministratore
    di sistema. Debian è una comunità guidata dal consenso e ha una
    <a href="../devel/constitution">struttura di governo democratica</a>.
    Dato che tutti gli sviluppatori Debian hanno gli stessi diritti, non può
    essere controllata da una singola azienda. Ci sono sviluppatori in oltre
    60 paesi e il nostro Installatore Debian è tradotto in oltre 80 lingue.
  </dd>
</dl>

<dl>
  <dt><strong>Debian dispone di più modalità di installazione.</strong></dt>
  <dd>
    Gli utenti finali utilizzeranno il nostro <a
    href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live CD</a>
     che contiene Calamares, l'installatore facile da usare, che richiede pochissimi input
    e conoscenze minime. Gli utenti un po' più competenti possono usare il nostro
    installatore unico e completo, mentre gli esperti possono ottimizzare l'installazione
    o addirittura usare lo strumento per automatizzare l'installazione via rete.
  </dd>
</dl>

<br>

<h1>Ambito aziendale</h1>

<p>
  Chi ha bisogno di usare Debian in ambito aziendale può usufruire di
  questi ulteriori vantaggi:
</p>

<dl>
  <dt><strong>Debian è affidabile.</strong></dt>
  <dd>
    Debian dimostra la propria affidabilità ogni giorno con migliaia di casi
    d'uso nel mondo reale che variano dal laptop di un singolo utente fino
    ai super collisori, lo scambio di azioni e l'industria dell'automobile.
    È popolare anche nel mondo accademico, scientifico e nel settore pubblico.
  </dd>
</dl>

<dl>
  <dt><strong>È facile trovare un consulente Debian.</strong></dt>
  <dd>
    I nostri manutentori di pacchetti non si occupano solo di creare i
    pacchetti e incorporare le nuove versioni originali in Debian. Spesso
    sono esperti del software originale e contribuiscono direttamente allo
    sviluppo a monte. A volte sono addirittura parte attiva nello sviluppo a monte.
  </dd>
</dl>

<dl>
  <dt><strong>Debian è sicuro.</strong></dt>
  <dd>
    Debian offre supporto per la sicurezza per rilasci stabili. Molte distribuzioni
    ed esperti di sicurezza fanno affidamento sul security tracker di Debian.
  </dd>
</dl>

<dl>
  <dt><strong>Supporto a Lungo Termine.</strong></dt>
  <dd>
    Esiste il <a href="https://wiki.debian.org/LTS">Supporto a Lungo
    Termine</a> (LTS) gratuito. Per il rilascio stabile è offerto un
    supporto esteso per 5 anni e oltre. Inoltre esiste anche l'iniziativa
    <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a> che
    estende il supporto per un insieme limitato di pacchetti per più di 5 anni.
  </dd>
</dl>

<dl>
  <dt><strong>Immagini cloud.</strong></dt>
  <dd>
    Le immagini ufficiali sono disponibili per tutte le principali piattaforme cloud.
    Sono disponibili gli strumenti e la configurazione per creare delle immagini
    personalizzate. È anche possibile utilizzare Debian in macchine virtuali su desktop
    oppure in un container.
  </dd>
</dl>

<br>

<h1>Sviluppatori</h1>
<p>Debian è largamente utilizzato da ogni tipo di sviluppatore software e hardware.</p>

<dl>
  <dt><strong>Sistema di tracciamento dei bug pubblico.</strong></dt>
  <dd>
    Il <a href="https://bugs.debian.org/">sistema di tracciamento bug</a>
    è consultabile pubblicamente da tutti tramite un normale browser web.
    Non nascondiamo i bug del nostro software e tutti possono inviare segnalazioni.
  </dd>
</dl>

<dl>
  <dt><strong>IoT e dispositivi embedded.</strong></dt>
  <dd>
    È supportata un'ampia gamma di dispositivi come il Raspberry Pi, le varianti di QNAP,
    dispositivi mobili, router casalinghi e molti SBC (Single Board Computer).
  </dd>
</dl>

<dl>
  <dt><strong>Numerose architetture hardware.</strong></dt>
  <dd>
    È supportata una <a href="../ports">lunga lista</a> di architetture
    tra cui amd64, i386, diverse varianti di ARM e MIPS, POWER7, POWER8,
    IBM System z e RISC-V. Debian può essere eseguita anche su altre piattaforme
    più vecchie o esotiche.
  </dd>
</dl>

<dl>
  <dt><strong>È disponibile un numero enorme di pacchetti software.</strong></dt>
  <dd>
    Debian dispone del più grande numero di pacchetti disponibili per
    l'installazione (attualmente <packages_in_stable>). I pacchetti utilizzano il
    formato deb che è famoso per la sua alta qualità.
  </dd>
</dl>

<dl>
  <dt><strong>Sono disponibili diverse versioni.</strong></dt>
  <dd>
    Oltre al rilascio stabile è possibile ottenere le versioni più recenti dei
    software utilizzando i rilasci testing oppure unstable.
  </dd>
</dl>

<dl>
  <dt><strong>Alta qualità grazie agli strumenti di sviluppo e alle policy.</strong></dt>
  <dd>
    Parecchi strumenti per gli sviluppatori aiutano a tenere la qualità a
	un alto livello e la nostra <a href="../doc/debian-policy/">policy</a>
    definisce i requisiti tecnici che ogni pacchetto deve soddisfare per essere inserito
    nella distribuzione. L'integrazione continua esegue autopkgtest,
    piuparts è lo strumento di test per l'installazione, l'aggiornamento e la rimozione
    e lintian è un verificatore completo per i pacchetti Debian.
  </dd>
</dl>

<br>

<h1>Cosa dicono gli utenti</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      Secondo me è il perfetto compromesso tra semplicità di utilizzo e
      stabilità. Negli anni ho utilizzato molte distribuzioni differenti
      ma Debian è l'unica che semplicemente funziona.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Saldo come una roccia. Tantissimi pacchetti. Comunità fantastica.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Debian è per me un esempio di stabilità e semplicità di utilizzo.
    </strong></q>
  </li>
</ul>
