#use wml::debian::template title="Promuovere un aspirante affiliato"
#use wml::debian::translation-check translation="a0723d79bbfc5ed2266ada5ffd402b2d3ab47405" maintainer="Francesca Ciceri"

<p>Prima di promuovere un aspirante affiliato si deve verificare che
soddisfi tutti i requisiti elencati nella <a href="./nm-checklist">checklist
del candidato</a>.</p>

<p>Si dovrebbe promuovere qualcuno <em>solo</em> quando si pensa che sia
pronto per essere un partecipante al progetto, questo vuol dire che già si conosce il
candidato e che si è potuto controllare il suo lavoro. È importante
che gli aspiranti affiliati abbiano già lavorato nel progetto per
qualche tempo. I modi in cui gli aspiranti affiliati possono
contribuire a Debian includono:</p>
<ul>
<li>mantenere uno o più pacchetti,</li>
<li>aiutare gli utenti,</li>
<li>lavorare per la promozione di Debian,</li>
<li>effettuare traduzioni,</li>
<li>partecipare all'organizzazione di DebConf,</li>
<li>inviare patch,</li>
<li>inviare bug report,</li>
<li>... e molto altro!</li>
</ul>

<p>Questi modi di contribuire, ovviamente, non si escludono a vicenda!
Molti aspiranti affiliati lavoreranno in aree differenti del
progetto, e non bisogna quindi necessariamente diventare manutentori di
pacchetti. Infatti <a
href="https://www.debian.org/vote/2010/vote_002">Debian dà il benvenuto ai
collaboratori che non si occupano di pacchettizzazione come membri del
progetto</a>. Se si desidera avere una più ampia panoramica su un certo
collaboratore, è possibile usare <a
href="http://ddportfolio.debian.net/">uno strumento che aggrega alcune
parti visibili al pubblico del progetto</a>. Inoltre, anche la conoscenza
personale del candidato è importante.
</p>

<p>L'aspirante affiliato dovrebbe avere familiarità con il modo
specifico di fare le cose proprio del progetto Debian e dovrebbe aver gi&agrave;
contribuito attivamente e in maniera efficace al progetto.
Altra cosa estremamente importante è che l'aspirante affiliato si
impegni ad aderire agli ideali e alla struttura del progetto Debian.
Chiedetevi se è desiderabile averlo in Debian &ndash; se si ritiene che
potrebbe essere un buon affiliato Debian, procedete ed incoraggiatelo
a candidarsi.
</p>

<p>I passi da seguire sono: accordarsi con il futuro affiliato per
raccomandarlo e questi <a href="https://nm.debian.org/public/newnm">presenta
la domanda</a>. Successivamente si deve andare su questo sito, fare clic
sul suo nome nella <a href="https://nm.debian.org/process/">lista dei
candidati</a>, andare alla pagina <q>promuovi questa candidatura</q>, inserire
la propria login Debian e premere il pulsante <q>submit</q>. Alla successiva
ricezione di una una e-mail con una chiave di autorizzazione, rispondere firmando con
la propria chiave GPG/PGP. Fatto questo, il futuro affiliato avrà un AM
assegnato che lo seguirà nel processo NM.</p>

<p>Consultare anche la pagina del wiki
<a href="https://wiki.debian.org/FrontDesk/AdvocacyTips">Advocacy Tips</a>.</p>
