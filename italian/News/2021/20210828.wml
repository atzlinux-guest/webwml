<define-tag pagetitle>Si è conclusa la DebConf21 online</define-tag>

<define-tag release_date>2021-08-28</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="eebf73f6f1306851590138e0b9fc3917cf4580a6" maintainer="Giuseppe Sacco"

<p>
Sabato 28 agosto 2021 l'annuale conferenza degli sviluppatori Debian
è arrivata a conclusione.
</p>

<p>
DebConf21 si è tenuta online per la seconda volta a causa della pandemia
del coronavirus (COVID-19).
</p>

<p>
Tutte le sessioni sono state trasmesse permettendo varie modalità di partecipazione:
tramite messaggi IRC, documenti per la collaborazione online, stanze per la video
conferenza.
</p>

<p>
Con 740 partecipanti registrati da oltre 15 nazioni, un totale di 70 sessioni
tra presentazioni e discussioni, incontri «Birds of a Feather (BoF)»
(incontri su temi specifici) e altre iniziative,
<a href="https://debconf21.debconf.org">DebConf21</a> è stata un grande successo.
</p>

<p>
La struttura consolidata durante i precedenti eventi online che
utilizzava Jitsi, OBS, Voctomix, SReview, nginx, Etherpad, un
frontend web per voctomix è stata ulteriormente migliorata e usata
con successo per DebConf21.
Tutti i componenti della infrastruttura video sono software libero e sono
stati configurati come mostrato nel «repository» pubblico del gruppo video
che utilizza <a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a>.
</p>

<p>
Il <a href="https://debconf21.debconf.org/schedule/">calendario</a> DebConf21 ha
incluso una pletora di eventi raggruppati in alcuni percorsi:
</p>
<ul>
<li>introduzione a software libero &amp; Debian,</li>
<li>pacchettizzazione, norme e infrastruttura Debian,</li>
<li>amministrazione di sistema, automazione e orchestrazione,</li>
<li>cloud e container,</li>
<li>sicurezza,</li>
<li>comunità, diversità, incontri locali e contesti sociali,</li>
<li>internazionalizzazione, localizzazione e accessibilità,</li>
<li>sistemi «embedded» &amp; kernel,</li>
<li>Debian Blends e distribuzioni derivate da Debian,</li>
<li>Debian nelle arti &amp; scienze</li>
<li>e altro.</li>
</ul>
<p>
Le presentazioni sono state trasmesse in due stanze, e varie di queste attività
sono state tenute in lingue diverse: telugu, portoghese, malese, kannada,
hindi, marathi e inglese, permettendo a audience diverse di partecipare agevolmente.
</p>

<p>
Negli intermezzi tra le presentazioni, la trasmissione ha mostrato gli sponsor,
in ciclo, oltre a video aggiuntivi che includevano foto di precedenti DebConf,
fatti divertenti su Debian e brevi interventi inviati dai partecipanti per comunicare
con i propri amici di Debian.
</p>

<p>
Il gruppo della pubblicità di Debian ha gestito la copertura mediatica per
incoraggiare la partecipazione con micro annunci per i diversi eventi. Il gruppo
DebConf ha anche fornito varie
<a href="https://debconf21.debconf.org/schedule/mobile/">opzioni per seguire il
programma su dispositivi mobili</a>.
</p>

<p>
Per quelli che non hanno potuto partecipare, gran parte delle presentazioni e delle sessioni
sono già disponibili sul
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/">sito web
degli incontri Debian</a>,
mentre quelle che mancano saranno disponibili nei prossimi giorni.
</p>

<p>
Il sito web <a href="https://debconf21.debconf.org/">DebConf21</a>
rimarrà attivo come archivio e continuerà ad offrire collegamenti
alle presentazioni e ai video di eventi.
</p>

<p>
L'anno prossimo, <a href="https://wiki.debian.org/DebConf/22">DebConf22</a> si terrà
a Prizren, in Cossovo, in luglio 2022.
</p>

<p>
DebConf offre un ambiente sicuro e accogliente a tutti i partecipanti.
Durante la conferenza vari gruppi (segreteria, accoglienza e gruppo della comunità)
sono stati disponibili per l'aiuto in modo che i partecipanti potessero avere
la migliore esperienza possibile, e hanno cercato di trovare soluzioni ad ogni
problema sollevato.
Vedere la <a href="https://debconf21.debconf.org/about/coc/">pagina web del
codice di condotta sul sito web DebConf21</a> per maggiori informazioni.
</p>

<p>
Debian ringrazia per la partecipazione numerosi <a href="https://debconf21.debconf.org/sponsors/">sponsor</a>
che hanno supportato DebConf21, in particolare gli sponsor platino:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://code4life.roche.com/">Roche</a>,
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>
e <a href="https://google.com/">Google</a>.
</p>

<h2>Su Debian</h2>
<p>
Il Progetto Debian è stato fondato nel 1993 con il fine di essere un
progetto di una comunità veramente libera. Da lì, il progetto è cresciuto
fino a diventare uno dei più vasti e influenti progetti open source.
Migliaia di volontari da tutto il mondo collaborano al fine di creare e di
mantenere il software Debian. Disponibile in 70 lingue e con una vasta gamma
di architetture supportate, Debian si definisce il
<q>sistema operativo universale</q>.
</p>


<h2>Su DebConf</h2>

<p>
DebConf è la conferenza degli sviluppatori del progetto Debian. Oltre ad un 
programma fitto di interventi tecnici, sociali e relativi alla policy, DebConf 
offre agli sviluppatori, ai collaboratori e alle altre persone interessate 
un'opportunità per incontrarsi di persona e per lavorare insieme a più stretto 
contatto. È stata organizzata ogni anno, a partire dal 2000, in varie sedi come
Scozia, Argentina e Bosnia-Erzegovina. Maggiori informazioni su DebConf sono
disponibili all'indirizzo <a href="https://debconf.org/">https://debconf.org</a>.
</p>


<h2>Su Lenovo</h2>
<p>
In qualità di leader tecnologico globale e produttore di una gran quantità di
prodotti connessi, inclusi telefoni cellulari, tablet, computer personali e da
lavoro, oltre che dispositivi AR/VR e soluzioni per casa/ufficio e datacentre,
<a href="https://www.lenovo.com">Lenovo</a> sa bene quanto siano critici i sistemi
e le piattaforme aperte nel mondo connesso.
</p>

<h2>Su Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> è la più grande società
di «hosting» web svizzera che offre anche servizi per il backup e lo «storage»,
per supportare organizzatori di eventi, dirette streaming e video a richiesta.
Posside interamente i propri datacentre e tutti gli elementi importanti per il
funzionamento dei servizi e dei prodotti forniti dall'azienda (sia
software che hardware).
</p>

<h2>Su Roche</h2>
<p>
<a href="https://code4life.roche.com/">Roche</a> è una delle maggiori società di
ricerca e fornitura del settore farmaceutico legata alla salute.
Più di 100.000 dipendenti in tutto il mondo lavorano per risolvere alcune delle
grandi sfide dell'umanità usando scienza e tecnologia. Roche è fortemente
legata a progetti di ricerca e collaborazione con finanziamenti pubblici con
altri partner industriali e accademici, inoltre supporta DebConf dal 2017.
</p>

<h2>Su Amazon Web Services (AWS)</h2>
<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> è
una delle maggiori piattaforme cloud più complete e maggiormente usate, offre
più di 175 servizi da datacentre globali (divisi in 77 zone distruibite in
24 regioni geografiche).
I clienti AWS includono le startup a grande crescita, le grandi imprese e
le maggiori agenzie governative.
</p>

<h2>Su Google</h2>
<p>
<a href="https://google.com/">Google</a> è una delle più grandi società
tecnologiche al mondo. Offre un grande porafoglio di servizi legati ad Internet
e di prodotti tecnologici quali la pubblicità online, la ricerca, supporto alle
applicazioni cloud, software e hardware.
</p>
<p>
Google supporta Debian sponsorizzando DebConf da oltre 10 anni ed è anche
un partner Debian che sponsorizza parte dell'infrastruttura per
l'integrazione continua di <a href="https://salsa.debian.org">Salsa</a>
all'interno della piattaforma cloud di Google.
</p>

<h2>Per maggiori informazioni</h2>

<p>Per ulteriori informazioni visitare le pagine web DebConf21 su
<a href="https://debconf21.debconf.org/">https://debconf21.debconf.org/</a>
o inviare un email a &lt;press@debian.org&gt;.</p>
