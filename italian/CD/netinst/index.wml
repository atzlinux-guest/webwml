#use wml::debian::cdimage title="Installazione via rete da un CD minimale" BARETITLE=true
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="cf13aff27aee3763a70db6bb4c8d2087bea5716e" maintainer="Luca Monducci"

<p>Un CD per l'<q>installazione via rete</q> o <q>netinst</q> è un
semplice CD che permette di installare l'intero sistema operativo.
Questo CD contiene la quantità minima di software necessaria per
scaricare dalla rete i restanti pacchetti.</p>

<p><strong>Cos'è meglio: il CD-ROM minimale autoavviante o i CD
completi?</strong>
Dipende, in molti casi l'immagine del CD minimale è la scelta migliore
soprattutto perché verranno scaricati solo i pacchetti che si sceglie di
installare sulla propria macchina, risparmiando tempo e banda. D'altro
canto, i CD completi sono più comodi quando si fanno installazioni su
più macchine o su macchine non dotate di una connessione Internet.</p>

<p><strong>Quali tipi di connessione alla rete sono supportati durante
l'installazione?</strong>
È ovvio che per l'installazione via rete è necessaria la disponibilit&agrave;
di un collegamento a Internet. Sono supportate diverse modalità di
collegamento tra le quali dial-up PPP analogico, Ethernet e WLAN
(con qualche limitazione); purtroppo ISDN non è supportato,
siamo spiacenti!</p>

<p>Sono disponibili per il download le seguenti immagini avviabili
di CD minimali:</p>

<ul>
  <li>Immagini ufficiali <q>netinst</q> per il rilascio <q>stable</q>,
  <a href="#netinst-stable">vedere sotto</a>.</li>

  <li>Immagini per il rilascio <q>testing</q> sia giornaliere sia
  già provate con successo, vedere la
  <a href="$(DEVEL)/debian-installer/">pagina dell'installatore
  Debian</a>.</li>
</ul>


<h2 id="netinst-stable">Immagini ufficiali <q>netinst</q> per il
rilascio <q>stable</q></h2>

<p>Fino ad una dimensione massima di 300&nbsp;MB, l'immagine contiene
l'installatore e una ridotta selezione di pacchetti che permettono
l'installazione di un sistema (molto) minimale.</p>

<div class="line">
<div class="item col50">
<p><strong>
Immagine del CD netinst (circa 180-300&nbsp;MB, dipende dall'architettura)
</strong></p>
  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>
Immagine del CD netinst (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)
</strong></p>
  <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>Le informazioni su questi file e su come usarli sono contenute
nelle <a href="../faq/">FAQ</a>.</p>

<p>Dopo aver scaricato le immagini, si consiglia di leggere le
<a href="$(HOME)/releases/stable/installmanual">istruzioni dettagliate
di installazione</a>.</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Se sul proprio sistema è presente un qualsiasi hardware che
<strong>richiede il caricamento di firmware non-free</strong> insieme ai
driver del dispositivo, è possibile usare uno dei <a
href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/bullseye/current/">tarball
con i pacchetti firmware più comuni</a> oppure scaricare
un'immagine <strong>non ufficiale</strong> che contiene i firmware
<strong>non-free</strong>. Le istruzioni su come usare questi tarball e
le informazioni su come caricare il firmware durante l'installazione
possono essere trovate nella <a href="../amd64/ch06s04">guida
all'installazione</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">immagini
non ufficiali di <q>stable</q> con firmware incluso</a>
</p>
</div>
