#use wml::debian::translation-check translation="04abf1830d2945ee74e96edb5e11e9a2fd9a1f60" maintainer="Szabolcs Siebenhofer"
<define-tag description>LTS biztonsági frissítés</define-tag>
<define-tag moreinfo>
<p>Néhány sebezhetőségi probléma kapcsolódik a pacemaker-hez, a klaszter erőforrás 
menedzserhez.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16877">CVE-2018-16877</a>

    <p>AHibát találtal a pacemaker kliens-szerver authentikációjának megvalósításában. 
    A helyi támadó ki tudja használni ezt a hibát, kombinálva más IPC gyengeségekkel, 
    hogy helyi jogosultságokat szerezzen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16878">CVE-2018-16878</a>

    <p>A nem megfelelő ellenőrzés miatt az ellenőrizetlen folyamatok előnyben részesítése a 
    szolgáltatás megtagadásához vezethet.
    </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25654">CVE-2020-25654</a>

    <p>ACL megkerülési hibát találtak a peacemaker-ben. A támadó rendelkezik helyi fiókkal a 
    kalszteren és a haclient csoport közvetlen IPC kommunikációt tud használni különféle 
    háttérben futó folyamatokkal, hogy feladatokat hajtson végre, az ACL megakadályozza őket 
    abbban, hogy elvégezzék a konfigurávciót.
    </p></li>

</ul>

<p>A Debian 9 <q>stretch</q> esetén a probléma a 1.1.24-0+deb9u1 verzióban javításra 
került.</p>

<p>Azt tanácsoljuk, hogy frissítsd a pacemaker csomagjaidat.</p>

<p>A pacemaker csomag biztonság állapotával kapcsolatban lásd a bizonsági 
nyomkövető oldalát:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/pacemaker">https://security-tracker.debian.org/tracker/pacemaker</a></p>

<p>További információk a Debian LTS biztonági figyelmeztetéseiről, hogyan tudod ezeket a 
frissítéseket a rendszereden telepíteni és más gyakran feltett kérdések megtalálhatóak itt: 
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2519.data"
# $Id: $
