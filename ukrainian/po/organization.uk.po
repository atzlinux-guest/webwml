# translation of organization.po to Ukrainian
# Eugeniy Meshcheryakov <eugen@debian.org>, 2005, 2006.
# Volodymyr Bodenchuk <Bodenchuk@bigmir.net>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: organization\n"
"PO-Revision-Date: 2017-11-18 16:30+0200\n"
"Last-Translator: Volodymyr Bodenchuk <Bodenchuk@bigmir.net>\n"
"Language-Team: українська <Ukrainian <ukrainian>>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.7\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n "
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>делегат"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>делегат"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"female\"/>делегат"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"female\"/>делегат"

#: ../../english/intro/organization.data:24
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"female\"/>делегат"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"female\"/>делегат"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "чинний"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "член"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "керівник"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "КСВ"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Керівник стабільного випуску"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "чарівник"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "голова"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "помічник"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "секретар"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Керівники"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Дистрибутив"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr "Зв'язок та інформаційна підтримка"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
msgid "Publicity team"
msgstr "Команда зв'язків з громадськістю"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "Підтримка та інфраструктура"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Лідер"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Технічний комітет"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Секретар"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Проекти розробки"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP-архіви"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "FTP-майстри"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "FTP-помічники"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "FTP-чарівники"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Зворотнє перенесення"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Команда зворотнього перенесення"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Керування випуском"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Команда, відповідальна за випуск"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Забезпечення якості"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Команда системи встановлення"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Інформація про випуск"

#: ../../english/intro/organization.data:152
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "Образи компакт-дисків"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Виробництво"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Тестування"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr "Інфраструктура автоматичного збирання"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr "Команда підготовки до збирання"

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr "Адміністрування збирання"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "Документація"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "Список пакунків що потребують доробки або плануються"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "Контакт для преси"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "Веб-сторінки"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Планета Debian"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr "Інформаційна підтримка"

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Проект Жінки Debian"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "Події"

#: ../../english/intro/organization.data:264
msgid "DebConf Committee"
msgstr "Комітет DebConf"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "Партнерська програма"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "Координація пожертвування обладнання"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "Система відслідковування помилок"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Керування списками розсилки та архіви списків розсилки"

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr "Реєстраційний стіл нових членів"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Керівники облікових записів Debian (КОЗД)"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Щоб надіслати приватне повідомлення усім КОЗД, використовуйте ключ GPG "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Супроводжуючі зв'язок ключів (PGP та GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "Команда безпеки"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "Політика"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "Системне адміністрування"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Це адреса, яку потрібно використовувати у випадку виникнення проблем з "
"машинами Debian, включно з проблемами з паролями, або якщо вам потрібно "
"встановити пакунок."

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Якщо ви помітили проблему з апаратурою машини Debian, перегляньте сторінку "
"<a href=\"https://db.debian.org/machines.cgi\">Машини Debian</a>, вона "
"повинна містити інформацію про адміністраторів кожної машини."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "Адміністратор LDAP-каталогу розробників"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "Дзеркала"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "Супроводжуючий DNS"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "Система відслідковування пакунків"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Запити на використання <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">торгової марки</a>"

#: ../../english/intro/organization.data:392
#, fuzzy
msgid "Salsa administrators"
msgstr "Адміністратори Alioth"

#~ msgid "Anti-harassment"
#~ msgstr "Анти-домагання"

#~ msgid "Debian Pure Blends"
#~ msgstr "Чисті суміші Debian"

#~ msgid "Individual Packages"
#~ msgstr "Окремі пакунки пакунки"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian для дітей від 1 до 99"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian для медичної практики та досліджень"

#~ msgid "Debian for education"
#~ msgstr "Debian для освіти"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian в адвокатурах"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian для людей з обмеженими можливостями"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian для науки та суміжних досліджень"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian для освіти"

#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "Команда робочої системи"

#~ msgid "Auditor"
#~ msgstr "Ревізор"

#~ msgid "Publicity"
#~ msgstr "Зв'язки з громадскістю"

#~ msgid "Release Assistants"
#~ msgstr "Помічники керівника випуском"

#~ msgid "Release Manager for ``stable''"
#~ msgstr "Керівник випуском „stable“"

#~ msgid "Vendors"
#~ msgstr "Постачальники"

#~ msgid "APT Team"
#~ msgstr "Команда APT"

#~ msgid "Mailing List Archives"
#~ msgstr "Архіви списків розсилки"

#~ msgid "Security Testing Team"
#~ msgstr "Команда безпеки тестового дистрибутиву"

#~ msgid "Key Signing Coordination"
#~ msgstr "Координація підписування ключів"

#~ msgid "Accountant"
#~ msgstr "Рахівник"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Універсальна операційна система як робочій стіл"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian для некомерційних організацій"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian для підприємств"

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Мультимедійний дистрибутив Debian"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Це ще не є офіційний внутрішній проект Debian, але оголошення про "
#~ "прагнення інтеграції."

#~ msgid "Security Audit Project"
#~ msgstr "Проект перевірки безпеки"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "Команда безпеки"

#~ msgid "Alioth administrators"
#~ msgstr "Адміністратори Alioth"

#~ msgid "User support"
#~ msgstr "Підтримка користувачів"

#~ msgid "Embedded systems"
#~ msgstr "Вбудовані системи"

#~ msgid "Firewalls"
#~ msgstr "Брандмауери"

#~ msgid "Laptops"
#~ msgstr "Лептопи"

#~ msgid "Special Configurations"
#~ msgstr "Спеціальні конфігурації"

#~ msgid "Ports"
#~ msgstr "Перенесення"

#~ msgid "CD Vendors Page"
#~ msgstr "Сторінка постачальників компакт-дисків"

#~ msgid "Consultants Page"
#~ msgstr "Сторінка консультантів"
