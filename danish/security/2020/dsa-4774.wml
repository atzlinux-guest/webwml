#use wml::debian::translation-check translation="43f8d7b8b91b167696b5c84ec0911bab7b7073f2" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til udførelse 
af vilkårlig kode, rettighedsforøgelse, lammelsesangreb eller 
informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12351">CVE-2020-12351</a>

    <p>Andy Nguyen opdagede en fejl i Bluetooth-implementeringen i den måde, 
    L2CAP-pakker med A2MP CID håndteres.  En fjernangriber indenfor kort 
    afstand, med kendskab til offerets Bluetooth-enhedsadresse, kunne sende en 
    ondsindet l2cap-pakke, og forårsage et lammelsesangreb eller muligvis 
    udførelse af vilkårlig kode med kernerettigheder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12352">CVE-2020-12352</a>

    <p>Andy Nguyen opdagede en fejl i Bluetooth-implementeringen.  
    Stakhukommelse blev ikke initialiseret på korrekt vis, ved håndtering af 
    visse AMP-pakker.  En fjernangriber indenfor kort afstand, med kendskab til 
    offerets Bluetooth-enhedsadresse, kunne få fat i 
    kernestakoplysninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25211">CVE-2020-25211</a>

    <p>En fejl blev opdaget i netfilter-undersystemet.  En lokal angriber, der 
    er i stand til at indsprøjte conntrack Netlink-opsætning, kunne forårsage et 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25643">CVE-2020-25643</a>

    <p>ChenNan fra Chaitin Security Research Lab opdagede en fejl i modulet 
    hdlc_ppp.  Ukorrekt validering af inddata i funktionen the ppp_cp_parse_cr() 
    kunne føre til hukommelseskorruption og informationsafsløring.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25645">CVE-2020-25645</a>

    <p>En fejl blev opdaget i grænsefladedriveren til GENEVE-indkapslet trafik, 
    når den er kombineret med IPsec.  Hvis IPsec er opsat til at kryptere 
    trafik til den specifikke UDP-port, som anvendes af GENEVE-tunnellen, blev 
    data ledt gennem tunnellen ikke på korrekt vis route't over den krypterede 
    forbindelse, og blev i stedet sendt ukrypteret.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 4.19.152-1.  Sårbarhederne er rettet ved at rebase til den nye stabile 
opstrømsversion 4.19.152, der indeholder yderligere fejlrettelser.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4774.data"
