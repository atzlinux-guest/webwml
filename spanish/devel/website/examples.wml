#use wml::debian::template title="Ejemplos"
#use wml::debian::translation-check translation="ceca0da4950426a38fa87b51efcfd25749d8995a"

<h3>Ejemplo de cómo se comienza una traducción</h3>

<p>Usaremos el idioma francés para el ejemplo:

<pre>
   git pull
   cd webwml
   mkdir french
   cd french
   cp ../english/.wmlrc ../english/Make.* .
   echo '<protect>include $(subst webwml/french,webwml/english,$(CURDIR))/Makefile</protect>' &gt; Makefile
   mkdir po
   git add Make* .wmlrc
   cp Makefile po
   make -C po init-po
   git add po/Makefile po/*.fr.po
</pre>

<p>Edite el fichero <tt>.wmlrc</tt> y modifique:
<ul>
  <li>'-D CUR_LANG=English' por '-D CUR_LANG=French'
  <li>'-D CUR_ISO_LANG=en' por '-D CUR_ISO_LANG=fr'
  <li>'-D CUR_LOCALE=en_US' por '-D CUR_LOCALE=fr_FR'
  <li>'CHARSET=iso-8859-1' por lo que sea apropiado.<br>
      Parece que el francés usa la misma codificación de caracteres que
      el inglés, de manera que no hace falta cambiarlo, sin embargo, es
      probable que otros idiomas necesiten ajustar este valor.
</ul>

<p>Edite Make.lang y cambie 'LANGUAGE := en' por 'LANGUAGE := fr'.
En el caso de que traduzca a un idioma que use más de un byte por carácter (multi-byte),
puede que tenga que cambiar algunas otras variables en ese fichero, para más
información lea ../Makefile.common y a lo mejor otros ejemplos de trabajo
(traducciones como la china).

<p>Vaya al directorio french/po y traduzca las cadenas de los ficheros PO.
Esto debería ser bastante intuitivo.

<p>Asegúrese siempre de que copia el Makefile a cada directorio que
traduce. Es necesario porque se usa el programa <code>make</code> para convertir
los .wml en HTML y <code>make</code> usa Makefiles.

<p>Cuando acabe de añadir y editar páginas, haga:
<pre>
   git commit -m "Add your commit message here"
   git push
</pre>
desde el directorio webwml. Ahora puede empezar a traducir las páginas.

<h3>Ejemplo de traducción de una página</h3>

<p>Usaremos la traducción del contrato social al francés como ejemplo:

<pre>
   cd webwml
   ./copypage.pl english/social_contract.wml
   cd french
</pre>

<p>Esto añadirá automáticamente la cabecera «translation-check» apuntando a
la versión del fichero original que se copió. También crea el directorio destino y
el fichero «Makefile», si no existieran.</p>

<p>Edite social_contract.wml y traduzca el texto. No trate de traducir o modificar
ningún enlace de forma alguna - si quiere cambiar cualquier cosa, notifíquelo en la
lista debian-www. Cuando termine, escriba:

<pre>
   git add social_contract.wml
   git commit -m "Translated social contract to french"
   git push
</pre>

<h3>Ejemplo de adición de un nuevo directorio</h3>

<p>Este ejemplo muestra cómo añadir el directorio intro/ a la traducción francesa:

<pre>
   cd webwml/french
   mkdir intro
   cd intro
   cp ../Makefile .
   git add Makefile
   git commit -m "added the intro dir to git"
   git push
</pre>

Asegúrese de que el nuevo directorio tiene el Makefile y de que ha hecho el commit en git.
De otra forma, dará un error a cualquiera que intente ejecutar make.


<h3>Ejemplo de un conflicto</h3>

<p>Este ejemplo muestra un commit fallido por haberse modificado
la copia en el repositorio desde que ejecutó <kbd>git pull</kbd> por última vez.</p>

<p>Usted hace algunas modificaciones en el fichero foo.wml. Cuando, más adelante, ejecute:</p>

<pre>
   git add foo.wml
   git commit -m "fixed a broken link"
   git push
</pre>

recibirá los mensajes:

<pre>
To salsa.debian.org:webmaster-team/webwml.git
 ! [rejected]                master -> master (fetch first)
error: falló el push de algunas referencias a 'git@salsa.debian.org:webmaster-team/webwml.git'
</pre>

<p>o algo parecido :)
      <br />
      <br />
   Esto significa que sus cambios <strong>no</strong> se han subido al repositorio
   git por la existencia de conflictos.
      <br />
   Tendrá que determinar qué ha ido mal, resolver los conflictos e intentar
   de nuevo las órdenes commit/push.</p>
