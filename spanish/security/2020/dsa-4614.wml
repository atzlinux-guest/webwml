#use wml::debian::translation-check translation="10d6441f58b80849b1c23b3599dafc9a773a04f2"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Joe Vennix descubrió una vulnerabilidad de desbordamiento de pila en
sudo, un programa diseñado para proporcionar a usuarios específicos privilegios limitados
de superusuario. La vulnerabilidad se puede desencadenar cuando sudo se ha configurado con la opción <q>pwfeedback</q>
habilitada. Un usuario sin privilegios se puede aprovechar de este defecto para obtener
privilegios completos de root.</p>

<p>Se pueden encontrar detalles en el aviso del proyecto original: 
<a href="https://www.sudo.ws/alerts/pwfeedback.html">https://www.sudo.ws/alerts/pwfeedback.html</a> .</p>

<p>Para la distribución «antigua estable» (stretch), este problema se ha corregido
en la versión 1.8.19p1-2.1+deb9u2.</p>

<p>Para la distribución «estable» (buster), un cambio en la gestión de
EOF introducido en la 1.8.26 impide la explotación de este fallo.</p>

<p>Le recomendamos que actualice los paquetes de sudo.</p>

<p>Para información detallada sobre el estado de seguridad de sudo, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/sudo">https://security-tracker.debian.org/tracker/sudo</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4614.data"
