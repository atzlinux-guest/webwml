#use wml::debian::translation-check translation="2a3fa2ac1b49f491737f3c89828986a43c00a61e"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.9</define-tag>
<define-tag release_date>2021-03-27</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la novena actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction avahi "Elimina el mecanismo avahi-daemon-check-dns, que ya no es necesario">
<correction base-files "Actualiza /etc/debian_version para la versión 10.9">
<correction cloud-init "Evita la escritura de las contraseñas generadas, en ficheros de log con permisos universales de lectura [CVE-2021-3429]">
<correction debian-archive-keyring "Añade las claves de bullseye; retira las claves de jessie">
<correction debian-installer "Usa la ABI del núcleo Linux 4.19.0-16">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction exim4 "Corrige el uso de conexiones TLS concurrentes bajo GnuTLS; corrige la verificación de certificados TLS con CNAME; README.Debian: documenta la limitación/alcance de la verificación de certificados de servidor en la configuración por omisión">
<correction fetchmail "Deja de reportar <q>System error during SSL_connect(): Success</q> («Error de sistema al ejecutar SSL_connect(): sin errores»); elimina la comprobación de la versión de OpenSSL">
<correction fwupd "Añade soporte de SBAT">
<correction fwupd-amd64-signed "Añade soporte de SBAT">
<correction fwupd-arm64-signed "Añade soporte de SBAT">
<correction fwupd-armhf-signed "Añade soporte de SBAT">
<correction fwupd-i386-signed "Añade soporte de SBAT">
<correction fwupdate "Añade soporte de SBAT">
<correction fwupdate-amd64-signed "Añade soporte de SBAT">
<correction fwupdate-arm64-signed "Añade soporte de SBAT">
<correction fwupdate-armhf-signed "Añade soporte de SBAT">
<correction fwupdate-i386-signed "Añade soporte de SBAT">
<correction gdnsd "Corrige desbordamiento de pila con direcciones IPv6 excesivamente largas [CVE-2019-13952]">
<correction groff "Recompilado contra ghostscript 9.27">
<correction hwloc-contrib "Habilita soporte para la arquitectura ppc64el">
<correction intel-microcode "Actualiza varios microcódigos">
<correction iputils "Corrige errores de redondeo de ping; corrige la corrupción del destino de tracepath">
<correction jquery "Corrige vulnerabilidades de ejecución de código no confiable [CVE-2020-11022 CVE-2020-11023]">
<correction libbsd "Corrige problema de lectura fuera de límites [CVE-2019-20367]">
<correction libpano13 "Corrige vulnerabilidad de cadena de caracteres de formato">
<correction libreoffice "No carga encodings.py del directorio actual">
<correction linux "Nueva versión «estable» del proyecto original; actualiza la ABI a la -16; rota las claves de firma de arranque seguro; rt: actualizado a la 4.19.173-rt72">
<correction linux-latest "Actualizado a la ABI -15 del núcleo; actualizado a la ABI -16 del núcleo">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original; actualiza la ABI a la -16; rota las claves de firma de arranque seguro; rt: actualizado a la 4.19.173-rt72">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original; actualiza la ABI a la -16; rota las claves de firma de arranque seguro; rt: actualizado a la 4.19.173-rt72">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original; actualiza la ABI a la -16; rota las claves de firma de arranque seguro; rt: actualizado a la 4.19.173-rt72">
<correction lirc "Normaliza el valor ${DEB_HOST_MULTIARCH} incluido en /etc/lirc/lirc_options.conf para encontrar ficheros de configuración sin modificar en todas las arquitecturas; recomienda gir1.2-vte-2.91 en lugar del inexistente gir1.2-vte">
<correction m2crypto "Corrige fallo en prueba con versiones recientes de OpenSSL">
<correction openafs "Corrige conexiones salientes posteriores al tiempo Unix 0x60000000 (14 de enero de 2021)">
<correction portaudio19 "Trata EPIPE devuelto por alsa_snd_pcm_poll_descriptors, corrigiendo caída">
<correction postgresql-11 "Nueva versión «estable» del proyecto original; corrige fuga de información en mensajes de error de constraint-violation [CVE-2021-3393]; corrige CREATE INDEX CONCURRENTLY para que espere a las transacciones preparadas concurrentemente">
<correction privoxy "Problemas de seguridad [CVE-2020-35502 CVE-2021-20209 CVE-2021-20210 CVE-2021-20211 CVE-2021-20212 CVE-2021-20213 CVE-2021-20214 CVE-2021-20215 CVE-2021-20216 CVE-2021-20217 CVE-2021-20272 CVE-2021-20273 CVE-2021-20275 CVE-2021-20276]">
<correction python3.7 "Corrige inyección de CRLF en http.client [CVE-2020-26116]; corrige desbordamiento de memoria en PyCArg_repr de _ctypes/callproc.c [CVE-2021-3177]">
<correction redis "Corrige una serie de problemas de desbordamiento de entero en sistemas de 32 bits [CVE-2021-21309]">
<correction ruby-mechanize "Corrige problema de inyección de órdenes [CVE-2021-21289]">
<correction systemd "core: se asegura de restaurar también el identificativo de la orden de control, corrigiendo una violación de acceso; seccomp: permite la desactivación del filtrado de seccomp por medio de una variable de entorno">
<correction uim "libuim-data: realiza conversión symlink_to_dir («de enlace simbólico a directorio») de /usr/share/doc/libuim-data en el paquete resucitado para conseguir actualizaciones limpias desde stretch">
<correction xcftools "Corrige vulnerabilidad de desbordamiento de entero [CVE-2019-5086 CVE-2019-5087]">
<correction xterm "Corrige el límite superior del área de memoria para la selección, teniendo en cuenta los caracteres combinados [CVE-2021-27135]">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2021 4826 nodejs>
<dsa 2021 4844 dnsmasq>
<dsa 2021 4845 openldap>
<dsa 2021 4846 chromium>
<dsa 2021 4847 connman>
<dsa 2021 4849 firejail>
<dsa 2021 4850 libzstd>
<dsa 2021 4851 subversion>
<dsa 2021 4853 spip>
<dsa 2021 4854 webkit2gtk>
<dsa 2021 4855 openssl>
<dsa 2021 4856 php7.3>
<dsa 2021 4857 bind9>
<dsa 2021 4858 chromium>
<dsa 2021 4859 libzstd>
<dsa 2021 4860 openldap>
<dsa 2021 4861 screen>
<dsa 2021 4862 firefox-esr>
<dsa 2021 4863 nodejs>
<dsa 2021 4864 python-aiohttp>
<dsa 2021 4865 docker.io>
<dsa 2021 4867 grub-efi-amd64-signed>
<dsa 2021 4867 grub-efi-arm64-signed>
<dsa 2021 4867 grub-efi-ia32-signed>
<dsa 2021 4867 grub2>
<dsa 2021 4868 flatpak>
<dsa 2021 4869 tiff>
<dsa 2021 4870 pygments>
<dsa 2021 4871 tor>
<dsa 2021 4872 shibboleth-sp>
</table>



<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


