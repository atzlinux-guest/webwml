#use wml::debian::template title="Debian &ldquo;etch&rdquo; installatie-informatie" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/etch/release.data"
#use wml::debian::translation-check translation="f36546f515e33fb0e590b3db17a516bf3d605f5f"

<h1>Debian GNU/Linux installeren <current_release_etch></h1>

<p><strong>Debian GNU/Linux 4.0 werd vervangen door
<a href="../../lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a>. Sommige van
deze installatie-images zijn mogelijk niet langer beschikbaar of werken niet
meer. Het wordt aanbevolen om in de plaats daarvan lenny te installeren.
</strong></p>


<p>
<strong>Voor de installatie van Debian GNU/Linux</strong> <current_release_etch>
(<em>etch</em>), kunt u een van de volgende images downloaden:
</p>

<div class="line">
<div class="item col50">
        <p><strong>netinst cd-image (meestal 135-175 MB)</strong></p>
                <netinst-images />
</div>

<div class="item col50 lastcol">
        <p><strong>visitekaart-cd-image (meestal 20-50 MB)</strong></p>
                <businesscard-images />
</div>
</div>

<div class="line">
<div class="item col50">
        <p><strong>volledige cd-sets</strong></p>
                <full-cd-images />
</div>

<div class="item col50 lastcol">
        <p><strong>volledige dvd-sets</strong></p>
                <full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<p><strong>cd (via <a
href="$(HOME)/CD/torrent-cd">bittorrent)</a></strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>dvd (via <a href="$(HOME)/CD/torrent-cd">bittorrent)</a></strong></p>
<full-dvd-torrent />
</div>
</div>

<div class="line">
<div class="item col50">
<p><strong>cd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>dvd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<p><strong>andere images (netboot, usb-stick, enz.)</strong></p>
<other-images />
</div>
</div>


<p>
<strong>Opmerkingen</strong>
</p>
<ul>
    <if-etchnhalf-released released="yes"><li>
	Informatie over <strong>het installeren van Debian GNU/Linux
	<q>etch-eneenhalf</q></strong> (met een opgewaardeerde 2.6.24 kernel) is
	te vinden op een <a href="etchnhalf">aparte pagina</a>.
    </li></if-etchnhalf-released>
    <li>
	De multi-arch <em>cd</em>-images zijn respectievelijk bedoeld voor
    i386/amd64/powerpc en alpha/hppa/ia64; de installatie is vergelijkbaar met
    een installatie met een netinst-image voor één enkele architectuur.
    </li><li>
	Het multi-arch <em>dvd</em>-image is bedoeld voor i386/amd64/powerpc; de
	installatie is vergelijkbaar met een installatie met een
	volledig cd-image voor één enkele architectuur. De dvd bevat ook
	alle broncode voor de opgenomen pakketten.
    </li><li>
	Voor de installatie-images zijn verificatiebestanden (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> en andere) te vinden in dezelfde map als de images.
    </li>
</ul>


<h1>Documentatie</h1>

<p>
<strong>Indien u slechts één document leest</strong> voor u met installeren
begint, lees dan onze <a href="../i386/apa">Installatie-Howto</a> met een snel
overzicht van het installatieproces. Andere nuttige informatie is:
</p>

<ul>
<li><a href="../installmanual">Etch Installatiehandleiding</a><br />
met uitgebreide installatie-instructies</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
en <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
met algemene vragen en antwoorden</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
met door de gemeenschap onderhouden documentatie</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Dit is een lijst met bekende problemen in het installatieprogramma dat met
Debian GNU/Linux <current_release_etch> wordt geleverd. Indien u bij het
installeren van Debian op een probleem gestoten bent en dit probleem hier niet
vermeld vindt, stuur ons dan een
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">installatierapport</a>
waarin u het probleem beschrijft of
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">raadpleeg de wiki</a>
voor andere gekende problemen.
</p>

<h3 id="errata-r3">Errata voor release 4.0r3</h3>

<p>Deze release lost de twee onderstaande problemen voor 4.0r2 op.</p>

<h3 id="errata-r2">Errata voor release 4.0r2</h3>

<p>Deze uitgave lost het probleem op in verband met het <q>instellen van de
seriële console met GRUB</q>, zoals hieronder vermeld wordt voor de uitgave
4.0r0.</p>

<p>Door een onoplettendheid gebruikt het installatieprogramma niet de
kernelversie die is uitgebracht met 4.0r2, maar een iets oudere
(2.6.18.dfsg.1-13). Modules welke later in de installatie worden geladen,
kunnen van een latere versie zijn (2.6.18.dfsg.1-16), maar omdat deze twee
versies ABI-compatibel zijn, zou dit geen probleem mogen zijn.</p>

<p>Om dezelfde reden is de in de <a href="$(HOME)/News/2007/20071227">releaseaankondiging</a> vermelde ondersteuning voor
Nevada SGI O2 machines niet echt opgenomen in deze release.</p>

<p>Deze twee problemen zullen opgelost worden met de volgende
tussenrelease voor Etch.</p>

<h3 id="errata-r1">Errata voor release 4.0r1</h3>

<p>Deze nieuwe release lost enkele problemen op die in de originele
4.0r0 release aanwezig waren.
Meer USB cd-stations zullen gevonden worden en <tt>gksu</tt> zal correct
geconfigureerd worden bij een installatie met een gedeactiveerd
root-account (<tt>sudo</tt>-modus). Het aanmaken van
<tt>ext2</tt>-bestandssystemen is nu mogelijk op amd64.</p>

<p>Ook de lijst met spiegelservers werd geactualiseerd, samen met de Catalaanse
en de Roemeense vertaling van het schijfindelingsprogramma.</p>

<p>Met de update van de kernel voor deze versie, zullen sommige
installatie-images die oorspronkelijk met etch 4.0r0 zijn uitgebracht (netboot
en diskettes) niet langer werken vanwege een niet-overeenkomende kernelversie.
Installaties met cd worden <strong>niet</strong> getroffen.</p>

<h3 id="errata-r0">Errata voor release 4.0r0</h3>

<dl class="gloss">
	<dt>Schijfapparaten kunnen bij het opnieuw opstarten veranderen</dt>
	<dd>
	Op systemen met meerdere schijfcontrollers kan door een andere volgorde
    waarin stuurprogramma's geladen worden, de kernel/udev
    bij het herstarten van het systeem een andere apparaatnode toekennen dan
    tijdens de installatie het geval was.<br />
	Dit kan ertoe leiden dat het systeem niet opgestart kan worden. In de
    meeste gevallen kan dit gecorrigeerd worden door de configuratie van het
    opstartprogramma en /etc/fstab te wijzigen, mogelijk door gebruik te maken
    van de reddingsmodus van het installatiesysteem.<br />
	Houd er echter rekening mee dat dit probleem zich opnieuw kan voordoen
    wanneer de volgende keer opgestart wordt. Het oplossen van dit probleem
    heeft prioriteit voor de volgende release van het
    Debian-installatieprogramma.
	</dd>

	<dt>De grootte van bestaande ext3-partities aanpassen kan mislukken</dt>
	<dd>
	Momenteel is het installatieprogramma niet in staat om de grootte van
    ext3-partities waarvoor de functies <tt>dir_index</tt> en/of
    <tt>resize_inode</tt> geactiveerd zijn, aan te passen. Dit geldt ook voor
    ext3-partities die werden aangemaakt met het installatieprogramma van
    Etch.<br />
	U kunt de grootte van een ext3-partitie echter handmatig aanpassen tijdens
    de installatie. Ga door tot aan de schijfindelingsstap, gebruik dan de knop
    &lt;Terug&gt; om terug te keren naar het hoofdmenu, start vervolgens een
    shell en pas de grootte van het bestandssysteem aan en voer een
    schijfindeling uit met de commando's die in de shell ter beschikking staan
    (<tt>fsck.ext3</tt>, <tt>resize2fs</tt> en bijv. <tt>fdisk</tt> of
    <tt>parted</tt>). Indien u na het voltooien van de aanpassing van de
    grootte opnieuw het programma partman start, zou dit de nieuwe grootte van
    de partitie moeten weergeven.
	</dd>

	<dt>Fout bij gebruik van encryptie met loop-aes tijdens een installatie met
    hd-media</dt>
	<dd>
	Indien encryptie met behulp van loop-aes gebruikt wordt tijdens een
    installatie met de hd-media-images (bijv. vanaf een USB-stick), kan deze
    mislukken met de volgende (of soortgelijke) foutmelding in het
    syslog-bestand (<a href="https://bugs.debian.org/434027">#434027</a>):<br />
	<tt>partman-crypto: ioctl: LOOP_SET_STATUS: Invalid argument, requested
	cipher or key length (256 bits) not supported by kernel (Ongeldig argument,
    gevraagde coderingsmethode of sleutellengte (256 bits) niet ondersteund door
    kernel).</tt><br />
    Oorzaak van dit probleem is dat in een vroeg stadium van de installatie het
    cd-image in een lus opnieuw is aangekoppeld met behulp van de gewone
    loop-module, wat voorkomt dat de loop-aes-module later wordt geladen.<br />
    U kunt dit probleem omzeilen door het cd-image handmatig te ontkoppelen, de
    loop-module te ontladen, de loop-aes-module te laden en het cd-image
    opnieuw aan te koppelen.
	</dd>

	<dt>Onvolledige installatie van de Desktop-taak vanaf een volledige cd</dt>
	<dd>
	Het volledige cd-image is te klein om alle pakketten te kunnen bevatten die
    nodig zijn om de volledige Desktop-taak te installeren. Dit betekent dat
    wanneer u enkel de cd gebruikt als pakketbron, enkel een deel van de
    Desktop-taak geïnstalleerd zal worden.<br />
	U kunt dit oplossen door ofwel <em>tijdens de installatie</em> ervoor te
    kiezen om naast de cd een netwerkspiegelserver te gebruiken als bron voor
    te installeren pakketten (niet aanbevolen indien u niet over een
    behoorlijke internetverbinding beschikt), ofwel kunt u <em>nadat u opnieuw
    bent opgestart</em> in het geïnstalleerde systeem, <tt>apt-cdrom</tt>
    gebruiken om extra cd's te laden en vervolgens in <tt>aptitude</tt> de
    Desktop-taak opnieuw selecteren.
	</dd>

	<dt>Routers die bugs vertonen, kunnen netwerkproblemen veroorzaken</dt>
	<dd>
	Als u netwerkproblemen ondervindt tijdens de installatie, kan dit worden
    veroorzaakt door een router ergens tussen u en de Debian-spiegelserver die
    niet correct omgaat met <q>window scaling</q>.
	Zie <a href="https://bugs.debian.org/401435">#401435</a> en dit
	<a href="http://kerneltrap.org/node/6723">artikel van kerneltrap</a> voor
	details.<br />
	U kunt dit probleem omzeilen door <q>TCP window scaling</q> uit te zetten.
	Activeer een shell en geef het volgende commando:<br />
	<tt>echo 0 &gt; /proc/sys/net/ipv4/tcp_window_scaling</tt><br />
	Voor het geïnstalleerde systeem moet u <q>TCP window scaling</q>
    waarschijnlijk niet volledig uitschakelen. Met de volgende opdracht wordt
    een bereik voor lezen en schrijven ingesteld dat met bijna elke router zou
    moeten werken:<br />
	<tt>echo 4096 65536 65536 &gt;/proc/sys/net/ipv4/tcp_rmem</tt><br />
	<tt>echo 4096 65536 65536 &gt;/proc/sys/net/ipv4/tcp_wmem</tt>
	</dd>

	<dt>Algemene problemen na opnieuw opstarten vanwege UTF-8-standaard</dt>
	<dd>
	In het geïnstalleerde systeem is UTF-8 nu als standaard ingesteld. Echter,
    nog niet alle toepassingen ondersteunen UTF-8 goed, wat kan resulteren in
    kleine of grote problemen bij het gebruik ervan.<br />
	Controleer of dergelijke problemen al zijn gemeld en indien niet, dien dan
    een bugrapport in tegen het relevante pakket (niet tegen het
    installatieprogramma).
	</dd>

	<dt>Beperkte lokalisatie van geïnstalleerd systeem</dt>
	<dd>
	In de Sarge-versie van het installatieprogramma was het pakket
    localization-config verantwoordelijk voor een deel van de lokalisatie van
    het geïnstalleerde systeem. Dit pakket werd uitgevoerd als onderdeel van
    base-config, dat in de release is weggelaten. Het aanpassen van
    localization-config zodat het wordt uitgevoerd voordat het systeem opnieuw
    wordt opgestart, staat op onze TODO-lijst, maar ondertussen kan het zijn
    dat bepaalde lokalisatie niet automatisch uitgevoerd wordt bij een
    installatie in een andere taal dan het Engels.
	</dd>

	<dt>Installatie van Sarge niet ondersteund</dt>
	<dd>
	Als gevolg van enkele structurele wijzigingen in het installatieprogramma
    wordt de installatie van Sarge (oldstable) niet ondersteund.
	</dd>

	<dt>Grafische installatieprogramma</dt>
	<dd>
	Het grafische installatieprogramma heeft nog enkele bekende problemen (zie
    ook de Installatiehandleiding):
	<ul>
		<li>sommige niet-Amerikaanse toetsenbordindelingen worden niet volledig
            ondersteund (dode toetsen en het combineren van tekens werken
            niet)</li>
		<li>touchpads zouden moeten werken, maar de ondersteuning is misschien
            niet optimaal; als u problemen ondervindt, moet u in plaats daarvan
            een externe muis gebruiken</li>
		<li>beperkte ondersteuning voor het maken van versleutelde partities</li>
		<li>zou moeten werken op bijna alle PowerPC-systemen die een
            ATI grafische kaart hebben, maar het is onwaarschijnlijk dat het op
            andere PowerPC-systemen zal werken</li>
	</ul>
	</dd>

	<dt>Onterechte waarschuwing over ontbrekend wisselgeheugen bij de
        installatie van de <em>laptop</em>-taak</dt>
	<dd>
	Bij het installeren van de <em>laptop</em>-taak zal het pakket
    <tt>uswsusp</tt> een onterechte waarschuwing geven met de melding:
    <q>Geen swap-partitie gevonden; gebruikersruimteslaapstand zal niet
    werken</q>. Dit is een louter <strong>foutieve</strong> waarschuwing
    en de slaapstand zou correct moeten werken. Zie ook bug
	<a href="https://bugs.debian.org/427104">#427104</a>.
	</dd>

	<dt>Het is bekend dat netwerkstuurprogramma sky2 defect is</dt>
	<dd>
	Het is bekend dat het netwerkstuurprogramma sky2 defect is in de kernel
	(2.6.18.dfsg.1-11) welke gebruikt wordt in het installatieprogramma. Dit
    kan aanleiding geven tot kernel panic. Zie bijvoorbeeld
	<a href="https://bugs.debian.org/404107">#404107</a>.
	<a href="https://bugs.debian.org/411115">#411115</a> bevat een reeks
	patches die het probleem oplossen.
	</dd>

<!-- leaving this in for possible future use...
	<dt>i386: more than 32 mb of memory is needed to install</dt>
	<dd>
	The minimum amount of memory needed to successfully install on i386
	is 48 mb, instead of the previous 32 mb. We hope to reduce the
	requirements back to 32 mb later. Memory requirements may have
	also changed for other architectures.
	</dd>
-->

	<dt>i386/amd64: instellen van seriële console in GRUB</dt>
	<dd>
	Er zijn enkele problemen met de manier waarop het installatieprogramma
    GRUB probeert in te stellen voor het gebruik van een seriële console. De
    volledige details zijn te vinden in het bugrapport
	<a href="https://bugs.debian.org/416310">#416310</a>.
	In het kort kunnen deze problemen worden opgelost door ervoor te zorgen dat
    de pariteits- en bits-opties ook worden doorgegeven in de definitie
    <q><tt>console=</tt></q>.<br />
	Voor de meeste mensen zal dit betekenen dat in de plaats van het
    installatieprogramma te starten met de optie
    <q><tt>console=ttyS0,9600</tt></q>, ze de optie
    <q><tt>console=ttyS0,9600n8</tt></q> zullen moeten gebruiken.
	</dd>

	<dt>i386: het opstarten van het installatieprogramma kan op sommige
    oudere systemen mislukken</dt>
	<dd>
	Door een regressie in het opstartprogramma syslinux dat voor de meeste
    images van het installatieprogramma gebruikt wordt, is het mogelijk dat het
    opstarten van het installatieprogramma mislukt op bepaalde oudere systemen.
    Indien het opstarten stopt na het weergeven van
    <q><tt>Loading initrd.gz....</tt></q>, zou u het moeten proberen met een
    van de
	<a href="https://d-i.alioth.debian.org/pub/etch/syslinux/">alternatieve
	images</a> die een oudere versie van syslinux gebruiken. Zie ook het
    bugrapport
	<a href="https://bugs.debian.org/415992">#415992</a>.
	</dd>

	<dt>amd64: aanmaken van een ext2-bestandssysteem is niet
    mogelijk (opgelost in 4.0r1)</dt>
	<dd>
	Omdat de ext2-kernelmodule niet beschikbaar is, is het niet mogelijk om
    een ext2-bestandssysteem aan te maken. Dit zal opgelost worden met de
    volgende update van het installatieprogramma voor Etch.
	</dd>

	<dt>powerpc: verschillende problemen</dt>
	<dd>
	De versie van Debian voor PowerPC kampt in deze uitgave met verschillende problemen:
	<ul>
		<li>op OldWorld PowerMac is de installatie vanaf diskette defect omdat
            geen apparaatnode gecreëerd wordt voor de swim3-module en omdat
            miboot niet opgenomen is</li>
		<li>de module snd-powermac wordt niet langer standaard geladen omdat
            ze sommige systemen zal doen blokkeren; u zult ze handmatig moeten
            toevoegen aan <tt>/etc/modules</tt></li>
	</ul>
	</dd>

	<dt>sparc: installaties met cd kunnen mislukken op sparc32</dt>
	<dd>
	De kernelmodule met het stuurprogramma esp is defect, hetgeen betekent dat
    voor de meeste sparc32-systemen geen installatie vanaf cd mogelijk is. We
    suggereren om in plaats daarvan gebruik te maken van de
    <q>netboot</q>-installatiemethode.
	</dd>

	<dt>sparc: installatieprogramma blijkt vast te lopen tijdens het opstarten</dt>
	<dd>
	Er zijn twee situaties waarin het installatieprogramma blijkt vast te lopen
    tijdens het opstarten na het weergeven van <q><tt>Booting Linux...</tt></q>.
    De eerste situatie doet zich voor wanneer er twee grafische controllers
    aanwezig zijn (dit wordt ook beschreven in de
	<a href="$(HOME)/releases/etch/sparc/ch05s03#sparc-boot-problems">installatiehandleiding</a>).
	De tweede is toe te schrijven aan een bug in het ATI
    framebufferstuurprogramma van de kernel en treft enkel bepaalde ATI
    grafische kaarten.<br />
	In beide gevallen kan dit probleem omzeild worden door de parameter
	<tt>video=atyfb:off</tt> toe te voegen bij het opstarten van het
    installatieprogramma.
	</dd>

	<dt>s390: niet-ondersteunde functionaliteit</dt>
	<dd>
	<ul>
		<li>momenteel is geen ondersteuning beschikbaar voor de
        DASD DIAG discipline</li>
		<li>ondersteuning voor LCS netwerkkaarten is niet langer beschikbaar</li>
	</ul>
	</dd>
</dl>
