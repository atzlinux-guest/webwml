#use wml::debian::template title="Debian op de Desktop" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="385b72248b2434177dbd472f7c322a759a6a8dd0"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#philosophy">Onze filosofie</a></li>
<li><a href="#help">Hoe u kunt helpen</a></li>
<li><a href="#join">Met ons meedoen</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian Desktop is een groep vrijwilligers die het best mogelijke besturingssysteem wil maken voor het computerwerkstation thuis of op het werk. Ons motto is: <q>software die gewoon werkt.</q> Ons doel: Debian, GNU en Linux tot bij het brede publiek brengen.</p>
</aside>

<h2><a id="philosophy">Onze filosofie</a></h2>

<p>
We onderkennen dat er veel
<a href="https://wiki.debian.org/DesktopEnvironment">Desktop-omgevingen</a>
bestaan en dat we hun gebruik zullen ondersteunen - dit houdt ook in dat we
ervoor moeten zorgen dat ze goed werken op Debian. Ons doel is om de grafische
interfaces gebruiksvriendelijk te maken voor beginners, terwijl gevorderde
gebruikers en experts de mogelijkheid hebben dingen aan te passen als ze dat
willen.
</p>

<p>
We zullen proberen ervoor te zorgen dat de software is geconfigureerd voor het
meest voorkomende desktopgebruik. Het normale gebruikersaccount dat tijdens de
installatie wordt aangemaakt, moet bijvoorbeeld de bevoegdheid hebben om audio
en video af te spelen, af te drukken en het systeem te beheren via sudo. Ook
willen we de vragen van <a href="https://wiki.debian.org/debconf">debconf</a>
(het configuratiebeheersysteem van Debian) tot het absolute minimum beperken.
Het is niet nodig om met moeilijke technische details te komen aandraven
tijdens de installatie. In plaats daarvan zullen we proberen ervoor te zorgen
dat debconf-vragen zinvol zijn voor de gebruikers. Een beginneling begrijpt
misschien niet eens waar deze vragen over gaan. En een expert kan er perfect
mee leven om de dekstop-omgeving te configureren nadat de installatie voltooid
is.
</p>

<h2><a id="help">Hoe u kunt helpen</a></h2>

<p>
We zoeken gemotiveerde mensen die dingen realiseren. U hoeft geen
Debian-ontwikkelaar (Debian Developer - DD) te zijn om patches in te dienen of
pakketten te maken. Het kernteam van Debian Desktop zal ervoor zorgen dat uw
werk wordt geïntegreerd. Dus, hier zijn enkele dingen die u kunt doen om te
helpen:
</p>

<ul>
  <li>De standaard desktopomgeving (of een van de andere desktops) voor de komende release testen. Haal een van de <a href="$(DEVEL)/debian-installer/">images uit testing</a> op en stuur feedback naar de <a href="https://lists.debian.org/debian-desktop/">mailinglijst debian-desktop</a>.</li>
  <li>Meewerken met het <a href="https://wiki.debian.org/DebianInstaller/Team">Team van het Debian-installatiesysteem</a> en deze <a href="$(DEVEL)/debian-installer/">debian-installer</a> helpen te verbeteren – het GTK+ frontend heeft u nodig.</li>
  <li>U kunt het <a href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME team</a>, de <a href="https://qt-kde-team.pages.debian.net/"> Debian Qt/KDE pakketbeheerders en het Debian KDE Extras team</a>, of de <a href="https://salsa.debian.org/xfce-team/">Debian Xfce groep</a> helpen met het verpakken, het oplossen van bugs, met documentatie, met testen en nog andere zaken.</li>
  <li><a href="https://packages.debian.org/debconf">debconf</a> helpen verbeteren door de prioriteit van vragen te verlagen of onnodige vragen uit pakketten te verwijderen. Maak de noodzakelijke deconf-vragen makkelijker te begrijpen.</li>
  <li>Bent u bedreven als vormgever? Waarom zou u dan niet werken aan de <a href="https://wiki.debian.org/DebianDesktop/Artwork">grafische vormgeving van Debian Desktop</a>.</li>
</ul>

<h2><a id="join">Met ons meedoen</a></h2>

<aside class="light">
  <span class="fa fa-users fa-4x"></span>
</aside>

<ul>
  <li><strong>Wiki:</strong> bezoek onze Wiki over <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a> (sommige artikels zijn misschien verouderd).</li>
  <li><strong>Mailinglijst:</strong> overleg met ons op de mailinglijst <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.</li>
  <li><strong>IRC-kanaal:</strong> ga met ons in gesprek op IRC. Sluit aan bij het kanaal #debian-desktop op <a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org)</li>
</ul>

