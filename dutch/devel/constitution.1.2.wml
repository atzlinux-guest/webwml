#use wml::debian::template title="Historische statuten van Debian v 1.2" BARETITLE="true"
#use wml::debian::translation-check translation="ba01cfdc529712e3626bdf15fd37d39e94126794"

<h1>Historische versie van de statuten van het Debian-project (v1.2)</h1>

<p>Versie 1.2, bekrachtigd op 29 oktober 2003. Vervangt
<a href="constitution.1.1">versie 1.1</a>, bekrachtigd op 21 juni 2003,
welke zelf een vervanging is voor <a href="constitution.1.0">versie 1.0</a>,
bekrachtigd op 2 december 1998.
Vervangen door <a href="constitution.1.3">versie 1.3</a>, bekrachtigd op 24 september 2006.
Versie 1.3 werd op zijn beurt vervangen door
<a href="constitution.1.4">versie 1.4</a>, bekrachtigd op 7 oktober 2007.
Die werd vervangen door <a href="constitution.1.5">versie 1.5</a>,
bekrachtigd op 9 januari 2015, en opnieuw vervangen door
<a href="constitution.1.6">versie 1.6</a>, bekrachtigd op 13 december 2015.
Die werd vervangen door de <a href="constitution">huidige versie 1.7</a>,
bekrachtigd op 14 augustus 2016.
</p>


<h2>1. Inleiding</h2>

<p><cite>Het Debian-project is een vereniging van individuen die zich
samen tot doel gesteld hebben een vrij besturingssysteem te ontwikkelen.</cite></p>

<p>Dit document beschrijft de organisatiestructuur in functie van het
nemen van formele beslissingen in het project. Het is geen beschrijving
van de doelstellingen van het project of hoe het deze bereikt,
en het bevat geen beleidsrichtlijnen, behalve die welke rechtstreeks
verband houden met het beslissingsproces.</p>

<h2>2. Organen en individuen met beslissingsbevoegdheid</h2>

<p>Elk besluit binnen het project wordt door een of meer van de volgende instanties genomen:</p>

<ol>
  <li>De ontwikkelaars, via een algemene resolutie of een verkiezing;</li>

  <li>De projectleider;</li>

  <li>Het technisch comité en/of zijn voorzitter;</li>

  <li>De individuele ontwikkelaar die aan een specifieke taak werkt;</li>

  <li>Gemachtigden die door de projectleider aangesteld zijn voor specifieke taken;</li>

  <li>De secretaris van het project.</li>
</ol>

<p>Het grootste deel van de rest van dit document beschrijft de
bevoegdheden van deze instanties, hun samenstelling en aanstelling en
hun besluitvormingsprocedure. De bevoegdheden van een persoon of een
instantie kunnen onderhevig zijn aan het toezicht of de inperking door
anderen; in dat geval zal het item dat handelt over het/de
toezichthoudende orgaan of persoon, hiervan melding maken.  <cite>>In
bovenstaande lijst staat een persoon of orgaan doorgaans vermeld
voorafgaand aan de personen of organen van wie zij de besluiten kunnen
verwerpen of welke zij (mee) aanstellen - maar niet iedereen die eerder
vermeld staat, kan ingaan tegen iedereen die nadien vermeld wordt.</cite></p>

<h3>2.1. Algemene regels</h3>

<ol>
  <li>
    <p>Niets uit deze statuten legt aan iemand een verplichting op om
    werk te verrichten voor het project. Een persoon die een aan hem/haar
    gedelegeerde of toegewezen taak niet wenst te verrichten, moet die
    ook niet verrichten. Men mag echter deze regels en de besluiten die
    op een correcte wijze volgens deze regels genomen werden, niet actief
    tegenwerken.</p>
  </li>

  <li>
    <p>Een persoon kan verschillende functies bekleden, behalve dat
    de projectleider, de projectsecretaris en de voorzitter van het
    technisch comité verschillende personen moeten zijn en dat de
    leider zichzelf niet als zijn eigen gemachtigde kan aanstellen.</p>
  </li>

  <li>
    <p>Een persoon kan op elk moment het project verlaten of ontslag
    nemen uit een specifieke functie welke deze bekleedt, door dit in
    het openbaar mee te delen.</p>
  </li>
</ol>

<h2>3. Individuele ontwikkelaars</h2>

<h3>3.1. Bevoegdheden</h3>

<p>Een individuele ontwikkelaar kan</p>

<ol>
  <li>elke technische of niet-technische beslissing nemen in verband
  met de eigen werkzaamheden;</li>

  <li>een ontwerp van algemene resolutie voorstellen of steunen;</li>

  <li>zichzelf kandidaat stellen voor de verkiezing van projectleider;</li>

  <li>een stem uitbrengen over een algemene resolutie en bij de verkiezing
  van de projectleider.</li>
</ol>

<h3>3.2. Samenstelling en aanstelling</h3>

<ol>
  <li>
    <p>Ontwikkelaars zijn vrijwilligers die ermee instemmen de
    doelstellingen van het project te bevorderen voor zover zij
    eraan deelnemen, en die voor het project pakketten onderhouden
    of ander werk verrichten dat door de gemachtigde(n) van de
    projectleider waardevol geacht wordt.</p>
  </li>

  <li>
    <p>De gemachtigde(n) van de projectleider kan/kunnen nieuwe
    ontwikkelaars weren of bestaande ontwikkelaars uitsluiten.  <cite>
    Indien de ontwikkelaars van mening zijn dat de gemachtigden
    hun gezag misbruiken, kunnen zij de beslissing natuurlijk via
    een algemene resolutie vernietigen - zie
    &sect;4.1(3), &sect;4.2.</cite></p>
  </li>
</ol>

<h3>3.3. Procedure</h3>

<p>Ontwikkelaars kunnen deze beslissingen naar eigen goeddunken nemen.</p>

<h2>4. De ontwikkelaars via algemene resolutie of verkiezingen</h2>

<h3>4.1. Bevoegdheden</h3>

<p>Samen kunnen de ontwikkelaars:</p>

<ol>
  <li>
    <p>De projectleider aanstellen of afzetten.</p>
  </li>

  <li>
    <p>Deze statuten wijzigen op voorwaarde dat een meerderheid van 3:1
       ermee instemt.</p>
  </li>

  <li>
    <p>Elke beslissing van de projectleider of een gemachtigde opheffen.</p>
  </li>

  <li>
    <p>Elke beslissing van het technisch comité opheffen op voorwaarde dat een
       meerderheid van 2:1 ermee instemt.</p>
  </li>

  <li>
    <p>Niet-technische beleidsdocumenten en verklaringen uitgeven, vervangen en
       intrekken.</p>

    <p>Daaronder vallen documenten over de doelstellingen van het
       project, de relaties van het project met andere entiteiten van de
       vrije-softwaregemeenschap, en niet-technische beleidsrichtlijnen,
       zoals de licentievoorwaarden voor gratis software waaraan
       Debian-software moet voldoen.</p>

    <p>Daaronder vallen ook standpuntbepalingen inzake actuele
       onderwerpen.</p>

    <ol style="list-style: decimal;">
      <li>Een basisdocument is een document of verklaring welke van
       cruciaal belang wordt geacht voor de missie en doelstellingen van
       het project.</li>
      <li>Als basisdocumenten worden beschouwd, de documenten met als
       titel <q>Het Debian sociaal contract</q> en <q>De Debian
       richtlijnen inzake vrije software</q>.</li>
      <li>De vervanging van een basisdocument vereist een meerderheid
       van 3:1. Het uitvaardigen van nieuwe basisdocumenten en het
       intrekken van bestaande gebeurt door herziening van de lijst
       van basisdocumenten in deze statuten.</li>
    </ol>

  </li>

  <li>
    <p>Samen met de projectleider en SPI beslissingen nemen over eigendom die
    in beheer gehouden wordt voor doeleinden die met Debian verband houden.
    (Zie &sect;9.1.)</p>
  </li>
</ol>

<h3>4.2. Procedure</h3>

<ol>
  <li>
    <p>De ontwikkelaars volgen de onderstaande standaard
    resolutieprocedure. Een resolutie of amendement wordt ingediend
    wanneer deze door een ontwikkelaar voorgesteld wordt en minstens
    door K andere ontwikkelaars gesteund wordt, of wanneer deze door
    de projectleider of het technisch comité voorgesteld wordt.</p>
  </li>

  <li>
    <p>Een beslissing van de projectleider of diens gemachtigde
     verdagen:</p>

    <ol>
      <li>Indien de projectleider of diens gemachtigde of het technisch
      comité een besluit genomen heeft, kunnen ontwikkelaars dat
      annuleren door hierover een resolutie aan te nemen;
      zie &sect;4.1(3).</li>

      <li>Indien een dergelijke resolutie minstens door 2K ontwikkelaars
      gesteund wordt, of indien ze door het technisch comité voorgesteld
      wordt, schort deze resolutie de beslissing onmiddellijk op
      (op voorwaarde dat de resolutie zelf dit zo bepaalt).</li>

      <li>Indien de originele beslissing een wijziging van een discussie-
      of stemmingsperiode betreft of indien de resolutie het herroepen
      van een beslissing van het technisch comité beoogt, dan moeten
      slechts K ontwikkelaars deze steunen om de beslissing onmiddellijk
      op te schorten.</li>

      <li>Indien de beslissing opgeschort werd, wordt
      onmiddellijk een stemming gehouden om te bepalen of de beslissing
      van kracht blijft tot er ten gronde over de beslissing gestemd werd
      ofwel of de toepassing van de originele beslissing tot dan verdaagd
      wordt. Voor deze onmiddellijke procedurele stemming is geen
      quorum vereist.</li>

      <li>Indien de projectleider (of de gemachtigde) de originele
      beslissing intrekt, wordt de stemming irrelevant en niet langer
      voortgezet.</li>
    </ol>
  </li>

  <li>
    <p>
       Stemmen worden door de projectsecretaris verzameld. Stemmen,
       tellingen en resultaten worden tijdens de stemperiode niet kenbaar
       gemaakt. Na de stemming maakt de projectsecretaris een lijst op van
       alle uitgebrachte stemmen. De stemperiode duurt 2 weken, maar de
       projectleider kan deze tot maximaal 1 week laten variëren.
    </p>
  </li>

  <li>
    <p>De minimale discussieperiode is 2 weken, maar de
       projectleider kan deze tot maximaal 1 week laten variëren. De
       projectleider heeft een beslissende stem. Het quorum bedraagt 3Q.</p>
  </li>

  <li>
    <p>Voorstellen, steunbetuigingen, amendementen, oproepen tot het
    uitbrengen van een stem en andere formele acties gebeuren via een
    aankondiging op een publiek leesbare elektronische mailinglijst die
    door de gemachtigde(n) van de projectleider aangewezen wordt. Elke
    ontwikkelaar kan er op posten.</p>
  </li>

  <li>
    <p>Stemmen worden per e-mail uitgebracht op een manier die passend
    is voor de secretaris. Voor elke stemming bepaalt de secretaris of
    stemmers hun uitgebrachte stem kunnen wijzigen.</p>
  </li>

  <li>
    <p>Q is de helft van de vierkantswortel van het huidige aantal
    ontwikkelaars. K is Q of 5, welk van beide het kleinst is. Q en K
    moeten geen gehele getallen zijn en worden niet afgerond.</p>
  </li>
</ol>

<h2>5. Projectleider</h2>

<h3>5.1. Bevoegdheden</h3>

<p>De <a href="leader">projectleider</a> kan:</p>

<ol>
  <li>
    <p>Gemachtigden aanstellen of besluitvormingen delegeren aan
    het technisch comité.</p>

    <p>De leider kan een domein van permanente verantwoordelijkheid of
    een specifieke beslissing definiëren en deze delegeren aan een andere
    ontwikkelaar of aan het technisch comité.</p>

    <p>Eens een specifieke beslissing gedelegeerd werd en een besluit
    genomen werd, kan de projectleider deze delegatie niet meer
    terugtrekken. Deze kan wel een lopende delegering van een specifiek
    verantwoordelijkheidsgebied intrekken.</p>
  </li>

  <li>
    <p>Andere ontwikkelaars autoriteit verlenen.</p>

    <p>De projectleider kan zijn steun uitspreken voor standpunten of
    voor andere projectleden, op vraag of anderszins; deze uitspraken
    worden enkel van kracht indien de projectleider gemachtigd zou zijn
    om de beslissing in kwestie te nemen.</p>
  </li>

  <li>
    <p>Elke beslissing nemen die dringende actie vereist.</p>

    <p>Dit is niet van toepassing op beslissingen die slechts stilaan
    dringend geworden zijn wegens een gebrek aan relevante actie, tenzij
    er een vaste deadline is.</p>
  </li>

  <li>
    <p>Elke beslissing nemen waarvoor niemand anders verantwoordelijk
    is.</p>
  </li>

  <li>
    <p>Ontwerpen van algemene resolutie en amendementen voorstellen.</p>
  </li>

  <li>
    <p>Samen met het technisch comité nieuwe leden van dat comité
    aanstellen. (Zie &sect;6.2.)</p>
  </li>

  <li>
    <p>Een beslissende stem uitbrengen wanneer ontwikkelaars stemmen.</p>

    <p>De projectleider heeft ook een normale stem in dergelijke
    stemmingen.</p>
  </li>

  <li>
    <p>De discussieperiode in verband met stemmingen van ontwikkelaars
    aanpassen (zoals hierboven).</p>
  </li>

  <li>
    <p>Discussies onder ontwikkelaars leiden.</p>

    <p>De projectleider behoort te proberen op een helpende wijze te
    participeren in discussies tussen ontwikkelaars in een poging de
    discussie toe te spitsen op de belangrijkste kwesties. De
    projectleider behoort geen gebruik te maken van zijn/haar
    leiderschapspositie om zijn/haar eigen persoonlijke meningen naar voor te
    schuiven.</p>
  </li>

  <li>
    <p>Samen met SPI beslissingen nemen die van invloed
     zijn op eigendommen die in beheer worden gehouden voor met Debian
     verband houdende doeleinden (zie &sect;9.1.)</p>
  </li>
</ol>

<h3>5.2. Aanstelling</h3>

<ol>
  <li>De projectleider wordt door de ontwikkelaars verkozen.</li>

  <li>De verkiezing begint negen weken voordat de functie van leider vacant
  wordt, of onmiddellijk (indien het reeds te laat is).</li>

  <li>Tijdens de volgende drie weken kan elke ontwikkelaar zichzelf nomineren
  als kandidaat projectleider.</li>

  <li>Tijdens de drie daaropvolgende weken kunnen geen kandidaten meer
   genomineerd worden; kandidaten behoren deze periode te gebruiken
   voor het voeren van campagne (om hun identiteit en positie kenbaar
   te maken). Indien er op het einde van de nominatieperiode geen kandidaten
   zijn, wordt de nominatieperiode verlengd met drie weken extra, en dit zo
   nodig meermaals.</li>

  <li>De volgende drie weken is de stemperiode waarin ontwikkelaars hun
   stem uitbrengen. Bij de verkiezing van de projectleider wordt de stemming
   geheim gehouden, ook na het einde van de stemming.</li>

  <li>Op het stembiljet bestaan de keuzemogelijkheden uit die kandidaten
   welke zichzelf genomineerd hebben en zich nog niet hebben
   teruggetrokken, plus "None Of The Above" (geen van bovenstaande).
   Indien "None Of The Above" de verkiezing wint, wordt de
   verkiezingsprocedure herhaald, zo nodig meermaals.</li>

  <li>De beslissing wordt genomen volgens de in &sect;A.6 van de
      standaard resolutieprocedure gespecificeerde methode. Het quorum
      is hetzelfde als bij een algemene resolutie (&sect;4.2) en de
      standaardoptie is <q>None Of The Above</q>.
  </li>

  <li>De ambtsperiode van de projectleider bedraagt een jaar vanaf diens
   verkiezing.</li>
</ol>

<h3>5.3. Procedure</h3>

<p>De projectleider behoort te proberen beslissingen te nemen die
   verenigbaar zijn met de consensus tussen de verschillende meningen van
   de ontwikkelaars.</p>

<p>Waar praktisch mogelijk behoort de projectleider op informele wijze
   te informeren naar de opvattingen van de ontwikkelaars.</p>

<p>De projectleider behoort te vermijden het eigen standpunt te zeer te
   beklemtonen bij het nemen van beslissingen in hoofde van zijn/haar functie
   als projectleider.</p>

<h2>6. Technisch comité</h2>

<h3>6.1. Bevoegdheden</h3>

<p>Het <a href="tech-ctte">technisch comité</a> kan:</p>

<ol>
  <li>
    <p>Beslissingen nemen in elke zaak met betrekking tot het technisch
    beleid.</p>

    <p>Dit betreft onder meer de inhoud van de handleidingen over het
    technische beleid, referentiemateriaal voor ontwikkelaars,
    voorbeeldpakketten en het gedrag van niet-experimentele hulpmiddelen
    voor het bouwen van pakketten. (In elk van deze zaken neemt de
    gebruikelijke onderhouder van de betreffende software of documentatie
    initieel de beslissingen; zie echter 6.3(5).)</p>
  </li>

  <li>
    <p>Beslissen in technische materies waarbij de bevoegdheden van
    ontwikkelaars elkaar overlappen.</p>

    <p>Het technisch comité kan een zaak beslechten in gevallen waarin
    ontwikkelaars compatibele technische beleidslijnen of standpunten
    moeten implementeren (bijvoorbeeld indien zij het oneens zijn over de
    prioriteit van conflicterende pakketten, of over het bezit van een
    commandonaam, of over welk pakket verantwoordelijk is voor een bug
    waarover beide akkoord gaan dat het een bug is, of over wie de
    onderhouder van een pakket behoort te zijn).</p>
  </li>

  <li>
    <p>Een beslissing nemen wanneer daarom gevraagd wordt.</p>

    <p>Elke persoon of instantie kan een eigen beslissing delegeren naar
    het technisch comité of er advies aan vragen.</p>
  </li>

  <li>
    <p>Een beslissing van een ontwikkelaar afwijzen (vereist een 3:1
    meerderheid).</p>

    <p>Het technisch comité kan een ontwikkelaar vragen een specifieke
    technische werkwijze te gebruiken, zelfs als de ontwikkelaar dit niet
    wenst; dit vereist een 3:1 meerderheid. Bijvoorbeeld kan het comité
    bepalen dat een klacht van een indiener van een bugrapport
    gerechtvaardigd is en dat de door de indiener voorgestelde oplossing
    behoort te worden geïmplementeerd.</p>
  </li>

  <li>
    <p>Advies geven.</p>

    <p>Het technisch comité kan formele uitspraken doen over zijn visie
    op gelijk welke zaak. <cite>Individuele leden kunnen uiteraard
    informele uitspraken doen over hun visie en over de vermoedelijke
    visie van het comité.</cite></p>
  </li>

  <li>
    <p>Samen met de projectleider nieuwe leden aanstellen bij het eigen
    comité of bestaande leden eruit verwijderen. (Zie &sect;6.2.)</p>
  </li>

  <li>
    <p>De voorzitter van het technisch comité aanstellen.</p>

    <p>
       De voorzitter wordt door het comité uit zijn leden verkozen. Alle
       leden van het comité zijn automatisch genomineerd. Het comité
       begint te stemmen, een week voor de functie vacant wordt (of
       onmiddellijk als het al te laat is). De leden kunnen bij openbare
       acclamatie stemmen voor gelijk welk collega-lid, ook voor
       zichzelf; er is geen standaardoptie. De stemming eindigt wanneer
       alle leden gestemd hebben of wanneer de stemperiode eindigt. Het
       resultaat wordt bepaald via de methode die gespecificeerd wordt in
       sectie A.6 van de standaard resolutieprocedure.
   </p>
  </li>

  <li>
    <p>De voorzitter kan samen met de secretaris de projectleider
    vervangen</p>

    <p>Zoals bepaald in &sect;7.1(2) kunnen de voorzitter van het
    technisch comité en de projectsecretaris samen de projectleider
    vervangen als er geen projectleider is.</p>
  </li>
</ol>

<h3>6.2. Samenstelling</h3>

<ol>
  <li>
    <p>Het technisch comité bestaat uit maximaal 8 ontwikkelaars en
    behoort gewoonlijk minstens 4 leden te hebben.</p>
  </li>

  <li>
    <p>Als het technisch comité minder dan 8 leden telt, kan het comité
    nieuwe leden aanbevelen bij de projectleider, die (individueel) kan
    beslissen om deze al dan niet aan te stellen.</p>
  </li>

  <li>
    <p>Wanneer het technisch comité 5 of minder leden telt, kan het
    nieuwe leden aanstellen tot het ledenaantal 6 bedraagt.</p>
  </li>

  <li>
    <p>Wanneer er voor minstens een week 5 of minder leden waren, kan de
    projectleider nieuwe leden aanstellen met een interval van minstens een
    week per aanstelling, tot het aantal leden 6 bedraagt.</p>
  </li>

  <li>
    <p>Als het technisch comité en de projectleider ermee akkoord gaan,
    kunnen ze een bestaand lid van het technisch comité verwijderen of
    vervangen.</p>
  </li>
</ol>

<h3>6.3. Procedure</h3>

<ol>
  <li>
    <p>Het technisch comité gebruikt de standaard resolutieprocedure.</p>

    <p>Elk lid van het technisch comité kan een ontwerpresolutie of een
    amendement voorstellen. Er is geen minimale discussieperiode. De
    stemperiode duurt hoogstens een week of tot er geen twijfel meer
    bestaat over het resultaat. Leden kunnen hun stem wijzigen. Het
    quorum bedraagt twee.</p>
  </li>

  <li>
    <p>Details in verband met de stemming</p>

    <p>De voorzitter heeft een beslissende stem. Wanneer het technisch
    comité stemt over het al dan niet opheffen van een beslissing van een
    ontwikkelaar die toevallig ook lid is van het comité, mag dat lid
    niet stemmen (tenzij het de voorzitter betreft en in dat geval mag
    deze enkel gebruik maken van zijn/haar beslissende stem).</p>
  </li>

  <li>
    <p>Openbare discussie en besluitvorming.</p>

    <p>Discussies, ontwerpresoluties en amendementen, en de door leden
    van het comité uitgebrachte stemmen, worden openbaar gemaakt op de
    publieke discussielijst van het technisch comité. Het comité heeft
    geen aparte secretaris.</p>
  </li>

  <li>
    <p>Vertrouwelijk karakter van aanstellingen.</p>

    <p>Het technisch comité kan bij het voeren van een discussie over
    aanstellingen bij het comité vertrouwelijke besprekingen hebben via
    private e-mail of een private mailinglijst of via andere middelen.
    Stemmingen over aanstellingen moeten evenwel openbaar zijn.</p>
  </li>

  <li>
    <p>Geen gedetailleerde ontwerpwerkzaamheden.</p>

    <p>Het technisch comité houdt zich niet bezig met het ontwerpen van
    nieuwe voorstellen en beleidsmaatregelen. Zulke ontwerpwerkzaamheden
    behoren persoonlijk of samen te worden uitgevoerd door particulieren
    en te worden besproken in gewone forums voor ontwerp en uitvoering
    van technisch beleid.</p>

    <p>Het technisch comité beperkt zich tot het kiezen uit of het
    aannemen van compromissen tussen oplossingen die elders voorgesteld
    en redelijkerwijs grondig besproken werden.</p>

    <p><cite>Individuele leden van het technisch comité kunnen uiteraard
    in eigen naam deelnemen aan elk aspect van ontwerp- en beleidswerk.</cite></p>
  </li>

  <li>
    <p>Het technisch comité neemt enkel beslissingen als laatste
    redmiddel.</p>

    <p>Het technisch comité neemt geen technische beslissing totdat
    inspanningen om via consensus tot een oplossing te komen geleverd
    werden en mislukt zijn, tenzij het gevraagd werd om een beslissing
    te nemen door de persoon of de instantie die daarvoor normaal
    verantwoordelijk is.</p>
  </li>
</ol>

<h2>7. De projectsecretaris</h2>

<h3>7.1. Bevoegdheden</h3>

<p>De <a href="secretary">secretaris</a>:</p>

<ol>
  <li>
    <p>Houdt stemmingen onder de ontwikkelaars en bepaalt het aantal en
    de identiteit van ontwikkelaars, telkens dit door de statuten vereist
    wordt.</p>
  </li>

  <li>
    <p>Kan samen met de voorzitter van het technisch comité de
    projectleider vervangen.</p>

    <p>Indien er geen projectleider is, kunnen de voorzitter van het
    technisch comité en de secretaris in gemeenschappelijk akkoord
    beslissingen nemen indien ze het noodzakelijk achten om dit te doen.</p>
  </li>

  <li>
    <p>Beslecht alle geschillen over de interpretatie van de statuten.</p>
  </li>

  <li>
    <p>Kan zijn/haar bevoegdheden gedeeltelijk of volledig delegeren naar
    iemand anders en een dergelijke delegatie op elk ogenblik intrekken.</p>
  </li>
</ol>

<h3>7.2. Aanstelling</h3>

<p>De projectsecretaris wordt aangesteld door de projectleider en de
huidige projectsecretaris.</p>

<p>Indien de projectleider en de huidige projectsecretaris het niet eens
kunnen worden over een nieuwe aanstelling, moeten ze het bestuur van SPI
(zie &sect;9.1.) vragen een secretaris aan te stellen.</p>

<p>Indien er geen projectsecretaris is of de projectsecretaris momenteel
onbeschikbaar is en de bevoegdheid in verband met het nemen van een
bepaalde beslissing niet gedelegeerd heeft, dan kan die beslissing
genomen of gedelegeerd worden door de voorzitter van het technisch
comité als waarnemend secretaris.</p>

<p>De ambtstermijn van de projectsecretaris bedraagt 1 jaar, waarna deze
of een andere secretaris (her)aangesteld moet worden.</p>

<h3>7.3. Procedure</h3>

<p>De projectsecretaris behoort eerlijke en redelijke beslissingen te
nemen, welke bij voorkeur verenigbaar zijn met de consensus onder de
ontwikkelaars.</p>

<p>Wanneer zij samen optreden als plaatsvervangers van een afwezige
projectleider, behoren de voorzitter van het technisch comité en de
projectsecretaris enkel beslissingen te nemen wanneer dit absoluut
noodzakelijk is en enkel wanneer deze verenigbaar zijn met de consensus
onder de ontwikkelaars.</p>

<h2>8. De gemachtigden van de projectleider</h2>

<h3>8.1. Bevoegdheden</h3>

<p>De gemachtigden van de projectleider:</p>

<ol>
  <li>hebben de bevoegdheden die aan hen gedelegeerd werden door de
  projectleider;</li>

  <li>mogen bepaalde beslissingen nemen die de projectleider niet
  rechtstreeks kan nemen, waaronder het aanvaarden of uitsluiten van
  ontwikkelaars of het aanstellen tot ontwikkelaar van personen die geen
  pakketten onderhouden. <cite>Dit is om een concentratie van macht in de
  handen van de projectleider te vermijden, in het bijzonder met
  betrekking tot het lidmaatschap van ontwikkelaars.</cite></li>
</ol>

<h3>8.2. Aanstelling</h3>

<p>De gemachtigden worden door de projectleider aangesteld en kunnen door
 de projectleider naar goeddunken vervangen worden. De projectleider mag
 de functie van gemachtigde niet voorwaardelijk maken aan specifieke
 beslissingen van de gemachtigde, en mag evenmin een door een gemachtigde
 genomen beslissing ongedaan maken.</p>

<h3>8.3. Procedure</h3>

<p>Gemachtigden mogen naar eigen goeddunken beslissingen nemen, maar
 behoren ernaar te streven technisch goede beslissingen toe te passen en/of
 te handelen volgens consensusopvattingen.</p>

<h2>9. Software in the Public Interest (Software in het algemeen belang)</h2>

<p><a href="https://www.spi-inc.org/">SPI</a> en Debian zijn aparte
organisaties die bepaalde gemeenschappelijke doelstellingen hebben. Debian is
dankbaar voor het ondersteunend juridisch kader dat door SPI geboden wordt.
<cite>De ontwikkelaars van Debian zijn momenteel lid van SPI op grond van hun
status als ontwikkelaar.</cite></p>

<h3>9.1. Bevoegdheid</h3>

<ol>
  <li>SPI heeft geen bevoegdheid met betrekking tot de technische of
  niet-technische beslissingen van Debian, behalve dat geen enkele beslissing
  van Debian met betrekking tot eigendom die in het bezit is van SPI zal
  vereisen dat SPI buiten zijn wettelijke bevoegdheid handelt, en dat de
  statuten van Debian SPI af en toe kan gebruiken als een beslissingsorgaan in
  laatste instantie.</li>

  <li>Debian eist geen andere bevoegdheid over SPI op dan die over het gebruik
  van bepaalde eigendommen van SPI, zoals hieronder beschreven, hoewel
  ontwikkelaars van Debian door de reglementen van SPI bevoegdheden binnen SPI
  kunnen krijgen.</li>

  <li>Ontwikkelaars van Debian zijn geen agenten of werknemers van SPI, of van
  elkaar of van personen met autoriteit in het Debian-project. Een persoon die
  optreedt als ontwikkelaar doet dit als individu, in eigen naam.</li>
</ol>

<h3>9.2. Beheer van eigendom voor doeleinden die verband houden met Debian</h3>

<p>Aangezien Debian niet de bevoegdheid heeft om geld of bezittingen te hebben,
  moeten alle donaties voor het Debian-project worden gedaan aan SPI, dat
  dergelijke zaken beheert.</p>

<p>SPI heeft de volgende toezeggingen gedaan:</p>

<ol>
  <li>SPI zal geld, handelsmerken en andere materiële en immateriële
  eigendommen bezitten en andere zaken beheren voor doeleinden die verband
  houden met Debian.</li>

  <li>Dergelijke eigendommen zullen afzonderlijk worden geboekt en voor deze
  doeleinden in bewaring worden gehouden, waarover Debian en SPI overeenkomstig
  deze sectie zullen beslissen.</li>

  <li>SPI zal geen eigendom van Debian verkopen of gebruiken zonder toestemming
  van Debian, die kan worden verleend door de projectleider of door een
  algemene resolutie van de ontwikkelaars.</li>

  <li>SPI zal er rekening mee houden om eigendommen die in beheer gehouden
  worden voor Debian, te gebruiken of te verkopen wanneer de projectleider
  daarom vraagt.</li>

  <li>SPI zal eigendommen die in beheer zijn voor Debian gebruiken of van de
  hand doen wanneer daarom wordt gevraagd door een algemene resolutie van de
  ontwikkelaars, op voorwaarde dat dit verenigbaar is met de wettelijke
  bevoegdheid van SPI.</li>

  <li>SPI zal de ontwikkelaars via een e-mail gericht aan een mailinglijst van
  het Debian-project, op de hoogte brengen wanneer het eigendommen die voor
  Debian in beheer zijn, gebruikt of van de hand doet.</li>
</ol>

<h2>A. Standaard resolutieprocedure</h2>

<p>Deze regels zijn van toepassing op de algemene besluitvorming door comité's
en ontwikkelaarsraadplegingen, zoals hierboven vermeld.</p>

<h3>A.1. Voorstel</h3>

<p>De formele procedure start wanneer een ontwerpresolutie volgens de
   vereisten voorgesteld en gesteund wordt.</p>

<h3>A.1. Discussie en amendering</h3>

<ol>
  <li>Na het voorstel kan de resolutie bediscussieerd worden.
  Amenderingen kunnen formeel gemaakt worden doordat ze voorgesteld en
  gesteund worden volgens de vereisten voor een nieuwe resolutie, of
  onmiddellijk door de indiener van de originele resolutie.</li>

  <li>De indiener van een resolutie kan een formeel amendement aanvaarden
  en in dat geval wordt de formele ontwerpresolutie daaraan onmiddellijk
  aangepast.</li>

  <li>Indien een formeel amendement niet aanvaard wordt, of indien een
  van de ondersteuners van de resolutie niet akkoord gaat met de
  aanvaarding van een formeel amendement door de indiener, dan blijft het
  amendement als amendement bestaan en zal daarover gestemd worden.</li>

  <li>Indien een door de originele indiener van de resolutie aanvaard
  amendement niet naar de zin is van anderen, kunnen deze een nieuw
  amendement indienen om de eerdere aanpassing ongedaan te maken (ook dan
  moet voldaan worden aan de vereisten inzake indienen en ondersteunen).</li>

  <li>De indiener van een resolutie kan wijzigingen aan de formulering
  van een amendement voorstellen; deze worden van kracht indien de
  indiener van het amendement ermee akkoord gaat en geen van de
  ondersteuners ervan bezwaar aantekent. In dat geval zal gestemd worden
  over het gewijzigd amendement in plaats van over het origineel.</li>

  <li>De indiener van een resolutie kan wijzigingen aanbrengen om kleine
  fouten te corrigeren (bijvoorbeeld typefouten of inconsistenties), of
  aanpassingen zonder dat de betekenis verandert, op voorwaarde dat
  niemand binnen de 24 uur bezwaar aantekent. In dit geval begint de
  minimale discussieperiode niet opnieuw.</li>
</ol>

<h3>A.2. Oproep tot de stemming</h3>

<ol>
  <li>De indiener of een ondersteuner van een motie of een amendement
  kunnen oproepen tot een stemming, op voorwaarde dat de minimale
  discussieperiode (indien van toepassing) is verstreken.</li>

  <li>De indiener en elke ondersteuner van een resolutie kunnen oproepen
  tot de stemming over deze resolutie en over alle ermee verband
  houdende amendementen.</li>

  <li>Degene die om stemming vraagt, geeft aan wat volgens hem/haar de
  verwoording van de resolutie en de eventuele relevante amendementen is,
  en bijgevolg in welke vorm de stemming behoort plaats te vinden.
  Nochtans berust de eindbeslissing over de vorm van de stemming(en) bij
  de secretaris - zie 7.1(1), 7.1(3) en A.3(4).</li>

  <li>De minimale discussieperiode wordt berekend vanaf het moment
  waarop het laatste formele amendement werd aanvaard, of vanaf het moment
  waarop de hele resolutie werd voorgesteld
  indien er geen amendementen werden voorgesteld en geaccepteerd.</li>
</ol>

<h3>A.3. Stemprocedure</h3>

<ol>
  <li>
       Over elke resolutie en de bijbehorende amendementen wordt gestemd
       in één enkele stemming die een optie bevat voor de oorspronkelijke
       resolutie, voor elk amendement en voor de standaardoptie (indien
       van toepassing).
   </li>

  <li>
       De standaardoptie mag geen bijzondere meerderheidseisen hebben.
       Opties waarvoor geen expliciete bijzondere meerderheidsvereiste
       geldt, hebben een meerderheidsvereiste van 1:1.
  </li>

  <li>
       De stemmen worden geteld volgens de regels in A.6. De
       standaardoptie is <q>Further Discussion</q> (de discussie
       voortzetten), tenzij anders bepaald.
  </li>

  <li>In twijfelgevallen beslecht de projectsecretaris procedurele
  zaken.</li>
</ol>

<h3>A.4. Intrekken van resoluties en niet-aanvaarde amendementen</h3>

<p>De indiener van een resolutie of een niet-aanvaard amendement kan dit
 intrekken. In dat geval kunnen nieuwe indieners zich melden om het
 in leven te houden en in dat geval wordt de eerste persoon die dit doet
 de nieuwe indiener en eventuele anderen worden ondersteuners indien ze
 niet reeds ondersteuner waren.</p>

<p>Een ondersteuner van een resolutie of amendement kan zich terugtrekken
 (tenzij het geaccepteerd werd).</p>

<p>Indien de terugtrekking van de indiener en/of van ondersteuners tot
 gevolg heeft dat een resolutie geen indiener of onvoldoende
 ondersteuners heeft, zal er niet over gestemd worden, tenzij dit
 rechtgezet wordt voordat de resolutie verloopt.</p>

<h3>A.5. Verval</h3>

<p>
   Indien een voorgestelde resolutie gedurende 4 weken niet
 bediscussieerd of geamendeerd werd, er niet over gestemd werd of deze
 niet op een andere wijze afgehandeld werd, kan de secretaris een
 verklaring uitgeven dat de zaak ingetrokken wordt. Indien niemand van de
 ondersteuners van een voorstel binnen de week bezwaar aantekent, is de
 zaak ingetrokken.
</p>

<p>
   De secretaris kan ook suggesties geven over hoe het verder moet, als
   dat passend is.
</p>

<h3>A.6. Tellen van de stemmen</h3>

<ol>
   <li>  Elke stemmer rangschikt de opties die ter stemming voorliggen.
   Niet alle opties dienen gerangschikt te worden. Gerangschikte opties
   worden aanzien als verkozen boven alle niet-gerangschikte opties.
   Stemmers kunnen opties gelijk rangschikken. Niet-gerangschikte opties
   worden onderling als gelijk gerangschikt beschouwd. Details over hoe
   stembiljetten kunnen worden ingevuld, worden opgenomen in de oproep
   tot de stemming.
   </li>
   <li> Indien bij de stemming een vereist quorum R geldt, wordt elke
   optie, op de standaardoptie na, die niet minstens R stemmen behaalt,
   buiten beschouwing gelaten.
   </li>
   <li> Elke (niet-standaard) optie die de standaardoptie niet verslaat
   met de vereiste meerderheidsverhouding, wordt buiten beschouwing
   gelaten.
        <ol>
             <li>
                  Gegeven twee opties A en B, dan is V(A,B) het aantal
                  stemmers dat optie A verkiest boven optie B.
             </li>
             <li>
                  Een optie A verslaat de standaardoptie D met een
                  meerderheidsverhouding N, indien V(A,D) strikt groter
                  is dan N * V(D,A).
             </li>
             <li>
                  Indien voor A een bijzondere meerderheid van S:1
                  vereist is, is zijn meerderheidsverhouding S; anders is
                  zijn meerderheidsverhouding 1.
             </li>
        </ol>
   </li>
   <li> Uit de lijst van opties die niet buiten beschouwing gelaten
        worden, genereren we een lijst met paarsgewijze nederlagen.
        <ol>
             <li>
                  Een optie A verslaat een optie B indien V(A,B) strikt
                  groter is dan V(B,A).
             </li>
        </ol>
   </li>
   <li> Uit de lijst van [niet buiten beschouwing gelaten] paarsgewijze
        nederlagen genereren we een verzameling transitieve nederlagen.
        <ol>
             <li>
                  Een optie A verslaat transitief een optie C indien A C
                  verslaat of indien er een bepaalde andere optie B is
                  waarbij A B verslaat EN B C transitief verslaat.
             </li>
        </ol>
   </li>
   <li> We stellen de Schwartz-verzameling samen uit de verzameling
        transitieve nederlagen.
        <ol>
             <li>
                  Een optie A zit in de Schwartz-verzameling indien voor
                  alle opties B, A ofwel B transitief verslaat of B A
                  niet transitief verslaat.
             </li>
        </ol>
   </li>
   <li> Indien er in de Schwartz-verzameling nederlagen zijn tussen
        opties, dan laten we de zwakste van dergelijke nederlagen weg uit
        de lijst van paarsgewijze nederlagen en keren we terug naar stap
        5.
        <ol>
             <li>
                  Een nederlaag (A,X) is zwakker dan een nederlaag (B,Y),
                  indien V(A,X) kleiner is dan V(B,Y). Eveneens is (A,X)
                  zwakker dan (B,Y), indien V(A,X) gelijk is aan V(B,Y)
                  en V(X,A) groter is dan V(Y,B).
             </li>
             <li>
                  Een zwakste nederlaag is een nederlaag die geen enkele
                  nog zwakkere nederlaag kent. Er kan meer dan één
                  dergelijke nederlaag zijn.
             </li>
        </ol>
   </li>
   <li> Indien er in de Schwartz-verzameling geen nederlagen zijn, dan
    wordt de winnaar gekozen uit de opties die de Schwartz-verzameling
    bevat. Indien er slechts één dergelijke optie is, dan is deze de
    winnaar. Indien er meerdere opties zijn, bepaalt de kiezer met de
    beslissende stem welke van deze opties wint.
   </li>
</ol>

<p>
 <strong>Opmerking:</strong> Opties welke de stemmers rangschikken boven
 de standaardoptie, zijn opties welke zij aanvaardbaar achten. Opties die
 gerangschikt werden onder de standaardoptie zijn opties welke zij
 onaanvaardbaar achten.
</p>

<p><cite>Wanneer de standaard resolutieprocedure gebruikt moet worden,
 moet de tekst die ernaar verwijst specificeren wat volstaat voor een
 ontwerpresolutie om ingediend en/of ondersteund te zijn, welke de
 minimale discussieperiode is en wat de stemperiode is. Deze moet ook een
 eventuele bijzondere meerderheid specificeren en/of het te gebruiken
 quorum (en de standaardoptie).</cite></p>

<h2>B. Taalgebruik en typografie</h2>

<p>De aantonende wijs (<q>is</q>, bijvoorbeeld) betekent dat de uitspraak
 in deze statuten als regel geldt.  <q>Mag</q> of <q>kan</q> geven aan
 dat de persoon of de instantie discretionaire bevoegdheid heeft.
 <q>Behoort te</q> betekent dat het als een goede zaak beschouwd wordt
 mocht aan de uitspraak gevolg gegeven worden, maar dat dit niet
 dwingend is. <cite>Tekst welke als een citaat, zoals dit, weergegeven
 wordt is toelichting en maakt geen deel uit van de statuten. Er kan
 enkel gebruik van gemaakt worden om te helpen bij de interpretatie van
 twijfelgevallen.</cite></p>
